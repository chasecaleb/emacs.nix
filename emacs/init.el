;;; -*- lexical-binding: t -*-
;;; Entry point to my config/init files.

(defconst cc/exwm-enabled (string= (getenv "ENABLE_EXWM") "true"))
;; unset env to prevent nested emacs instances from starting EXWM accidentally.
(setenv "ENABLE_EXWM")

(require 'cc-core-startup)
(cc/log-init-time "Started Emacs")
(cc/core-startup-init)
(cc/init-feature 'cc-nixos-startup #'cc/nixos-startup-init)

(require 'gcmh)
(progn
  ;; `gcmh-high-cons-threshold' defaults to 1 GB, which seems questionably high. The intent is to
  ;; effectively prevent GC until idle, but the downside is that *if* that threshold is somehow hit
  ;; then there will be a significant noticeable pause.
  (setq gcmh-high-cons-threshold (* 256 1024 1024))
  (setq gcmh-idle-delay 5)
  (gcmh-mode)
  (diminish 'gcmh-mode))

(let* ((cmd "cc-init-git-repos")
       (exit (call-process-shell-command cmd nil (get-buffer-create "*cc-init-git-repos*"))))
  (unless (zerop exit)
    (error "Command failed with exit code %s: %s" exit cmd)))

(cc/init-feature 'cc-user-directories #'cc/user-directories-init)
(cc/init-feature 'cc-auth #'cc/auth-init)
(cc/init-feature 'cc-appearance #'cc/appearance-init)
(cc/init-feature 'cc-mode-line #'cc/mode-line-init)

(cc/init-feature 'cc-exwm-utils #'cc/browser-init)
(when cc/exwm-enabled
  (declare-function cc/exwm-init "cc-exwm")
  (cc/init-feature 'cc-exwm #'cc/exwm-init)
  (declare-function cc/git-sync-setup "cc-git-sync")
  (cc/init-feature 'cc-git-sync #'cc/git-sync-setup))

(cc/init-feature 'cc-kitchen-sink)

(cc/init-feature 'cc-org-roam #'cc/org-roam-init)
(cc/init-feature 'cc-org-interactive #'cc/org-interactive-init)

(cc/init-feature 'cc-completions #'cc/completions-init)
(cc/init-feature 'cc-consult-project #'cc/consult-project-init)
(cc/init-feature 'cc-spelling #'cc/spelling-init)
(cc/init-feature 'cc-vterm #'cc/vterm-init)
(cc/init-feature 'cc-git #'cc/git-init)

(cc/init-feature 'cc-source-code #'cc/source-code-init)
(cc/init-feature 'cc-go #'cc/go-init)
(cc/init-feature 'cc-json #'cc/json-init)
(cc/init-feature 'cc-lisp #'cc/lisp-init)
(cc/init-feature 'cc-markdown #'cc/markdown-init)
(cc/init-feature 'cc-nix #'cc/nix-init)
(cc/init-feature 'cc-sh #'cc/sh-init)
(cc/init-feature 'cc-terraform #'cc/terraform-init)
(cc/init-feature 'cc-toml #'cc/toml-init)
(cc/init-feature 'cc-yaml #'cc/yaml-init)

(cc/init-feature 'cc-dir-locals #'cc/dir-locals-init)

(cc/log-init-time (concat "Finished loading user init: " user-init-file))
(add-hook 'after-init-hook
          (lambda ()
            (cc/log-init-time "Finished after-init-hook"))
          50)

(provide 'init)
