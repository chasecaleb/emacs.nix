;;; -*- lexical-binding: t -*-
;; Inspired by/some parts modified based on:
;; https://github.com/daviwil/dotfiles/blob/master/Desktop.org#panel
;;
;; Polybar ref: https://github.com/polybar/polybar/wiki/Module:-ipc
(require 'exwm-workspace)
(require 'org-clock)
(require 'cc-utils)

(defvar cc/panel--process nil)
(defvar cc/panel--update-time-timer nil)

(defconst cc/panel-update-hooks '(exwm-workspace-switch-hook
                                  org-clock-in-hook
                                  org-clock-out-hook
                                  org-clock-cancel-hook)
  "Hooks that should trigger a panel update")

(defun cc/panel--kill ()
  (when (and cc/panel--process
             (process-live-p cc/panel--process))
    (kill-process cc/panel--process))
  (setq cc/panel--process nil))

(defun cc/panel--start ()
  (cc/panel--kill)
  (cc/with-home-dir
   (setq cc/panel--process
         (start-process-shell-command "polybar" "*polybar*"
                                      "polybar panel"))))

(defun cc/panel--update-time ()
  "Some modules need to be updated periodically due to showing e.g. org clock time"
  (cc/with-home-dir
   (start-process-shell-command "polybar-msg" nil "polybar-msg hook emacs-org 1")))

(defun cc/panel--update ()
  (cc/with-home-dir
   (let ((cmd (string-join (seq-map (lambda (module)
                                      (format "polybar-msg hook %s 1" module))
                                    '("emacs-workspace" "emacs-org"))
                           "; ")))
     (start-process-shell-command "polybar-msg" nil cmd))))

(defun cc/panel-data-workspace ()
  (format "EXWM: %s" exwm-workspace-current-index))

(defun cc/panel-data-org ()
  "Org clock data.

Includes color format tags, since they're dynamic depending on value."
  (let* ((face (if (org-clocking-p)
                   'mode-line-buffer-id
                 'mode-line-highlight))
         (fg (cc/panel--color-to-rgb-hex (face-attribute face :foreground)))
         (bg (cc/panel--color-to-rgb-hex (face-attribute face :background)))
         (text (if (org-clocking-p)
                   (string-trim (substring-no-properties (org-clock-get-clock-string)))
                 "No clock")))
    (format "%%{F%s}%%{B%s} %s %%{F-}%%{B-}" fg bg text)))

(defun cc/panel--color-to-rgb-hex (color)
  "Convert an Emacs color name to CSS-style hex (e.g #ffffff).

Also handles RGB hex as input, in which case this is effectively an identity
function.

P.S. Emacs, how do you not have this as a utility function already?"
  (concat "#" (string-join
               (seq-map (lambda (value)
                          (format "%02x" (round (/ value (/ 65536 255)))))
                        (color-values color)))))

(defun cc/panel-enable ()
  (interactive)
  (cc/panel--start)
  (seq-do
   (lambda (hook)
     (add-hook hook #'cc/panel--update))
   cc/panel-update-hooks)
  (when (timerp cc/panel--update-time-timer)
    (cancel-timer cc/panel--update-time-timer))
  (setq cc/panel--update-time-timer (run-with-timer 60 60 #'cc/panel--update-time)))

(provide 'cc-panel)
