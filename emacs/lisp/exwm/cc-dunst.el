;;; -*- lexical-binding: t; byte-compile-warnings: (not docstrings)-*-
;;; Disable docstring warnings because of Hydra.
(require 'hydra)
(require 'cc-utils)

(defvar cc/dunst--resume-timer nil)
(defun cc/dunst-resume ()
  (interactive)
  (when (timerp cc/dunst--resume-timer)
    (cancel-timer cc/dunst--resume-timer)
    (setq cc/dunst--resume-timer nil))
  (cc/with-home-dir
   (shell-command "dunstctl set-paused false && notify-send --urgency low 'Notifications resumed'")))

(defun cc/dunst-pause-temporarily ()
  (interactive)
  (cc/with-home-dir (shell-command  "dunstctl set-paused true"))
  ;; Clear existing timer to reset pause duration.
  (when (timerp cc/dunst--resume-timer)
    (cancel-timer cc/dunst--resume-timer))
  (let ((minutes 75))
    (run-with-timer (* minutes 60) nil #'cc/dunst-resume)
    (message "Notifications paused for %s minutes" minutes)))

(defun cc/dunst-status-message ()
  (if (string-match-p "true" (cc/with-home-dir (shell-command-to-string "dunstctl is-paused")))
      "Notifications are: PAUSED"
    "Notifications are: on"))

(defhydra cc/dunst-hydra
  nil
  "[%s(cc/dunst-status-message)]"
  ("SPC" (cc/with-home-dir (shell-command "dunstctl close")) "Dismiss" :column "Action")
  ("C-/" (cc/with-home-dir (shell-command "dunstctl history-pop")) "Show history")
  ("g" (cc/with-home-dir (shell-command "dunstctl action")) "Go (default)" :exit t)
  ("M-g" (cc/with-home-dir (shell-command "dunstctl context")) "Go (menu)" :exit t)
  ("z" #'cc/dunst-resume "Resume" :column "Misc")
  ("M-z" #'cc/dunst-pause-temporarily "Pause")
  ("RET" nil "Quit"))

(provide 'cc-dunst)
