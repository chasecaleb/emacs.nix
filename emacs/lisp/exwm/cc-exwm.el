;;; -*- lexical-binding: t; byte-compile-warnings: (not docstrings)-*-
;;; Disable docstring warnings because of Hydra.
(require 'subr-x)
(require 'atomic-chrome)
(require 'exwm)
(require 'exwm-randr)
(require 'exwm-edit)
(require 'hydra)
(require 'markdown-mode)
(require 'cc-utils)
(require 'cc-appearance)
(require 'cc-dunst)
(require 'cc-panel)
(require 'cc-exwm-utils)

(defun cc/exwm-init ()
  (add-hook 'exwm-mode-hook #'cc/exwm--mode-setup)
  (add-hook 'exwm-manage-finish-hook #'cc/exwm--simulation-keys)
  (add-hook 'exwm-manage-finish-hook #'cc/exwm--child-emacs-setup)
  (add-hook 'exwm-update-title-hook #'cc/exwm--rename-buffer)
  (add-hook 'exwm-init-hook #'cc/exwm--init-callback)
  (add-hook 'exwm-randr-screen-change-hook #'cc/exwm--randr-on-screen-change)
  (add-hook 'exwm-randr-refresh-hook #'cc/exwm--randr-on-resolution-change)
  (add-hook 'after-init-hook #'cc/exwm--randr-on-screen-change)

  (cc/define-key-list exwm-mode-map
                      `(("C-q" .     ,#'exwm-input-send-next-key)
                        ("C-c '" .   ,#'exwm-edit--compose)
                        ("C-c C-'" . ,#'exwm-edit--compose)))
  (global-set-key [remap save-buffers-kill-terminal] #'cc/exwm-exit-hydra/body)

  (cc/csetq exwm-input-global-keys
            `((,(kbd "s-w") . exwm-workspace-switch)
              (,(kbd "s-k") . exwm-workspace-delete)
              (,(kbd "s-f") . exwm-layout-toggle-fullscreen)
              (,(kbd "s-M-f") . exwm-floating-toggle-floating)
              (,(kbd "s-SPC") . exwm-input-toggle-keyboard)
              (,(kbd "s-h") . previous-buffer)
              (,(kbd "s-l") . next-buffer)
              (,(kbd "s-M-h") . winner-undo)
              (,(kbd "s-M-l") . winner-redo)
              (,(kbd "s-b") . cc/hydra-exwm-apps/body)
              (,(kbd "s-a") . cc/hydra-web-apps/body)
              ,@(mapcar (lambda (it)
                          `(,(kbd (car it)) .
                            (lambda ()
                              (interactive)
                              (cc/with-home-dir
                               (start-process-shell-command "launcher" nil ,(cdr it))))))
                        '(("<XF86MonBrightnessDown>" . "light -U 3")
                          ("<XF86MonBrightnessUp>" . "light -A 3")))

              (,(kbd "s-.") . cc/dunst-hydra/body)
              ;; Additional notification close keybind, because this is quicker than going through hydra.
              (,(kbd "C-s-.") . (lambda ()
                                  (interactive)
                                  (cc/with-home-dir (shell-command "dunstctl close"))))
              (,(kbd "C-s-h") . ,(kbd "<left>"))
              (,(kbd "C-s-l") . ,(kbd "<right>"))
              (,(kbd "C-s-j") . ,(kbd "<down>"))
              (,(kbd "C-s-k") . ,(kbd "<up>"))))
  (cc/csetq exwm-input-prefix-keys
            `(,(kbd "M-o")
              ,(kbd "C-;")
              ,(kbd "C-M-;")
              ,(kbd "C-h")
              ,(kbd "C-u")
              ,(kbd "C-x")
              ,(kbd "M-:")
              ,(kbd "M-x")
              ,(kbd "M-y")
              ,(kbd "<s-return>")
              ;; All super-<letter> keybinds should be sent to Emacs, not X11.
              ,@(seq-map
                 (lambda (it)
                   (kbd (concat "s-" (byte-to-string it))))
                 (number-sequence ?a ?z))))
  (cc/csetq exwm-workspace-show-all-buffers t)
  (cc/csetq exwm-layout-show-all-buffers t)
  (cc/csetq exwm-manage-configurations
            '(((string-match-p "Cypress" exwm-class-name)
               managed t floating nil)
              ;; Fix IntelliJ popups - https://github.com/ch11ng/exwm/issues/680
              ;; This might be too broad of a match, but whatever.
              ((equal exwm-instance-name "sun-awt-X11-XDialogPeer")
               managed t floating t)
              ;; Firefox "picture-in-picture" video pop-outs
              ((and (string-match-p cc/firefox-class-regex exwm-class-name)
                    (equal exwm-title "Picture-in-Picture"))
               managed t floating nil)
              ;; Slack "open in new window" popups (e.g. for threads)
              ((string-match-p "Slack" exwm-class-name)
               managed t floating nil)))

  (advice-add #'exwm-layout--hide :around #'cc/exwm--layout-hide-focus-kludge)

  (exwm-enable)
  (exwm-randr-mode)
  (cc/exwm--editor-init))

(defun cc/exwm--mode-setup ()
  ;; EXWM mode lines a few things hidden, so more room for names.
  (setq-local cc/mode-line-id-length 65)
  ;; I don't want EXWM buffers associated with specific directories or projects (and especially not
  ;; TRAMP directories).
  (setq-local default-directory (expand-file-name "~")))

(defun cc/exwm--init-callback ()
  ;; Emacs-ception: need daemon for e.g. $EDITOR=emacsclient.
  (unless (server-running-p) (server-start))
  (cc/panel-enable)
  (when (file-exists-p "/etc/chasecaleb-work")
    (cc/with-home-dir
     (start-process-shell-command "slack" nil "slack --startup")
     ;; Press both ctrl keys (or both shift, or both alt) to toggle enabled/disabled.
     (start-process-shell-command "screenkey" nil "screenkey --start-disabled --vis-shift"))))

(defvar cc/exwm--kill-emacs-hook-command nil
  "Optional shell command to run as the (hopefully last) hook for `kill-emacs-hook'")
(defun cc/exwm--kill-emacs-do-command ()
  (when cc/exwm--kill-emacs-hook-command
    (cc/with-home-dir
     (shell-command cc/exwm--kill-emacs-hook-command))))

(defun cc/exwm-exit (command)
  (setq cc/exwm--kill-emacs-hook-command command)
  ;; Attempt to make sure my post-kill command runs at the very end of the hook, since it will do
  ;; e.g. shutdown or reboot.
  (remove-hook 'kill-emacs-hook #'cc/exwm--kill-emacs-do-command)
  (add-hook 'kill-emacs-hook #'cc/exwm--kill-emacs-do-command t)
  ;; No confirmation prompt - that's handled via `confirm-kill-emacs' (which exwm sets).
  (save-buffers-kill-emacs))

(defhydra cc/exwm-exit-hydra (:color blue)
  "EXWM Exit"
  ("s" (cc/with-home-dir (shell-command "systemctl suspend")) "Suspend")
  ("l" (cc/with-home-dir (shell-command "cc-lock")) "Lock")
  ("C-M-l" (cc/exwm-exit "loginctl terminate-session self") "Logout")
  ("C-M-p" (cc/exwm-exit "systemctl poweroff") "Poweroff")
  ("C-M-r" (cc/exwm-exit "systemctl reboot") "Reboot"))

;; I also use https://github.com/philc/vimium/ to provide avy-like link jumping.
;; Configuration:
;; unmapAll
;; map <c-o> LinkHints.activateMode
;; map <c-O> LinkHints.activateModeToOpenInNewTab
(defun cc/exwm--simulation-keys ()
  "Set `exwm-input-set-local-simulation-keys' for current application."
  (let ((common `(;; Navigation
                  (,(kbd "M-<") . ,(kbd "<home>"))
                  (,(kbd "M->") . ,(kbd "<end>"))
                  (,(kbd "C-v") . ,(kbd "<down><down><down><down><down>"))
                  (,(kbd "M-v") . ,(kbd "<up><up><up><up><up>"))
                  ;; Text manipulation
                  (,(kbd "C-w") . ,(kbd "C-x"))
                  (,(kbd "M-w") . ,(kbd "C-c"))
                  (,(kbd "C-y") . ,(kbd "C-v"))
                  (,(kbd "C-/") . ,(kbd "C-z")))))
    (if (cc/firefox-buffer-p)
        (exwm-input-set-local-simulation-keys
         `(,@common
           ;; Navigation
           (,(kbd "C-n") . ,(kbd "<down>"))
           (,(kbd "C-p") . ,(kbd "<up>"))
           (,(kbd "C-f") . ,(kbd "<right>"))
           (,(kbd "C-b") . ,(kbd "<left>"))
           (,(kbd "C-s") . ,(kbd "C-f"))
           ;; Tab/window management
           (,(kbd "C-k") . ,(kbd "C-w")) ; close
           (,(kbd "C-t") . ,(kbd "C-n")) ; open new window (not tab)
           (,(kbd "C-S-t") . ,(kbd "C-S-n")) ; restore window
           (,(kbd "M-l") . ,(kbd "M-<left>")) ; history back
           (,(kbd "M-r") . ,(kbd "M-<right>"))))
      (exwm-input-set-local-simulation-keys common)))) ; history forward

(defun cc/exwm--child-emacs-setup ()
  "Set child Emacs EXWM buffer to char mode when opened."
  (when (string= exwm-class-name "Emacs")
    (exwm-input-toggle-keyboard exwm--id)))

(defvar-local cc/exwm--firefox-id nil)
(defun cc/exwm--firefox-next-id ()
  (let ((ff-buffers (seq-filter #'cc/firefox-buffer-p (buffer-list))))
    (1+ (seq-max (cons -1 (seq-map (lambda (it)
                                     (with-current-buffer it
                                       (or cc/exwm--firefox-id -1)))
                                   ff-buffers))))))

(defun cc/exwm--rename-buffer ()
  ;; `exwm-title' may not be set immediately (at least once upon a time for Firefox).
  (let ((name (or exwm-title exwm-class-name)))
    (exwm-workspace-rename-buffer
     (cond
      ((cc/firefox-buffer-p)
       (unless cc/exwm--firefox-id
         (setq-local cc/exwm--firefox-id (cc/exwm--firefox-next-id)))
       (let ((parsed (cc/firefox-buffer-name name cc/exwm--firefox-id)))
         (setf (cc/firefox-url) (cadr parsed))
         (car parsed)))
      (t name)))))

(defun cc/exwm--buffers (class-regex)
  "Get matching EXWM buffers."
  (seq-filter (lambda (b)
                (with-current-buffer b
                  (and
                   (equal major-mode 'exwm-mode)
                   (not (null exwm-class-name)) ; xev spawns a window with no class.
                   (string-match-p class-regex exwm-class-name))))
              (buffer-list)))

(defun cc/exwm-run-or-switch (class-regex start-cmd)
  "Switch to first EXWM buffer of CLASS or start one if none exists.

Will always start a new instance if called with prefix argument."
  (let ((existing (cc/exwm--buffers class-regex)))
    (if (or current-prefix-arg
            (not existing))
        (cc/with-home-dir
         (start-process-shell-command (car (split-string start-cmd)) nil start-cmd))
      (switch-to-buffer (car existing)))))

(defun cc/exwm--layout-hide-focus-kludge (fn &rest args)
  "Gross kludge to fix EXWM focus issue when switching to an existing Electron X11 window.

Based on https://github.com/ch11ng/exwm/issues/759#issuecomment-692824523"
  (cc/with-advice #'exwm-layout--set-ewmh-state :before
                  (lambda (&rest _ewmh-args)
                    ;; I'm not sure what side effects this kludge may have (do X11 apps pay
                    ;; attention to whether or not they're "hidden"?), so only do it for programs
                    ;; that actually need it. Firefox doesn't need this kludge, for example. Too bad
                    ;; there isn't a way to easily determine if a given X11 window is electron.
                    (when (member exwm-class-name '("Slack" "discord"))
                      (setq exwm--ewmh-state
                            (delq xcb:Atom:_NET_WM_STATE_HIDDEN exwm--ewmh-state))))
    (apply fn args)))

(defun cc/exwm-screenshot ()
  "Take screenshot and save filename to kill-ring."
  (interactive)
  (message "Taking screenshot...")
  (let* ((default-directory temporary-file-directory)
         (cmd "scrot --select --exec 'echo -n \"$f\"' 2>/dev/null")
         (result (shell-command-to-string cmd)))
    (if (string-empty-p result)
        (message "Screenshot cancelled")
      (let ((absolute-file (expand-file-name result)))
        (kill-new absolute-file)
        (message "Saved screenshot: %s" absolute-file)))))

(defhydra cc/hydra-exwm-apps (:color blue)
  ("d" (cc/exwm-run-or-switch (rx bos "discord" eos) "discord") "Discord" :column "General")
  ("e" (cc/exwm-run-or-switch (rx bos "Element" eos) "element-desktop") "Element")
  ;; Warning: Yubikey likes to change the executable and X11 class name frequently... and no,
  ;; "yubioath" without a "u" is not a typo (at least currently).
  ("y" (cc/exwm-run-or-switch (rx bos ".yubioath") "yubioath-flutter") "Yubikey")
  ("/" cc/exwm-screenshot "Screenshot")
  ("l" (cc/exwm-run-or-switch (rx bos "Slack" eos) "slack") "Slack" :column "Work")
  ("C-g" nil nil)
  ("RET" nil nil))

(defun cc/exwm-web-app (url title-regexp)
  (let ((existing (seq-filter (lambda (b)
                                (with-current-buffer b
                                  (string-match-p title-regexp exwm-title)))
                              (cc/exwm--buffers "firefox"))))
    (if existing
        (switch-to-buffer (car existing))
      ;; This will break if url has single quotes, but it's my own config so whatever.
      (cc/with-home-dir
       (start-process-shell-command "exwm-web-app" nil (format "firefox --new-window '%s'" url))))))

(defhydra cc/hydra-web-apps (:color blue)
  ("m" (cc/exwm-web-app "https://messages.google.com/web" "Messages for web") "SMS" :column "Apps")
  ;; Assumption: @gmail.com is the default Google account (e.g. first account signed in)
  ("g" (cc/exwm-web-app "https://mail.google.com/mail/u/0" "- Gmail -") "Gmail")
  ("G" (cc/exwm-web-app "https://mail.google.com/mail/u/1" "- chasecaleb Mail -") "Gmail (chasecaleb)")
  ("y" (cc/exwm-web-app "https://app.youneedabudget.com" "| YNAB") "YNAB")
  ("t" (cc/exwm-web-app "https://todoist.com/app" ": Todoist") "Todoist")
  ("o" (cc/exwm-web-app "https://outlook.office365.com" "Outlook") "Outlook" :column "Work")
  ("j" (cc/exwm-web-app "https://jira.naic.org" "NAIC / NIPR Jira") "Jira"))

(defun cc/exwm--randr-on-screen-change ()
  "Adjust screen resolution when e.g. VM window is resized."
  (cc/with-home-dir
   (start-process-shell-command "xrandr" nil "xrandr --output \"$(xrandr | grep primary | awk '{print $1}')\" --auto")))

(defun cc/exwm--randr-on-resolution-change ()
  "Adjust font size as needed after resolution changes."
  ;; This is quite the hack, but it's the least crazy way I could think of to automatically switch
  ;; font size on my work laptop (2019 Macbook Pro 16") install, which runs in a VM. Unfortunately
  ;; due to running in a VM there isn't a way to tell which display is actually in use, but I can
  ;; make a reasonable guess based on screen resolution. This doesn't work if the VM isn't
  ;; full-screen, but fortunately I live inside the VM 99% of the time so that's not a big issue.
  ;;
  ;; Now if only Firefox was this easy to change programmatically...
  (when (file-exists-p "/etc/chasecaleb-work")
    (let* ((mac-resolution "3072x1920") ; Internal display set to "looks like 1536x960"
           (surface-resolution "2496x1664") ; Internal display for Surface laptop.
           (actual-resolution
            (replace-regexp-in-string (rx "\n" string-end) ""
                                      (cc/with-home-dir
                                       (shell-command-to-string
                                        (concat "xrandr --listmonitors | "
                                                "awk '/Virtual/ {print $3}' | "
                                                "awk -F '[/x]' '{print $1 \"x\" $3}'")))))
           (large-font-p (or (string= mac-resolution actual-resolution)
                             (string= surface-resolution actual-resolution))))
      (cc/appearance-font-size-adjust large-font-p))))


;;; X11 editing: exwm-edit and atomic-chrome.
;; For atomic chrome: make sure to install GhostText for FireFox/Atomic Chrome for Chrom(e|ium).
;; https://github.com/agzam/exwm-edit
;; https://github.com/alpha22jp/atomic-chrome

(defun cc/exwm--editor-init ()
  ;; Default to markdown since I tend to use exwm-edit for GitLab/GitHub in particular.
  (add-hook 'exwm-edit-compose-hook #'markdown-mode)

  (setq atomic-chrome-url-major-mode-alist
        `((,(rx "github.com") . gfm-mode)
          (,(rx "gitlab.com") . gfm-mode)))
  (advice-add #'atomic-chrome-show-edit-buffer :around #'cc/exwm--atomic-chrome-display-advice)
  (atomic-chrome-start-server))

(defun cc/exwm--atomic-chrome-display-advice (fn &rest args)
  "Customize window placement for `atomic-chrome'.

Using `display-buffer-alist' doesn't work for atomic chrome since the buffers it
creates don't have a specific, unique major mode nor are they named
predictably. Advice to the rescue!"
  (let ((display-buffer-overriding-action
         '(cc/appearance--display-buffer-mru-window
           display-buffer-use-some-window
           display-buffer-pop-up-window)))
    (apply fn args)))

(provide 'cc-exwm)
