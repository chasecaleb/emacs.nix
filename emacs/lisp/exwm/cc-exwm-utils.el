;;; -*- lexical-binding: t -*-
(require 'browse-url)
(require 'cl-lib)
(require 'consult)
(require 'ibuffer)

(defun cc/browser-init ()
  (setq browse-url-generic-program "firefox")
  (setq browse-url-browser-function #'cc/browse-url-or-switch)
  (cc/firefox-ibuffer--init))

(defconst cc/firefox-class-regex (rx bos (or "firefox" "Firefox")))

(defvar-local cc/exwm--firefox-url nil)

(defun cc/firefox-buffer-p (&optional buffer)
  (with-current-buffer (or buffer (current-buffer))
    (and (derived-mode-p 'exwm-mode)
         (bound-and-true-p exwm-class-name)
         (string-match-p cc/firefox-class-regex  exwm-class-name))))

(defun cc/firefox-buffer-name (name id)
  "Parse original FireFox buffer name and return cons of new-name . url"
  (let* ((replaced (replace-regexp-in-string
                    ;; I can't persuade Emacs to save a Unicode em-dash properly, so
                    ;; punctuation class it is. Also this is more robust.
                    (rx  (+ " ") (+ punctuation) (+ " ") "Mozilla Firefox" string-end)
                    ""
                    name))
         (prefix (format "[%03d]" (mod id 1000))))
    ;; URL at end of title is provided by this extension:
    ;; https://github.com/erichgoldman/add-url-to-window-title
    ;; See also my unit tests.
    (if (string-match (rx bos (group (+ any)) " - " (group (+? any)) eos) replaced)
        (let ((page-title (match-string 1 replaced))
              (url (match-string 2 replaced)))
          (list (concat prefix " " page-title) url))
      (list (concat prefix " " replaced) ""))))


;;; `browse-url' related

(defun cc/firefox-url (&optional buffer)
  "Can be used with `setf' to modify.

If BUFFER is not provided, defaults to `current-buffer'."
  (with-current-buffer (or buffer (current-buffer))
    cc/exwm--firefox-url))

(gv-define-setter cc/firefox-url (value &optional buffer)
  `(with-current-buffer (or ,buffer (current-buffer))
     (setq-local cc/exwm--firefox-url ,value)))

(defun cc/browse-url-or-switch (url &rest _args)
  (let* ((normalized (cc/browse-url--normalize url))
         (existing (seq-map #'buffer-name
                            (seq-filter (lambda (buf)
                                          (with-current-buffer buf
                                            (and (cc/firefox-buffer-p)
                                                 (equal (cc/browse-url--normalize (cc/firefox-url))
                                                        normalized))))
                                        (buffer-list)))))
    (cl-labels ((do-browse ()
                           ;; Use `browse-url-firefox' directly in order to open in a new window,
                           ;; instead of opening as a tab in an existing window and then letting a
                           ;; browser addon convert it to a new window. This significantly improves
                           ;; buffer history and window placement.
                           (browse-url-firefox url t)))
      (if existing
          (let ((consult-buffer-sources `((:name "Similar buffers (C-g for new)"
                                           :category buffer
                                           :face consult-buffer
                                           :history buffer-name-history
                                           :state ,#'consult--buffer-state
                                           :items ,existing
                                           :new ,(lambda (_ignored) (do-browse))))))
            (condition-case nil
                (consult-buffer)
              (quit (do-browse))))
        (do-browse)))))

(defun cc/browse-url--normalize (url)
  "Parse and normalize URL.

Intended usage: comparing if two different URLs are equivalent-ish."
  ;; Need to handle various invalid URLs without crashing in case the browser plugin mis-detects the
  ;; URL, e.g. an empty string, nil, or a title string instead of the URL.
  (condition-case nil
      (let* ((parsed (url-generic-parse-url url))
             ;; `url-generic-parse-url' annoyingly shoves path and query string into the same field.
             (path (replace-regexp-in-string (rx "?" (* any)) "" (url-filename parsed)))
             (path-normalized (replace-regexp-in-string (rx "/" eos) "" path)))
        (concat (url-domain parsed) (if (equal path "/") "" path-normalized)))
    (error "")))


;;; Ibuffer

(defun cc/firefox-ibuffer--init ()
  (define-ibuffer-column cc/url-domain (:name "Domain")
    (cc/firefox-ibuffer--extract buffer #'url-domain))
  (define-ibuffer-column cc/url-path (:name "Path")
    (cc/firefox-ibuffer--extract buffer #'url-filename))

  (define-ibuffer-sorter url "Sort the buffers by FireFox URL." (:description "url")
                         (cc/firefox-ibuffer--sort a b))

  ;; Intentionally not #'function-quoting here, because byte compiler gets confused by the macro.
  (define-key ibuffer-mode-map (kbd "s u") 'ibuffer-do-sort-by-url))

(defun cc/firefox-ibuffer--extract (buffer extract-fn)
  "Extract part of a URL from a FireFox EXWM buffer."
  ;; condition-case error handler because URL parsing is exception-happy with e.g. strings that
  ;; aren't URLs, are nil, and so on. It's simpler to go with Python's
  ;; easier-to-ask-forgiveness-than-permission philosophy instead of look-before-you-leap
  (condition-case nil
      (funcall extract-fn (url-generic-parse-url (cc/firefox-url buffer)))
    (error "")))

(defun cc/firefox-ibuffer--sort (a b)
  (let ((parse (lambda (buffer)
                 (concat (cc/firefox-ibuffer--extract buffer #'url-domain)
                         (cc/firefox-ibuffer--extract buffer #'url-filename)))))
    (string-lessp (funcall parse (car a))
                  (funcall parse (car b)))))

(defun cc/firefox-ibuffer()
  "Special `ibuffer' for Firefox."
  (interactive)
  (let ((ibuffer-name "*Ibuffer: Firefox*"))
    (ibuffer nil
             ibuffer-name
             '((predicate . (cc/firefox-buffer-p)))
             nil
             nil
             nil
             ;; Use backtick to toggle between formats
             `(((name 70 70 :left :elide)
                " | " (cc/url-domain 25 25 :left :elide)
                cc/url-path)
               ((name 85 85 :left)
                " | " (cc/url-domain 35 35 :left)
                cc/url-path)
               (name
                " | " cc/url-domain
                cc/url-path)))
    ;; `ibuffer' doesn't return a buffer object, so find it (it *should* be the current focused
    ;; buffer, but I think buffer window placement customization could affect that).
    (with-current-buffer (get-buffer ibuffer-name)
      ;; Too bad `ibuffer' doesn't have another argument to specify initial sort. Oh well.
      (setq-local ibuffer-sorting-mode 'url)
      (ibuffer-update nil t))))

(provide 'cc-exwm-utils)
