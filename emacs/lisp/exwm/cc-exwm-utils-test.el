;;; -*- lexical-binding: t -*-
(require 'buttercup)
(require 'cc-exwm-utils)

(describe (symbol-name #'cc/firefox-buffer-name)
  (it "Works for simple case"
    (expect (cc/firefox-buffer-name "Some Title - foo.com/path?query=true - Mozilla Firefox" 0)
            :to-equal '("[000] Some Title" "foo.com/path?query=true")))
  (it "Works with dashes in title and url"
    (expect (cc/firefox-buffer-name "some - dash - title - foo.com/some-path - Mozilla Firefox" 123)
            :to-equal '("[123] some - dash - title" "foo.com/some-path")))
  (it "Works for blank new page"
    (expect (cc/firefox-buffer-name "Mozilla Firefox" 456)
            :to-equal '("[456] Mozilla Firefox" ""))))

(describe (symbol-name #'cc/browse-url--normalize)
  (it "Works as expected"
    (expect (cc/browse-url--normalize "https://www.foo.com/some/path?query=xyz")
            :to-equal "foo.com/some/path"))
  (it "Handles url without a path"
    (expect (cc/browse-url--normalize "https://google.com/")
            :to-equal "google.com"))
  (it "Removes www prefix"
    (expect (cc/browse-url--normalize "https://www.foo.com")
            :to-equal "foo.com"))
  (it "Removes query string"
    (expect (cc/browse-url--normalize "https://example.com/path?a=1&b=2")
            :to-equal "example.com/path"))
  (it "Handles trailing slash in path"
    (expect (cc/browse-url--normalize "https://example.com/foo/")
            :to-equal "example.com/foo"))
  (it "Handles nil"
      (expect (cc/browse-url--normalize nil)
              :to-equal ""))
  (it "Handles invalid string"
      (expect (cc/browse-url--normalize "this is not a url")
              :to-equal "")))
