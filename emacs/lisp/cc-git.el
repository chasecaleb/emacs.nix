;;; -*- lexical-binding: t; byte-compile-warnings: (not docstrings) -*-
;;; TODO remove byte-compile-warnings config (above) if/when I replace hydra with consult.
;;; Version control/git-related config.
(require 'cc-user-directories)
(require 'cc-utils)
(require 'cc-org-utils)
(require 'diff-hl)
(require 'ediff)
(require 'forge)
(require 'git-link)
(require 'git-timemachine)
(require 'lab)
(require 'magit)
(require 'magit-delta)
(require 'magit-ediff)
(require 'magit-extras)
(require 'orgit) ; Just needs to be loaded, no config required.
(require 'yaml)


;;; General

(defun cc/git-init ()
  (setq git-commit-style-convention-checks '(non-empty-second-line overlong-summary-line))
  (setq git-commit-summary-max-length 50)
  (setq git-commit-use-local-message-ring t)
  (define-key diff-mode-map (kbd "M-o") nil) ; Conflicts with ace-window switching.
  (global-set-key (kbd "C-c g") #'cc/git-hydra/body)
  (cc/git--bug-reference-init)
  (cc/git--magit-init)
  (cc/git--magit-delta-init)
  (cc/git--git-link-init)
  (cc/git--git-timemachine-init)
  (cc/git--forge-init)
  (cc/git--lab-init))

(defun cc/git--ediff-init ()
  (setq magit-ediff-dwim-show-on-hunks t)
  (setq ediff-window-setup-function #'ediff-setup-windows-plain))

(defun cc/git-merge-request ()
  "Create merge request in GitLab."
  (interactive)
  (magit-git-command "git go-mr"))

(defhydra cc/git-hydra (:color blue)
  ("ll" #'git-link "Line/region" :column "Links")
  ("lf" #'cc/git-git-link-file "File")
  ("lc" #'git-link-commit "Commit (hash)")
  ("lp" #'git-link-homepage "Project home")
  ("lm" #'cc/git-merge-request "Create MR")
  ("t" #'git-timemachine-toggle "Timemachine" :column "Buffer-specific")
  ("g" #'magit-file-dispatch "Magit: file dispatch")
  ("n" #'diff-hl-next-hunk "Next change" :color red)
  ("p" #'diff-hl-previous-hunk "Prev change" :color red)
  ("rm" #'magit-list-repositories "Magit: all" :column "Repos")
  ("rf" #'forge-list-repositories "Forge: all")
  ;; FIXME forge-list-global-pullreqs and so on?
  ;; ("ma" #'cc/forge-global-list-pullreqs "All":column "Merge Requests")
  ;; ("mc" #'cc/forge-global-list-authored-pullreqs "Created by me")
  ;; ("mp" #'cc/forge-global-list-participating-pullreqs "Participating")
  ;; Semicolon prefix for lab: just a random home row key, not a mnemonic. Might revisit this in the
  ;; future after getting a better feel for forge vs lab.el vs glab (cli) workflows.
  (";m" #'lab-list-my-merge-requests "My MRs (global)" :column "GitLab")
  (";p" #'lab-list-project-pipelines "Pipelines")
  (";w" #'lab-list-project-pipelines "Watch last pipeline")
  ("C-g" nil nil)
  ("RET" nil nil))


;;; bug-reference (built-in)

(defconst cc/git--bug-reference-setup-work
  (list (rx "gitlab.com" (or "/" ":") (group "NAIC/" (+ ascii)) ".git")
        ;; Jira issues: NIPR-123
        ;; GitLab issues: #123 or project#123 or parent/group/project#123
        ;; GitLab MRs: !123 or project!123 or parent/group/project#123
        (rx (group (or (group "NIPR-" (+ digit))
                       (seq (? (group (+ (any "A-Za-z0-9_/-"))))
                            (group (or "#" "!"))
                            (group (+ numeric))))))
        (lambda (repo-groups)
          (lambda ()
            (let ((jira-id (match-string-no-properties 2))
                  (gitlab-project (or (match-string-no-properties 3)
                                      (nth 1 repo-groups)))
                  (gitlab-type (match-string-no-properties 4))
                  (gitlab-id (match-string-no-properties 5)))
              (cond
               (jira-id
                (format "https://jira.naic.org/browse/%s" jira-id))
               ((string= gitlab-type "#")
                (format "https://gitlab.com/%s/issues/%s" gitlab-project gitlab-id))
               ((string= gitlab-type "!")
                (format "https://gitlab.com/%s/merge_requests/%s" gitlab-project gitlab-id))
               (t
                (error "Unexpected bug reference match failure: %s" (match-string-no-properties 0))))))))
  "`bug-reference-setup-from-vc-alist' entry for work repos.")

(defun cc/git--bug-reference-init ()
  (add-hook 'cc/source-code-hook #'bug-reference-prog-mode)
  ;; Depth 90 because `forge-bug-reference-hooks' needs to go before `bug-reference-mode' hook so
  ;; that pressing RET on the start of issue and PR lines opens in forge, not browser. Opening in
  ;; browser is still possible with C-c C-w (`forge-browse-pullreq' etc) or C-c C-o (custom
  ;; `bug-reference-push-button' keybind).
  (add-hook 'magit-mode-hook #'bug-reference-mode 90)
  (define-key bug-reference-map (kbd "C-c C-o") #'bug-reference-push-button)
  (setq bug-reference-setup-from-vc-alist (list cc/git--bug-reference-setup-work)))


;;; Magit core (i.e. not 3rd party additions)

(defun cc/git--magit-init ()
  (cc/define-key-list magit-repolist-mode-map
                      `(("f" . ,#'cc/magit-repolist-git-fetch)))
  (cc/define-key-list magit-section-mode-map
                      `(("C-M-u" . magit-section-up)
                        ;; Same bindings as M-n and M-p.
                        ("C-M-n" . magit-section-forward-sibling)
                        ("C-M-p" . magit-section-backward-sibling)))

  (diminish 'magit-wip-mode)
  (add-hook 'git-commit-setup-hook #'cc/magit-commit-setup)
  (dolist (fn (list #'magit-branch-create
                    #'magit-branch-and-checkout
                    #'magit-branch-spinoff
                    #'magit-branch-spinout))
    (advice-add fn :filter-args #'cc/magit--interactive-branch-name-advice))

  ;; Use git-absorb instead of git-autofixup since it's more popular and maintained.
  ;; TLDR: automatically create fixup commits for staged changes based on the most recent commit to
  ;; touch a given change.
  ;; https://github.com/tummychow/git-absorb
  ;; Thanks to https://lobste.rs/s/llxaji/git_autofixup_create_fixup_commits_for#c_qdgmtx
  (transient-replace-suffix 'magit-commit 'magit-commit-autofixup
    '(4 "x" "Absorb changes" magit-commit-absorb))

  (setq magit-diff-refine-hunk 'all)
  ;; keep-foreground is a bit hard on the eyes (with spacemacs-dark).
  (setq magit-diff-unmarked-lines-keep-foreground nil)

  (setq magit-branch-prefer-remote-upstream '("master" "main"))
  (setq magit-branch-adjust-remote-upstream-alist '(("origin/master" . ("master" "main"))
                                                    ("origin/main" . ("master" "main"))))
  (setq magit-clone-set-remote.pushDefault t)
  ;; `quit-window' with t also kills the buffer, which I find helpful to avoid slowdown over time
  ;; when working with a ton of repos. Also not using `magit-mode-quit-window' because I don't like
  ;; how it potentially messes with my window layout.
  (setq magit-bury-buffer-function (lambda (_) (quit-window t)))
  (setq magit-repository-directories
        (seq-map (lambda (dir) (cons dir 1)) cc/user-repo-search-dirs))
  (setq magit-status-goto-file-position t) ; This is amazing and should be a default.
  (setq magit-save-repository-buffers 'dontask) ; These prompts are annoying
  (setq magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1)
  (setq magit-copy-revision-abbreviated t)

  (magit-wip-mode t)

  ;; `magit-delete-by-moving-to-trash' doesn't work properly over SSH, and I don't care about it.
  (setq magit-delete-by-moving-to-trash nil)

  (setf (alist-get 'unpulled magit-section-initial-visibility-alist) 'show)

  (setq magit-repolist-columns '(("Name"    25 magit-repolist-column-ident nil)
                                 ("Branch"  15 magit-repolist-column-branch nil)
                                 ("St"       2 magit-repolist-column-stashes
                                  ((:help-echo "Number of stashes")
                                   (:right-align t)))
                                 ("F"        1 magit-repolist-column-flag
                                  ((:help-echo "Status flag (N=untracked, U=unstaged, S=staged)")
                                   (:right-align t)))
                                 ("B<U"      3 magit-repolist-column-unpulled-from-upstream
                                  ((:right-align t)
                                   (:help-echo "Upstream changes not in branch")))
                                 ("B>U"      3 magit-repolist-column-unpushed-to-upstream
                                  ((:right-align t)
                                   (:help-echo "Local changes not in upstream")))
                                 ("Path"    99 magit-repolist-column-path nil)))

  ;; Magit performance in enormous nixpkgs repo is... subpar, to put it lightly.
  ;; Additional tip: for logs, disable graph (-g) in transient. Would add that here too but I rarely
  ;; look at nixpkgs logs, and I'm not sure how well dir-local transient customization would work.
  (let ((eval-sexp `(progn
                      (magit-disable-section-inserter #'magit-insert-tags-header))))
    (add-to-list 'safe-local-variable-values (cons 'eval eval-sexp))
    (dir-locals-set-class-variables
     'magit-performance
     `((magit-status-mode
        . ((eval . ,eval-sexp))))))
  (dolist (d '("~/nix-input/all/nixpkgs"
               "~/nix-inputs/updates/nixpkgs"
               "~/code/nixpkgs"))
    (dir-locals-set-directory-class (expand-file-name d) 'magit-performance)))

;; TODO replace/remove this
(defun cc/magit--read-string-branch-advice (args)
  "See `cc/magit--interactive-branch-name-advice'."
  (if-let* ((ticket (with-temp-buffer
                      (cc/org-clock-insert-ticket "-ccc-")
                      (buffer-string)))
            ;; eks-cluster repo has an annoying naming convention that I always forget.
            (name (if (string= (file-relative-name (magit-gitdir) "~/nipr/eks-cluster")
                               ".git/")
                      (format "exp/%s" ticket)
                    ticket)))
      ;; This is awkward, but it handles a list of length 1, unlike (setf (nth 1 ..) ..).
      (append (list (car args) name) (nthcdr 2 args))
    args))

(defun cc/magit--interactive-branch-name-advice (args)
  "Awkward hack to set ticket as default name when creating a new branch.

This is further complicated due to the branch name prompt occurring in the
`interactive' form."
  (interactive (lambda (old-spec)
                 (cc/with-advice #'magit-read-string-ns :filter-args #'cc/magit--read-string-branch-advice
                   (advice-eval-interactive-spec old-spec))))
  args)

(defun cc/magit-commit-setup ()
  (setq fill-column 72)
  (unless (git-commit-buffer-message)
    (cc/org-clock-insert-ticket ": ")))

(defun cc/magit-repolist-git-fetch ()
  "Fetch all remotes in repositories returned by `magit-list-repos'.

Fetching is done synchronously. Based on:
https://github.com/magit/magit/issues/2971#issuecomment-336644529"
  (interactive)
  (run-hooks 'magit-credential-hook)
  (let* ((repos (magit-list-repos))
         (l (length repos))
         (i 0))
    (dolist (repo repos)
      (let* ((default-directory (file-name-as-directory repo))
             (msg (format "(%s/%s) Fetching in %s..."
                          (cl-incf i) l default-directory)))
        (message msg)
        ;; Hack to deal with caching bug. Caches are fun and easy, right? (n.b. no, they aren't)
        (cc/with-advice #'magit-get-upstream-branch :around
                        (lambda (fn &rest args)
                          (let ((magit--refresh-cache nil))
                            (apply fn args)))
          (magit-run-git "remote" "update" (magit-fetch-arguments)))

        (message (concat msg "done")))))
  (magit-refresh))


;;; magit-delta: https://github.com/dandavison/magit-delta

(defun cc/git--magit-delta-init ()
  (add-hook 'magit-mode-hook #'cc/magit-delta-maybe)
  ;; zenburn and Sublime Snazzy are also decent.
  (setq magit-delta-default-dark-theme "Monokai Extended")
  ;; Don't hide plus/minus markers, because that makes copying to kill ring confusing (since yanking
  ;; will then insert invisible markers which cause pain).
  (setq magit-delta-hide-plus-minus-markers nil))

(defun cc/magit-delta-maybe ()
  "Enable `magit-delta-mode' except on certain large repos for performance."
  ;; Checking the relative directory name instead of the remote url is simple and good enough.
  (let ((repo-name (file-name-nondirectory
                    (replace-regexp-in-string (rx "/" eos) "" default-directory))))
    (unless (string= repo-name "nixpkgs")
      (magit-delta-mode))))


;;; git-link
(defun cc/git--git-link-init ()
  (setq git-link-open-in-browser t))

(defun cc/git-git-link-file ()
  "`git-link' but without line number(s)."
  (interactive)
  (let ((current-prefix-arg '-))
    (call-interactively #'git-link)))


;;; git-timemachine

(defun cc/git--git-timemachine-init ()
  (add-hook 'git-timemachine-mode-hook #'cc/git-timemachine-setup)
  (advice-add #'git-timemachine-show-revision :after #'cc/git-timemachine-modify-id)
  (setq git-timemachine-abbreviation-length 8))

(defun cc/git-timemachine-modify-id (&rest _args)
  "Adjust overly-verbose git-timemachine modeline.

See: https://gitlab.com/pidu/git-timemachine/-/issues/83"
  (setq mode-line-buffer-identification
        (list (car mode-line-buffer-identification)
              "@"
              (nth 2 mode-line-buffer-identification)
              ;; 3+4 are git file name. This is useful if the file was renamed at some point, but
              ;; it's too noisy to be worth it. Just press "c" to show the commit in magit to figure
              ;; it out if that's ever relevant.
              (nth 5 mode-line-buffer-identification) ; 5 is whitespace
              ;; Remove redundant date from end - it's shown in minibuffer too.
              (replace-regexp-in-string
               (rx " " (one-or-more (not ")")))
               ""
               (nth 6 mode-line-buffer-identification)))))

(defun cc/git-timemachine-setup ()
  ;; Shorten buffer name prefix. See:
  (rename-buffer (replace-regexp-in-string
                  (rx string-start "timemachine:")
                  "T:"
                  (buffer-name)))
  (setq-local cc/mode-line-id-length 70))


;;; forge
(defun cc/git--forge-init ()
  (dolist (map (list magit-repolist-mode-map
                     forge-repository-list-mode-map))
    (define-key map (kbd "F") #'cc/magit-repolist-forge-fetch))
  ;; FIXME look through info manual to figure out new way of adding this
  ;; (magit-add-section-hook 'magit-status-sections-hook
  ;;                         #'forge-insert-authored-pullreqs
  ;;                         #'forge-insert-pullreqs)
  ;; (magit-add-section-hook 'magit-status-sections-hook
  ;;                         #'forge-insert-assigned-issues
  ;;                         #'forge-insert-issues)

  (setq forge-database-file (expand-file-name "forge-database.sqlite" cc/user-cache))
  ;; Hide closed MRs initially, show with `forge-toggle-closed-visibility'
  ;; FIXME `forge-limit-topic-choices' ?
  ;; (setq forge-topic-list-limit '(60 . -10))
  (setq forge-owned-accounts '(("chasecaleb") ("cchase"))))

;; TODO is there a new equivalent of this?
(defun cc/magit-repolist-forge-fetch ()
  "Do `forge-pull' to update all known forge repos in `magit-list-repos'."
  (interactive)
  (run-hooks 'magit-credential-hook)
  (let* ((repos (magit-list-repos)))
    (dolist (repo repos)
      (let ((default-directory (file-name-as-directory repo)))
        (message "directory: %s" default-directory)
        (if (forge-get-repository nil)
            ;; It would be nice to have a way to tell when all these async `forge-pull' commands
            ;; finish in order to refresh to repo list afterwards, but oh well.
            (forge-pull)
          (message "Skipping repo: %s" repo))))))


;;; lab: opinionated GitLab interface, primarily for pipelines and MRs.
;;; https://github.com/isamert/lab.el

(defun cc/git--lab-token-advice (&rest _advice-args)
  "Lazily set `lab-token' when first needed."
  (unless lab-token
    (let ((user (if (file-exists-p "/etc/chasecaleb-work")
                    "cchase"
                  "chasecaleb")))
      ;; Re-using glab CLI token is a little gross in theory, but whatever.
      (setq lab-token (auth-source-pick-first-password :host "glab" :user user)))))

(defun cc/git--lab-init ()
  ;; Intentionally not setting `lab-group' because work group has too many projects to be useful.
  (setq lab-host "https://gitlab.com")
  ;; `lab-token' is conveniently only used by `lab--request'.
  (advice-add #'lab--request :before #'cc/git--lab-token-advice))

(provide 'cc-git)
