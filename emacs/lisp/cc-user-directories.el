;;; -*- lexical-binding: t -*-
(eval-and-compile
  (defconst cc/user-cache (expand-file-name "~/.cache/emacs"))
  (defconst cc/user-source-repo (expand-file-name "~/code/emacs.nix/emacs"))
  (defconst cc/user-config-repo (expand-file-name "~/code/emacs.nix/emacs/config"))
  ;; Entries at the END of `cc/user-repo-search-dirs' will be at the start of list for e.g.
  ;; `cc/completions-project-switch'.
  (defconst cc/user-repo-search-dirs '("~/nix-inputs/all" "~/" "~/code")))

(defun cc/user-directories-init ()
  (unless (file-exists-p cc/user-cache)
    (make-directory cc/user-cache t)))

(provide 'cc-user-directories)
