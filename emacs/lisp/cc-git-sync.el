;;; -*- lexical-binding: t -*-
(require 'subr-x)
(require 'cc-utils)
(require 'cc-file-auto-save)

(defvar cc/git-sync-success-hook nil
  "Called after successful git sync.")

(defvar cc/git-sync--did-enable nil
  "To prevent duplicate timers")

(defvar cc/git-sync--last 0
  "Time of last sync (0 indicates no sync has occurred yet).")

(defvar cc/git-sync--scheduled nil
  "Non-nil if sync is scheduled (for the next idle period)")

(defun cc/git-sync--changes-p ()
  (cc/with-home-dir
    ;; It would be smart to check all synced repos for changes, but org-files is the only one that I
    ;; care about quickly syncing due to conflict potential so whatever.
    (not (string-empty-p (shell-command-to-string "cd ~/org-files && git status --porcelain .")))))

;; Uber kludge: uncomment and eval on the rare occasion that I want to disable syncing.
;; (defun cc/git-sync--do-schedule (&rest _args)
;;   (message "WARNING: git sync disabled temporarily"))
(defun cc/git-sync--do-schedule (minimum-sync-time idle-time)
  "Schedule sync for next idle if enough time has elapsed since last sync.

Times are in seconds. MINIMUM-SYNC-TIME is minimum time in between consecutive
syncs (unless there are local changes), IDLE-TIME is minimum idle time for a
sync to occur. Sync will occur once both times have elapsed."
  (unless cc/git-sync--scheduled
    (let ((delta (- (float-time (current-time)) cc/git-sync--last)))
      (when (or (> delta minimum-sync-time)
                ;; Sync regardless of `minimum-sync-time' if there are local org file changes.
                (cc/git-sync--changes-p))
        ;; Running when next idle instead of immediately has two benefits:
        ;; 1. Less user impact (although this is not significant)
        ;; 2. Makes it more likely that git commit will happen at a logical-ish point.
        ;; Of course note that this will occur immediately if already idle for a long enough time.
        (setq cc/git-sync--scheduled t)
        (run-with-idle-timer idle-time nil #'cc/git-sync--execute-async)))))

(defun cc/git-sync--execute-async ()
  "Run git sync in background."
  (cc/file-auto-save-do-save)
  (cc/with-home-dir
    (set-process-sentinel
     (start-process-shell-command "cc-run-git-sync" "*git-sync-background*"
                                  "cc-run-git-sync")
     (lambda (_process change)
       (when (equal change "finished\n")
         (run-hooks 'cc/git-sync-success-hook))
       (setq cc/git-sync--scheduled nil)
       (setq cc/git-sync--last (float-time (current-time)))))))

(defun cc/git-sync-execute ()
  "Run git sync in foreground (blocking)."
  (interactive)
  (cc/file-auto-save-do-save)
  ;; Don't need to notify about failure because the sync script already does.
  (cc/with-home-dir
    (call-process-shell-command
     (concat "notify-send --urgency low --expire-time 1000 'Running git sync...' && "
             "cc-run-git-sync --no-wait && "
             "notify-send --urgency low --expire-time 3000 'Sync succeeded'")
     nil
     (get-buffer-create "*git-sync-blocking*")))
  (run-hooks 'cc/git-sync-success-hook))

(global-set-key (kbd "s-g") #'cc/git-sync-execute)

(defun cc/git-sync-setup ()
  ;; Do sync immediately as a one-off, periodically on timer, and on shutdown.
  (unless cc/git-sync--did-enable
    (setq cc/git-sync--did-enable t)
    (if after-init-time
        (cc/git-sync--execute-async)
      (add-hook 'after-init-hook #'cc/git-sync--execute-async))
    (cc/run-with-idle-timer-repeating 15 #'cc/git-sync--do-schedule (* 60 10) 30)
    (add-hook 'kill-emacs-hook #'cc/git-sync-execute)))

(provide 'cc-git-sync)
