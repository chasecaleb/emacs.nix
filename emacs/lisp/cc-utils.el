;;; -*- lexical-binding: t -*-
(require 'help-mode)


;;; Miscellaneous

(defmacro cc/csetq (sym value)
  "Like `setq', but aware of setters on custom variables.

Taken from https://with-emacs.com/posts/tutorials/almost-all-you-need-to-know-about-variables/"
  `(funcall (or (get ',sym 'custom-set)
                'set-default)
            ',sym ,value))

(defmacro cc/with-eval-after-load (feature &rest body)
  "`with-eval-after-load' without the byte-compiler brain damage.

Avoids blatant false-positive free variable/function warnings. Inspired by
https://github.com/tarao/with-eval-after-load-feature-el"
  (declare (indent 1) (debug t))
  `(with-eval-after-load ,feature
     (eval-when-compile
       (require ,feature))
     ,@body))

(defmacro cc/with-home-dir (&rest body)
  "Execute BODY with `default-directory' bound to ~.

Particularly important for global keybindings of shell command functions such as
`shell-command' and `shell-command-to-string' which aren't intended to run
against the current buffer. This prevents edge cases such as if the current
buffer is in a directory that no longer exists or is a remote Tramp file."
  (declare (debug t))
  `(let ((default-directory (expand-file-name "~")))
     ,@body))

(defun cc/run-with-idle-timer-repeating (secs fn &rest args)
  "Inspired by `run-with-idle-timer', but repeats repeats indefinitely during an
idle session instead of running only (at most) once per idle period.

LIMITATION: The timer returned is only used once, not repeatedly, since a new
timer is created for every repeat. This means the only way to cancel is by
manipulating `timer-idle-list'. This could be worked around, but I haven't had a
reason to deal with it for my use cases."
  (run-with-idle-timer secs nil #'cc/run-with-idle-timer-repeating--decorator secs fn args))

(defun cc/run-with-idle-timer-repeating--decorator (secs fn args)
  (if (> (or (float-time (current-idle-time)) 0) secs)
      (progn
        (apply fn args)
        (run-with-timer secs nil #'cc/run-with-idle-timer-repeating--decorator secs fn args))
    (run-with-idle-timer secs nil #'cc/run-with-idle-timer-repeating--decorator secs fn args)))

;; https://emacs.stackexchange.com/a/54502
(defmacro cc/with-advice (original-fn where advice-fn &rest body)
  "Execute BODY with advice temporarily enabled."
  (declare (indent 3))
  `(let ((fn-advice-var ,advice-fn))
     (unwind-protect
         (progn
           (advice-add ,original-fn ,where fn-advice-var)
           ,@body)
       (advice-remove ,original-fn fn-advice-var))))

(defun cc/silence-messages-advice (fn &rest args)
  "Silence messages in echo area and *Messages* buffer.

Use this as :around advice on a noisy function. Set variable `debug-on-message'
to figure out which function to silence."
  (let ((inhibit-message t)
        (message-log-max nil))
    (apply fn args)))

(defmacro cc/with-minibuffer-input (input &rest body)
  "Prefill minibuffer within BODY with INPUT string."
  (declare (indent 1))
  `(minibuffer-with-setup-hook
       (lambda ()
         (cc/simulate-input ,input))
     ,@body))

(defun cc/simulate-input (input)
  "Simulate input events using `unread-command-events'.

INPUT may be a string or list of chars"
  (setq unread-command-events
        (append unread-command-events (if (stringp input)
                                          (string-to-list input)
                                        input))))


;;; Buffers and windows

(defun cc/switch-to-buffer-with-display-actions-advice (fn &rest args)
  "Make `switch-to-buffer' behave identically to `pop-to-buffer'."
  ;; You would think that `switch-to-buffer-obey-display-actions' would solve this, but no. Setting
  ;; that makes `switch-to-buffer' call `pop-to-buffer-same-window' instead of `pop-to-buffer',
  ;; which specifies `display-buffer--same-window-action' as the display action. That means that
  ;; `display-buffer-alist' config is taken into consideration `display-buffer-alist' and
  ;; `display-buffer-overriding-action' but not `display-buffer-base-action', so it's only a partial
  ;; solution.
  (cl-letf (((symbol-function 'switch-to-buffer)
             (lambda (buffer &optional norecord _force-same-window)
               (pop-to-buffer buffer nil norecord))))
    (apply fn args)))

(defmacro with-no-new-buffers (&rest body)
  "Run BODY, and kill any new buffers created.

Any buffers that are killed will also be saved. Returns result of BODY."
  `(let ((current-buffers (buffer-list)))
     (unwind-protect
         (progn ,@body)
       (mapc (lambda (buf)
               (unless (member buf current-buffers)
                 (with-current-buffer buf
                   (when buffer-file-name (save-buffer))
                   (kill-buffer))))
             (buffer-list)))))

(defun cc/atomic-change-group-multi-inner (files fn)
  (if files
      (let ((existing-buffer (find-buffer-visiting (car files))))
        (with-current-buffer (or existing-buffer
                                 (find-file-existing (car files)))
          (atomic-change-group
            (cc/atomic-change-group-multi-inner (cdr files) fn))))
    (funcall fn)))

(defmacro cc/atomic-change-group-multi (files &rest body)
  "Recursively calls `atomic-change-group' for each of FILES, then executes BODY.

This makes FN atomic *so long as* it only operates on (a subset of) FILES."
  (declare (indent 1) (debug true))
  `(let ((original (current-buffer)))
     (with-no-new-buffers
      (cc/atomic-change-group-multi-inner
       ,files
       (lambda ()
         ;; inner function modifies current-buffer, so restore it to match what caller expects.
         (with-current-buffer original
           ,@body))))))


;;; Keymaps and `define-key'

(defun cc/define-key-list (map key-list)
  (dolist (it key-list)
    (let ((key (if (stringp (car it))
                   (kbd (car it))
                 ;; Handle "[remap foo]"
                 (car it))))
      (define-key map key (cdr it))))
  ;; Return map for easy usage with `defvar'.
  map)

(defun cc/describe-keymap (keymap)
  "Describe a keymap using `substitute-command-keys'.

Taken from: https://stackoverflow.com/a/36994486"
  (interactive
   (list (completing-read
          "Keymap: " (let (maps)
                       (mapatoms (lambda (sym)
                                   (and (boundp sym)
                                        (keymapp (symbol-value sym))
                                        (push sym maps))))
                       maps)
          nil t)))
  (with-output-to-temp-buffer (format "*keymap: %s*" keymap)
    (princ (format "%s\n\n" keymap))
    (princ (substitute-command-keys (format "\\{%s}" keymap)))
    (with-current-buffer standard-output ; temp buffer
      (setq help-xref-stack-item (list #'cc/describe-keymap keymap)))))


;;; Indentation

(defun cc/indent-buffer ()
  "Indent entire buffer and remove trailing whitespace."
  (interactive "*")
  (save-excursion
    (indent-region (point-min) (point-max) nil)
    (delete-trailing-whitespace)))
(global-set-key [f12] 'cc/indent-buffer)

(defvar cc/indent-region-disabled-modes '(yaml-mode)
  "List of modes that `indent-region' should be a no-op for.

This works correctly for org-babel source blocks, too.")

(defun cc/indent-region-advice (region-fn &rest region-args)
  (cc/with-advice #'indent-according-to-mode
      :around (lambda (inner-fn &rest inner-args)
                (unless (apply #'derived-mode-p cc/indent-region-disabled-modes)
                  (apply inner-fn inner-args)))
    (apply region-fn region-args)))

(advice-add #'indent-region :around #'cc/indent-region-advice)

(provide 'cc-utils)
