;;; -*- lexical-binding: t -*-
(require 'ispell)
(require 'spell-fu)
(require 'cc-user-directories)
(require 'cc-utils)

(defun cc/spelling-init ()
  (setq ispell-program-name "/run/current-system/sw/bin/aspell")
  (setq ispell-dictionary "en_US")
  (setq ispell-personal-dictionary (expand-file-name "ispell-dictionary" cc/user-config-repo))
  ;; spell-fu's cache once hid a dictionary problem from me for an indeterminate amount of time, so
  ;; now I burn it with a fire by putting it in /tmp.
  (setq spell-fu-directory "/tmp/spell-fu")
  (set-face-attribute 'spell-fu-incorrect-face nil :underline '(:style wave :color "OrangeRed3"))
  (cc/define-key-list global-map
                      `(("C-, C-," . ,#'spell-fu-goto-next-error)
                        ("C-, n"   . ,#'spell-fu-goto-next-error)
                        ("C-, C-n" . ,#'spell-fu-goto-next-error)
                        ("C-, p"   . ,#'spell-fu-goto-previous-error)
                        ("C-, C-p" . ,#'spell-fu-goto-previous-error)
                        ("C-, a"   . ,#'spell-fu-word-add)
                        ("C-, r"   . ,#'spell-fu-word-remove)
                        ("C-, i"   . ,#'ispell-word)))
  (spell-fu-global-mode)
  ;; Don't spellcheck read-only buffers.
  (setq spell-fu-global-ignore-buffer (lambda (buf) (buffer-local-value 'buffer-read-only buf)))
  (setq spell-fu-ignore-modes '(vterm-mode
                                exwm-mode
                                dired-mode
                                minibuffer-mode))
  (setq-default spell-fu-faces-exclude '(font-lock-constant-face))
  (add-hook 'cc/source-code-hook #'cc/spelling--source-code-setup)
  (add-hook 'org-mode-hook #'cc/spelling--org-mode-setup)
  (add-hook 'markdown-mode-hook #'cc/spelling--markdown-mode-setup))

(defun cc/spelling--source-code-setup ()
  ;; `spell-fu-mode-enable' magically sets `spell-fu-faces-include' to comments, docs, and strings
  ;; if it is not already set. Spell checking strings is incredibly obnoxious in my opinion, because
  ;; there are way too many strings whose contents are not intended to be proper English prose.
  (setq-local spell-fu-faces-include '(font-lock-comment-face font-lock-doc-face)))

(defun cc/spelling--org-mode-setup ()
  (setq spell-fu-faces-exclude
        '(org-block-begin-line
          org-block-end-line
          org-code
          org-date
          org-drawer
          org-document-info-keyword
          org-ellipsis
          org-link
          org-meta-line
          org-properties
          org-properties-value
          org-special-keyword
          org-src
          org-tag
          org-verbatim
          cc/org-link-web-face
          cc/org-link-file-remote-face
          cc/org-link-file-invalid-face
          cc/org-link-org-id-face)))

(defun cc/spelling--markdown-mode-setup ()
  (setq spell-fu-faces-exclude
        '(markdown-url-face
          markdown-code-face
          markdown-inline-code-face)))

(provide 'cc-spelling)
