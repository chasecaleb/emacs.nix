;;; -*- lexical-binding: t; byte-compile-warnings: (not docstrings) -*-
;; Docstring warnings disabled (above) due to hydra generating long ones.
(require 'org)
(require 'org-agenda)
(require 'hydra)
(require 'cc-utils)
(require 'cc-file-auto-save)
(require 'cc-org-utils)

(defun cc/org-gtd-process-start (&optional arg)
  "Start processing inbox items and loop until complete.

With C-u C-u prefix ARG: go to inbox without processing.
With single C-u prefix ARG: process item at point."
  (interactive "P")
  (if (equal arg '(4))
      (unless (derived-mode-p 'org-mode)
        (user-error "Not in an org file, cannot process item at point"))
    (cc/org-gtd--find-file-or-window (expand-file-name "inbox.org" "~/org-files/gtd"))
    (widen)
    (unless (equal arg '(16))
      (goto-char (point-min))))
  ;; Make sure blank lines are dealt with before starting capture, to avoid risk of capture/refile
  ;; marker issues.
  (cc/org-format-blank-lines)
  (unless (equal arg '(16))
    (cc/org-gtd--process-next-item-view)
    (if (org-at-heading-p)
        (cc/org-gtd--process-hydra/body)
      (message "No tasks to process"))))

(defun cc/org-gtd--switch-to-buffer-or-window (buffer-or-name &optional norecord)
  "`select-window' if already visible, otherwise `switch-to-buffer'"
  (if-let ((w (get-buffer-window buffer-or-name)))
      (select-window w norecord)
    (switch-to-buffer buffer-or-name norecord)))

(defun cc/org-gtd--find-file-or-window (filename &optional wildcards)
  "Like `find-file', but switch to window if already visible."
  (let ((value (find-file-noselect filename nil nil wildcards)))
    (if (listp value)
        (mapcar #'cc/org-gtd--switch-to-buffer-or-window (nreverse value))
      (cc/org-gtd--switch-to-buffer-or-window value))))

(defun cc/org-gtd--process-next-item-view ()
  (if (org-before-first-heading-p)
      (org-forward-heading-same-level nil)
    (org-back-to-heading))
  (when (org-at-heading-p) ; Inbox may be empty
    (org-narrow-to-subtree)
    (org-fold-show-all)))

(defhydra cc/org-gtd--process-hydra
  (:color red
   ;; before-exit runs once after hydra exits
   :before-exit (widen)
   ;; after-exit runs after each command (and *before* :before-exit, absurdly enough)
   :after-exit (unless (org-at-heading-p)
                 ;; This only works in :after-exit, not in the command itself.
                 (hydra-keyboard-quit)
                 (message "Finished processing all tasks")))
  ("t" (cc/org-gtd--process-item 'task)    "Task")
  ("l" (cc/org-gtd--process-item 'backlog) "Backlog")
  ("d" (cc/org-gtd--process-item 'done)    "Done")
  ("k" (cc/org-gtd--process-item 'kill)    "Kill")
  ("C-g" nil nil)
  ("RET" nil nil))

(defun cc/org-gtd--process-item (type)
  ;; Need to inhibit auto save (triggered by e.g. window or buffer change) because that may
  ;; cause org file to be reformatted, which throws off the marker used by
  ;; `org-refile-goto-last-stored'. See `cc/org-on-save', which is a buffer-local
  ;; `before-save-hook'.
  (cc/with-inhibited-file-auto-save
   (cc/atomic-change-group-multi (delete-dups ; parse refile targets to get unique file list.
                                  (seq-map
                                   (apply-partially #'nth 1)
                                   (org-refile-get-targets)))
     (save-window-excursion
       ;; Org tag/todo fast selection deletes other windows and I can't change that without a lot
       ;; of work, so instead do it pre-emptively so that the window layout doesn't change
       ;; partway through (which is obviously disorienting).
       (delete-other-windows)

       (save-excursion
         (pcase type
           ('task
            ;; Don't modify todo state if already set to avoid resetting an in-progress task.
            (unless (org-get-todo-state)
              (org-todo "TODO"))
            (cc/org-gtd--process-task-inner))

           ((or 'done 'backlog)
            (org-todo (upcase (symbol-name type)))
            (cc/org-gtd--process-task-inner))

           ('kill
            (org-cut-subtree)
            (message "Item killed"))

           (_ (error "Unknown process item type: %s" type)))))
     ;; Cleanup the original buffer (e.g. inbox, not the refile target)
     (widen)
     (delete-blank-lines)
     (org-next-visible-heading 1)
     (cc/org-gtd--process-next-item-view))))

(defun cc/org-gtd--process-task-inner ()
  (condition-case nil ; allow processing in place (instead of refiling)
      (progn
        (org-refile)
        (org-refile-goto-last-stored))
    (quit))
  (save-restriction ; this is the refile target, not the inbox/source
    (org-narrow-to-subtree)
    (org-priority)
    (cc/org-gtd--set-tag-ensure-context)
    (org-set-effort)
    ;; Deadline/scheduled dates are optional, so use C-g to leave as-is.
    (condition-case nil
        (org-deadline nil)
      (quit))
    (condition-case nil
        (org-schedule nil)
      (quit))))

(defun cc/org-gtd--set-tag-ensure-context ()
  (cl-loop do (progn
                (org-set-tags-command))
           until (cc/org-tags-find-context (org-get-tags))
           do (read-key "An @context tag is required (press any key to retry)")))

(provide 'cc-org-gtd)
