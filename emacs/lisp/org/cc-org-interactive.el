;;; -*- lexical-binding: t -*-
;;; Interface/interactive org things.
;; TODO not sure that I like this file name/organization - revisit after extracting rest of my org
;; stuff from kitchen sink.
(require 'org-ql)
(require 'org-ql-search)
(require 'vertico)
(require 'cc-utils)
(require 'cc-org-utils)

;; I don't entirely understand why I need to copy-pasta this from org-ql, but it seems to be
;; necessary to include here to avoid the following compile error:
;; the function `org-ql--normalize-query' might not be defined at runtime.
(declare-function org-ql--normalize-query "org-ql" t t)

(defun cc/org-interactive-init ()
  (global-set-key (kbd "s-n") #'cc/org-ql-find-gtd)
  (define-key org-mode-map (kbd "C-c ^") #'cc/org-sort))

(defun cc/org-sort (&optional prefix)
  "`org-sort' by todo state, then by priority within each state.

With prefix arg or when in a table or item: do normal `org-sort' (without
prefix)."
  (interactive "P")
  (if (or prefix
          (org-at-table-p)
          (org-at-item-p))
      (org-sort nil)
    ;; Could also reinvent the wheel and write a custom sort function, but delegating to existing
    ;; sort implementations is easier.
    ;;
    ;; ?o is todo state, ?p is priority. Sorting by priority first then state looks backwards, but
    ;; it isn't.
    (org-sort-entries nil ?p)
    (org-sort-entries nil ?o)))

(defun cc/org-ql-find-gtd ()
  "`org-ql-find' for GTD files.

String without a predicate defaults to \"rifle:\", which matches anywhere in
entry (not just heading). Can be customized with `org-ql-default-predicate'.

Some useful query predicates (non-exhaustive):
- h: match against heading (instead of entire content)
- h*: for heading regexp
- r: regexp against entire entry (not just heading)
- olp: to match outline path. Must match *all* elements as substring and in any
  order.
- ancestors: match against *heading* of any ancestors.
- todo: any non-done todo if just \"todo:\", or *any* matching keyword if
  \"todo:FOO,BAR\".
- tags: match any tag if just \"tags:\" or at least one matching tag if
  \"tags:foo,bar\"
- priority: heading with *any* matching priority.
- habit
- src: any babel source block, or src:sh for a specific language.

Predicates generally accept a comma-separated list, e.g. \"todo:PENDING,BLOCKED\",
which means to match a heading with PENDING *OR* BLOCKED keyword. Use
double-quotes to surround arguments with spaces.

See org-ql info page for full query syntax and list of predicates, or identical
readme at https://github.com/alphapapa/org-ql/#queries"
  (interactive)
  (org-ql-find (apply #'append (seq-map #'cc/get-org-files '("gtd" "gtd/areas")))))

(provide 'cc-org-interactive)
