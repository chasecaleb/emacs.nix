;;; -*- lexical-binding: t -*-
(require 'f)
(require 's)
(require 'org-clock)
(require 'org-element)

;;; Files
(defmacro cc/when-org-files (&rest body)
  (declare (debug t))
  `(when (f-directory-p (expand-file-name "~/org-files"))
     ,@body))

(defun cc/get-org-files (dir)
  "Return org files in DIR relative to org files root, non-recursively."
  (directory-files (expand-file-name dir "~/org-files") t (rx ".org" (? ".gpg") string-end)))

(defun cc/org-format-blank-lines ()
  "Ensure blank line between headings of current buffer.

`org-blank-before-new-entry' only works in some scenarios, such as `org-return'
command, but that leaves a lot of gaps such as `org-refile', creating a new
heading manually, and editing outside of Emacs (e.g. organice)."
  ;; Not using `org-map-entries' because it calls `org-agenda-prepare' which shows an obnoxious
  ;; "Non-existent agenda file" prompt when finalizing an org capture if the file doesn't exist.
  ;; This is particularly annoying for org-roam captures, since those generally create a new file.
  ;;
  ;; So anyways, this implementation is based on the minimal necessary bit of `org-map-entries'.
  (save-excursion
    (save-restriction
      (widen)
      (goto-char (point-min))
      (org-scan-tags (lambda ()
                       (unless (or (looking-back "\n\n" nil)
                                   ;; Leave first heading of buffer alone.
                                   (and (equal (org-current-level) 1)
                                        (org-first-sibling-p)))
                         (insert "\n")))
                     t nil))))


;;; Tags
(defconst cc/org-tags-context-alist
  '(("@computer" . ?1)
    ("@reading" . ?2)
    ("@reference" . ?3)
    ("@people" . ?4)
    ("@house" . ?5)
    ("@away" . ?6)))  ; away = outside the house

(defun cc/org-tags-find-context (tags)
  (seq-filter
   (lambda (next)
     (assoc next cc/org-tags-context-alist))
   tags))


;;; Clocking
(defun cc/org-clock-ticket-p ()
  (not (s-blank-p (cc/org-clock--get-ticket))))

(defun cc/org-clock--get-ticket ()
  (when (org-clocking-p)
    (let ((heading-text (save-window-excursion
                          (save-excursion
                            (org-clock-goto)
                            (org-element-property :raw-value (org-element-at-point))))))
      (when (string-match (rx "jira" (+? (not whitespace))  "/browse/"
                              (group (+ alphanumeric) "-" (+ digit)))
                          heading-text)
        (upcase (match-string 1 heading-text))))))

(defun cc/org-clock-insert-ticket (&optional suffix)
  "Insert Jira ticket ID from headline of currently clocked task.

Non-nil SUFFIX is inserted after ticket id."
  (interactive)
  (when-let ((ticket (cc/org-clock--get-ticket)))
    (if (and (derived-mode-p 'vterm-mode)
             (fboundp 'vterm-insert))
        (vterm-insert ticket)
      (insert ticket))
    (when suffix (insert suffix))))

(defun cc/org-clock-insert-ticket-url ()
  "Like `cc/org-clock-insert-ticket', but insert URL instead of plain ticket ID."
  (interactive)
  (when-let ((ticket (cc/org-clock--get-ticket)))
    (insert "https://jira.naic.org/browse/" ticket)))

(defun cc/org-clock-browse-ticket ()
  (interactive)
  (when-let ((ticket (cc/org-clock--get-ticket)))
    (browse-url (concat "https://jira.naic.org/browse/" ticket))))

(provide 'cc-org-utils)
