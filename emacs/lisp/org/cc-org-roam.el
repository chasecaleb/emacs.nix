;;; -*- lexical-binding: t; byte-compile-warnings: (not obsolete) -*-
;; FIXME Make a PR to fix obsolete warnings - https://github.com/skeeto/emacs-web-server/issues/27
(require 'org-roam)
(require 'org-roam-graph)
(require 'org-roam-protocol) ;; Registers with `org-protocol-protocol-alist' on load.
(require 'org-roam-ui)
(require 'org-id)
(require 'cc-user-directories)
(require 'cc-utils)
(require 'cc-git-sync)

(defun cc/org-roam--keybinds ()
  (cc/define-key-list
   global-map
   `(("C-c n f"   . ,#'org-roam-node-find)
     ("C-c n c"   . ,#'org-roam-capture)
     ;; Moves subtree at point to a new roam file.
     ("C-c n e"   . ,#'org-roam-extract-subtree)
     ;; Refiles to a roam node (as opposed to `org-refile' which uses `org-refile-targets').
     ("C-c n w"   . ,#'org-roam-refile)
     ("C-c n g g" . ,#'org-roam-ui-open)
     ("C-c n g z" . ,#'org-roam-ui-node-zoom)
     ("C-c n g l" . ,#'org-roam-ui-node-local)
     ("C-c n i"   . ,#'org-roam-node-insert)
     ("C-c n l"   . ,#'org-roam-buffer-toggle))))

(defun cc/org-roam-init ()
  "See also: https://www.orgroam.com/manual/"
  (cc/org-roam--keybinds)
  (setq org-roam-directory "~/org-files/roam")
  ;; Don't want db file in my org directory, which is synced across computers.
  (setq org-roam-db-location (expand-file-name "org-roam.db" cc/user-cache))
  (advice-add #'org-roam-node-slug :filter-return #'cc/org-roam--title-to-slug)

  (let ((roam-file-template "${slug}.org"))
    (setq org-roam-extract-new-file-path roam-file-template)
    (setq org-roam-capture-templates
          `(("d" "default" plain "%?"
             :if-new (file+head ,roam-file-template
                                "#+title: ${title}\nSee also:")
             :unnarrowed t))))
  (setq org-roam-node-display-template "${title}")

  (setq org-roam-graph-extra-config '(("rankdir" . "RL")))
  ;; The graph is wayyyy too noisy if these are shown. I want to see relationships between notes,
  ;; not all the things in the outside world that I reference/link to.
  (setq org-roam-graph-link-hidden-types '("file" "http" "https" "help" "info" "man"))

  (org-roam-db-autosync-mode)
  ;; Calling `org-roam-db-sync' twice in quick succession causes a (minor-but-annoying) message when
  ;; connection is garbage collected: "finalizer failed: (wrong-type-argument sqlitep nil)". Since
  ;; `org-roam-db-autosync-mode' calls `org-roam-db-sync', we want to skip an extraneous sync during
  ;; this initial call of `cc/org-roam--refresh'. This also (marginally?) improves startup time
  ;; since `org-roam-db-sync' is expensive.
  (cc/org-roam--refresh 'skip)
  (add-hook 'cc/git-sync-success-hook #'org-roam-db-sync)
  (add-hook 'cc/git-sync-success-hook #'cc/org-roam--refresh)

  ;; `org-roam-ui' config - don't need to enable `org-roam-ui-mode' because `org-roam-ui-open' will
  ;; do so automatically.
  ;;
  ;; *REMINDER*: customize web ui settings:
  ;; - visual -> graph coloring: communities
  ;; - behavior:
  ;;   - preview node: never
  ;;   - open in emacs: click
  ;;   - expand node: right click
  ;; https://github.com/org-roam/org-roam-ui/issues/231
  ;; https://github.com/org-roam/org-roam-ui/issues/223
  (setq org-roam-ui-open-on-start nil)
  (diminish 'org-roam-ui-mode)
  (diminish 'org-roam-ui-follow-mode))

(defun cc/org-roam--title-to-slug (title)
  "Make `org-roam-node-slug' use hyphens instead of underscores."
  (replace-regexp-in-string "_" "-" title))

(defun cc/org-roam--refresh (&optional skip-db-sync)
  (unless skip-db-sync
    (org-roam-db-sync))
  (when (file-exists-p org-roam-directory)
    ;; I don't use `org-agenda-text-search-extra-files' currently, but by default they're included
    ;; in `org-id-extra-files'. It makes sense to keep it that way.
    (setq org-id-extra-files (append org-agenda-text-search-extra-files
                                     (org-roam-list-files)))))

(provide 'cc-org-roam)
