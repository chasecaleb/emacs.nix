;;; -*- lexical-binding: t -*-
(require 'json-ts-mode)
(require 'cc-source-code)

(defun cc/json-init ()
  (add-to-list 'major-mode-remap-alist '(js-json-mode . json-ts-mode))
  (cc/source-code-enable-eglot 'json-ts-mode))

(provide 'cc-json)
