;;; -*- lexical-binding: t -*-
(require 'sh-script)
(require 'shfmt)
(require 'cc-source-code)

(defun cc/sh-init ()
  ;; I use ZSH for interactive terminal session, but always script in Bash.
  (setq sh-shell-file "/usr/bin/bash")
  (setq sh-indent-after-continuation 'always)
  (add-to-list 'major-mode-remap-alist '(sh-mode . bash-ts-mode))
  ;; bash-language-server is bash only, so enable only for `bash-ts-mode' instead of `sh-base-mode'.
  (cc/source-code-enable-eglot 'bash-ts-mode)
  (add-hook 'sh-base-mode-hook #'cc/sh--mode-setup)
  ;; shfmt doesn't support zsh, so don't use with `bash-ts-mode'.
  (define-key bash-ts-mode-map (kbd "<f12>") #'shfmt-buffer)
  (define-key bash-ts-mode-map (kbd "C-M-\\") #'shfmt-region)
  (advice-add #'shfmt-buffer :around #'cc/shfmt-arguments-advice)
  (advice-add #'shfmt-region :around #'cc/shfmt-arguments-advice))

(defun cc/sh--mode-setup ()
  (when (derived-mode-p 'sh-mode)
    ;; shfmt doesn't support zsh, so don't use with `bash-ts-mode'.
    (shfmt-on-save-mode))
  (sh-electric-here-document-mode -1) ; This is a horrible default thanks to e.g. <<< syntax
  (add-hook 'after-save-hook #'executable-make-buffer-file-executable-if-script-p nil 'local)
  ;; Use eglot/bash-language-server's shellcheck, not flymake backend, since I don't need to run it
  ;; twice. Also I don't want to do shellcheck for zsh.
  (remove-hook 'flymake-diagnostic-functions #'sh-shellcheck-flymake t))

(defun cc/shfmt-arguments-advice (fn &rest args)
  "Set default shfmt args if there isn't a .editorconfig.

Use .editorconfig settings when a config file exists, otherwise provide default
`shfmt-arguments'. I could create /.editorconfig (yes, all the way up in root)
but that's... gross."
  (if (locate-dominating-file default-directory ".editorconfig")
      (apply fn args)
    ;; Set defaults *only if* `shfmt-arguments' not already set (e.g. by a buffer-local).
    (let ((shfmt-arguments (or shfmt-arguments
                               '("--indent" "4" "--case-indent" "--binary-next-line"))))
      (apply fn args))))

(provide 'cc-sh)
