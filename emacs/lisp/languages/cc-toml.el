;;; -*- lexical-binding: t -*-
(require 'toml-ts-mode)

(defun cc/toml-init ()
  (add-to-list 'major-mode-remap-alist '(conf-toml-mode . toml-ts-mode)))

(provide 'cc-toml)
