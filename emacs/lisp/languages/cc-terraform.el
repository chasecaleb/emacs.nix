;;; -*- lexical-binding: t -*-
(require 'terraform-mode)
(require 'cc-source-code)

(defun cc/terraform-init ()
  (add-to-list 'eglot-server-programs '(terraform-mode . ("terraform-ls" "serve")))
  (cc/source-code-enable-eglot 'terraform-mode :auto-format t))

(provide 'cc-terraform)
