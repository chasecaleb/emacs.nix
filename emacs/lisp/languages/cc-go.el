;;; -*- lexical-binding: t -*-
(require 'eglot)
(require 'go-mode)
(require 'cc-source-code)

;; FIXME/TODO verify server settings work - untested since migrating from lsp-mode
;; TODO use tree-sitter `go-ts-mode' and `go-mod-ts-mode'
(defun cc/go-init ()
  ;; gopls eglot config info:
  ;; - https://cs.opensource.google/go/x/tools/+/refs/tags/gopls/v0.13.2:gopls/doc/emacs.md
  ;; For analyzer info, see:
  ;; - https://github.com/golang/tools/blob/master/gopls/doc/settings.md#analyses
  ;; - https://github.com/golang/tools/blob/master/gopls/doc/analyzers.md
  ;; Analyzer config specified here is merged with gopls defaults, so anything not specified here
  ;; will use the default. So in other words, I'm just adding/configuring additional analyzers (not
  ;; implicitly turning off all the defaults).
  (cc/source-code-enable-eglot 'go-mode
                               :auto-format t
                               :config '(:gopls (:analyses '((nilness . t)
                                                             (shadow . t)
                                                             (unusedparams . t)
                                                             (unusedwrite . t)
                                                             (implementmissing . t)))))
  (add-hook 'go-mode-hook #'cc/go-mode-setup))

(defun cc/go-mode-setup ()
  ;; -50 to run before formatter configured by `cc/source-code-enable-eglot'.
  (add-hook 'before-save-hook #'cc/go-organize-imports -50 t))

(defun cc/go-organize-imports ()
  (interactive)
  ;; From https://github.com/joaotavora/eglot/issues/574#issuecomment-1401023985
  (eglot-code-actions nil nil "source.organizeImports" t))

(provide 'cc-go)
