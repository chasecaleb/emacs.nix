;;; -*- lexical-binding: t -*-
(require 'eglot)
(require 'nix-mode)
(require 'cc-source-code)
(require 'cc-utils)

(defun cc/nix-init ()
  (add-to-list 'auto-mode-alist '("\\flake.lock\\'" . json-ts-mode))
  ;; LSP server config: https://github.com/oxalica/nil/blob/main/docs/configuration.md
  ;; NOT using nil.nix.flake.autoEvalInputs because it's way too resource-heavy (thanks, nixpkgs) to
  ;; be worth it.
  (cc/source-code-enable-eglot 'nix-mode
                               :auto-format t
                               :config '(:nil (:formatting (:command ["nixpkgs-fmt"])
                                               :nix (:flake (:autoArchive t)))))
  (cc/define-key-list nix-mode-map
                      `(("C-c h" . ,#'nix-flake)
                        ("<f12>" . ,#'eglot-format-buffer))))

(provide 'cc-nix)
