;;; -*- lexical-binding: t -*-
(require 'markdown-mode)
(require 'mermaid-mode)
(require 'edit-indirect) ;; Used to edit source blocks with "C-c '"

;; Markdown syntax reminder: GitLab and GitHub both follow CommonMark (with some extensions).
;; - https://spec.commonmark.org/
;; - https://docs.gitlab.com/ee/user/markdown.html
;;
;; Regarding list indentation and `markdown-indent-function': nested lists need to be indented to
;; the same level as the first non-list-marker character (i.e. the text content) of the parent --
;; CommonMark does NOT use a fixed 2 or 4 character indent like Gruber's original markdown.pl.
;; Examples:
;;   - A nested listed under "- foo" is "  - two leading spaces"
;;   - A nested list under "1. foo" is "   - three leading spaces"
;;   - A nested list under "-   three spaces after the dash" is "    - four leading spaces"
;;
;; Currently, markdown-mode's `markdown-cycle' (tab key) does the right thing for dynamic
;; indentation, but some other less-used commands like `markdown-demote' and `markdown-promote' use
;; a fixed width based on `markdown-list-indent-width'... So *don't* use those commands on lists.
;; See: https://github.com/jrblevin/markdown-mode/issues/630
(defun cc/markdown-init ()
  (add-to-list 'auto-mode-alist '("\\.md\\'" . gfm-mode))
  (add-hook 'markdown-mode-hook #'auto-fill-mode)
  (setq markdown-command '("cc-markdown"))
  (setq markdown-live-preview-delete-export 'delete-on-export)
  (setq markdown-disable-tooltip-prompt t) ;; Tooltip text on links is silly.
  (setq markdown-ordered-list-enumeration nil)
  (setq markdown-unordered-list-item-prefix "- ")
  ;; `markdown-italic-underscore' nil: explicit default as a reminder. CommonMark treats asterisks
  ;; and underscores equivalently, so stick with just asterisks. One asterisk/underscore corresponds
  ;; to html <em> (typically italics) and two asterisks/underscores.
  (setq markdown-italic-underscore nil)
  (setq markdown-asymmetric-header t) ; Adding # to end of headers is silly.
  (setq markdown-hr-strings '("---")) ; markdown-mode's hr hierarchy is meaningless in CommonMark.
  (add-to-list 'markdown-gfm-recognized-languages "mermaid"))

(provide 'cc-markdown)
