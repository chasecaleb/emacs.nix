;;; -*- lexical-binding: t -*-
(require 'flymake)
(require 'lisp-mode)
(require 'dumb-jump)
(require 'cc-user-directories)
(require 'cc-utils)

;; Shut byte compiler up. This same defvar exists in `lisp-mode' file.
(defvar calculate-lisp-indent-last-sexp)

(defvar-local cc/lisp--flymake-do-autoload nil)

(defun cc/lisp-init ()
  (add-hook 'emacs-lisp-mode-hook #'cc/lisp-mode-setup)
  (setq emacs-lisp-docstring-fill-column 80)
  (diminish 'eldoc-mode))

(defun cc/lisp-mode-setup ()
  (outline-minor-mode)
  (reveal-mode)
  (cc/lisp--flymake-setup))

;; Fixes stupid indentation of keyword lists.
;; Taken from:
;; https://github.com/Fuco1/.emacs.d/blob/af82072196564fa57726bdbabf97f1d35c43b7f7/site-lisp/redef.el#L20-L94
;; See also: https://emacs.stackexchange.com/questions/10230/how-to-indent-keywords-aligned
;;
;; before
;;   (:foo bar
;;         :baz qux)
;; after
;;   (:foo bar
;;    :baz qux)
(defun lisp-indent-function (indent-point state)
  "This function is the normal value of the variable `lisp-indent-function'.
The function `calculate-lisp-indent' calls this to determine
if the arguments of a Lisp function call should be indented specially.
INDENT-POINT is the position at which the line being indented begins.
Point is located at the point to indent under (for default indentation);
STATE is the `parse-partial-sexp' state for that position.
If the current line is in a call to a Lisp function that has a non-nil
property `lisp-indent-function' (or the deprecated `lisp-indent-hook'),
it specifies how to indent.  The property value can be:
* `defun', meaning indent `defun'-style
  \(this is also the case if there is no property and the function
  has a name that begins with \"def\", and three or more arguments);
* an integer N, meaning indent the first N arguments specially
  (like ordinary function arguments), and then indent any further
  arguments like a body;
* a function to call that returns the indentation (or nil).
  `lisp-indent-function' calls this function with the same two arguments
  that it itself received.
This function returns either the indentation to use, or nil if the
Lisp function does not specify a special indentation."
  (let ((normal-indent (current-column))
        (orig-point (point)))
    (goto-char (1+ (elt state 1)))
    (parse-partial-sexp (point) calculate-lisp-indent-last-sexp 0 t)
    (cond
     ;; car of form doesn't seem to be a symbol, or is a keyword
     ((and (elt state 2)
           (or (not (looking-at "\\sw\\|\\s_"))
               (looking-at ":")))
      (if (not (> (save-excursion (forward-line 1) (point))
                  calculate-lisp-indent-last-sexp))
          (progn (goto-char calculate-lisp-indent-last-sexp)
                 (beginning-of-line)
                 (parse-partial-sexp (point)
                                     calculate-lisp-indent-last-sexp 0 t)))
      ;; Indent under the list or under the first sexp on the same
      ;; line as calculate-lisp-indent-last-sexp.  Note that first
      ;; thing on that line has to be complete sexp since we are
      ;; inside the innermost containing sexp.
      (backward-prefix-chars)
      (current-column))
     ((and (save-excursion
             (goto-char indent-point)
             (skip-syntax-forward " ")
             (not (looking-at ":")))
           (save-excursion
             (goto-char orig-point)
             (looking-at ":")))
      (save-excursion
        (goto-char (+ 2 (elt state 1)))
        (current-column)))
     (t
      (let ((function (buffer-substring (point)
                                        (progn (forward-sexp 1) (point))))
            method)
        (setq method (or (function-get (intern-soft function)
                                       'lisp-indent-function)
                         (get (intern-soft function) 'lisp-indent-hook)))
        (cond ((or (eq method 'defun)
                   (and (null method)
                        (> (length function) 3)
                        (string-match "\\`def" function)))
               (lisp-indent-defform state indent-point))
              ((integerp method)
               (lisp-indent-specform method state
                                     indent-point normal-indent))
              (method
               (funcall method indent-point state))))))))


;;; Flymake
(defun cc/lisp--flymake-setup ()
  ;; `elisp-flymake-checkdoc' makes sense for public package/Emacs core contributors, but it
  ;; drives me crazy in my own init and when looking at other people's code.
  (remove-hook 'flymake-diagnostic-functions #'elisp-flymake-checkdoc t)

  ;; GROSS KLUDGES AHEAD: fix `load-path' and autoloads for my init lisp.
  (advice-add #'elisp-flymake-byte-compile :around #'cc/elisp-flymake--batch-compile-wrapper)
  (when (and buffer-file-name
             (file-in-directory-p buffer-file-name cc/user-source-repo))
    (setq-local cc/lisp--flymake-do-autoload t)
    (cc/flymake--load-path-setup)))

(defun cc/flymake--load-path-setup ()
  "Manipulate `elisp-flymake-byte-compile-load-path' for my Emacs init."
  ;; Have flymake check against source copy of my init instead of what's installed. This doesn't
  ;; perfectly replicate the load-path order since the position of my init isn't preserved, but
  ;; for this use case (static analysis) it's good enough.
  (setq-local elisp-flymake-byte-compile-load-path
              (let* ((filtered (seq-filter (lambda (dir)
                                             (not (string-match-p "/lisp/chasecaleb" dir)))
                                           load-path))
                     (source-root (expand-file-name "lisp" cc/user-source-repo))
                     (sources (cons source-root
                                    (seq-filter #'file-directory-p
                                                (directory-files-recursively source-root "" t)))))
                (append sources filtered))))

(defun cc/elisp-flymake--batch-compile-wrapper (fn &rest wrapper-args)
  ;; Note: this technically makes flymake slightly slower for any of my init files, but the
  ;; difference is extremely minor (like 0.001 seconds or less when testing on this buffer).
  (if cc/lisp--flymake-do-autoload
      (cc/with-advice #'make-process
          :filter-args (lambda (proc-args)
                         (let* ((cmd (plist-get proc-args :command))
                                (n (cl-position "-f" cmd :test #'equal)))
                           (push "--eval" (nthcdr n cmd))
                           (push "(progn (require 'cc-nixos-startup) (cc/nixos-startup--autoloads))" (nthcdr (1+ n) cmd)))
                         proc-args)
        (apply fn wrapper-args))
    (apply fn wrapper-args)))

(provide 'cc-lisp)
