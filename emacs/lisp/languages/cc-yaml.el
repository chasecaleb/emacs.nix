;;; -*- lexical-binding: t -*-
;; Miscellaneous LSP-related notes:
;; - Docs: readme of https://github.com/redhat-developer/yaml-language-server
;; - Magic comment syntax: # yaml-language-server: $schema=<url-or-path>
;;   See https://github.com/redhat-developer/yaml-language-server#using-inlined-schema
;; - Kubernetes schemas: https://github.com/yannh/kubernetes-json-schema
;;   Example:
;;   # yaml-language-server: $schema=https://raw.githubusercontent.com/yannh/kubernetes-json-schema/master/v1.20.15-standalone-strict/deployment.json
(require 'yaml-mode)
(require 'cc-source-code)

;; Do NOT use `yaml-ts-mode', because (at least in Emacs 29.1 on 8/2023) it has issues like dumb
;; indentation, which yaml-ts-mode handles better.
(defun cc/yaml-init ()
  (cc/source-code-enable-eglot 'yaml-mode)
  (add-hook 'yaml-mode-hook #'cc/yaml-mode-setup)
  ;; Include non-alphabetic entries (e.g. .hidden-anchors) in imenu.
  ;; Taken from this PR (ignored by maintainer):
  ;; https://github.com/yoshiki/yaml-mode/pull/89
  (setq yaml-imenu-generic-expression
        '((nil  "^\\(:?[^{}(),\s \n]+\\):" 1)
          ("*Anchors*" "^\\s *[^{}(),\s \n]+: \\&\\([^{}(),\s \n]+\\)" 1)))
  ;; yaml-mode binds M-<tab> (aka C-M-i) to `ispell-complete-word', which is dumb. I want to use my
  ;; normal completion.
  (define-key yaml-mode-map (kbd "C-M-i") nil))

(defun cc/yaml-mode-setup ()
  (setq-local outline-regexp (rx (not (any "#" whitespace "\n")))))

(provide 'cc-yaml)
