;;; -*- lexical-binding: t -*-
(require 'dash)
(require 'eglot)
(require 'hl-line)
(require 'hl-prog-extra)
(require 'hl-todo)
(require 'pulsar)
(require 'rainbow-delimiters)
(require 'spacemacs-dark-theme)
(require 'subr-x)
(require 'cc-utils)

(defvar cc/font-height-default 100)

(defun cc/appearance-init ()
  ;; Set `frame-inhibit-implied-resize' *before* setting font/display-related config. This makes
  ;; sure that changing font size will not attempt to resize the frame, which makes no sense with
  ;; EXWM and would result in e.g. the mode line rendering off-screen.
  (setq frame-inhibit-implied-resize t)
  (cc/appearance--font-init)
  (cc/appearance--buffer-placement-init)
  (cc/appearance--pulsar-init)
  (cc/appearance--hl-prog-init)
  (add-hook 'visual-line-mode-hook #'visual-wrap-prefix-mode)
  (add-hook 'cc/source-code-hook #'cc/appearance--source-code-setup)
  (global-set-key (kbd "s--") #'cc/appearance-font-size-adjust)
  (load-theme 'spacemacs-dark t)

  (setq scroll-preserve-screen-position t)
  (setq scroll-error-top-bottom t)
  (setq-default fill-column 100)
  (setq-default display-fill-column-indicator t)

  (setq hl-todo-keyword-faces
        '(("TODO" . "orange")
          ("FIXME" . "red")
          ("NEXT" . "yellow")))
  (global-hl-line-mode))


;;; General appearance config

(defun cc/appearance--font-init ()
  "General font config."
  (set-face-attribute 'default nil
                      :family "Hack"
                      :height cc/font-height-default
                      :weight 'normal
                      :width 'normal)
  (set-face-attribute 'fixed-pitch-serif nil
                      :family "Hack")

  ;; `symbol-overlay' and `eglot' don't always highlight the exact same set of symbols when point is
  ;; on a symbol (at least with bash-language-server) so make their highlighting identical instead
  ;; of picking one or the other. Also, some modes (e.g. elisp) don't use eglot.
  (set-face-attribute 'eglot-highlight-symbol-face nil :inherit 'symbol-overlay-default-face)
  ;; (set-face-attribute 'font-lock-punctuation-face nil)
  ;; For some reason (possibly Emacs 28 bug) italic face is underline without this.
  (set-face-attribute 'italic nil :slant 'italic :underline nil)
  ;; `use-default-font-for-symbols' disabled for emoji font to work. See:
  ;; https://github.com/alphapapa/ement.el#displaying-symbols-and-emojis
  ;; https://emacs.stackexchange.com/questions/62049/override-the-default-font-for-emoji-characters
  (setq use-default-font-for-symbols nil)
  (set-fontset-font t 'symbol "Noto Color Emoji"))

(defun cc/appearance--source-code-setup ()
  (setq-local show-trailing-whitespace t)
  (display-line-numbers-mode)
  (display-fill-column-indicator-mode)
  (diminish 'page-break-lines-mode)
  (page-break-lines-mode)
  (hl-todo-mode)
  (rainbow-delimiters-mode))

(defun cc/appearance-font-size-adjust (&optional arg)
  "Adjust font size between two presets.

Switches to large size if called with C-u or if ARG is t, extra large with C-u
C-u, or default otherwise."
  (interactive "p")
  (let ((height (pcase arg
                  ((or 't 4) 120)
                  (16 140)
                  (64 160)
                  (_ cc/font-height-default))))
    (set-face-attribute 'default nil :height height)))

(defun cc/appearance--pulsar-init ()
  (add-hook 'consult-after-jump-hook #'pulsar-recenter-middle)
  (add-hook 'consult-after-jump-hook #'pulsar-reveal-entry)
  (global-set-key (kbd "s-p") #'cc/appearance-pulse-line-long)
  (setq pulsar-iterations 5)
  (setq pulsar-face 'pulsar-red)

  (declare-function aw-switch-to-window "ace-window")
  (declare-function cc/ace-window-or-mini "cc-kitchen-sink")
  (dolist (fn (list #'pop-to-buffer
                    #'aw-switch-to-window
                    #'cc/ace-window-or-mini
                    #'previous-buffer
                    #'next-buffer))
    (add-to-list 'pulsar-pulse-functions fn))
  (pulsar-global-mode))

(defun cc/appearance-pulse-line-long ()
  (interactive)
  (let ((pulsar-iterations 40))
    (pulsar-pulse-line)))


;;; Buffer window placement.

(defun cc/appearance--buffer-placement-init ()
  ;; The default `window-min-height' and `window-min-width' are hilariously low, presumably because
  ;; back in the day computer screens were tiny. These affect both splitting *and resizing*
  ;; universally, including explicit user split/resize commands -- so don't set these too high.
  (setq window-min-width 60
        window-min-height 10)
  (setq display-buffer-base-action
        '((;; PRIMARY RULES: pick an existing without splitting.
           display-buffer--maybe-same-window ; Current window if showing the same buffer.
           display-buffer-reuse-window ; Some window with the same buffer.
           display-buffer-same-window ; Current window unless dedicated or inhibit-same-window.
           display-buffer-reuse-mode-window ; Some window with same major mode.
           ;; Using most recent here instead of `display-buffer-use-least-recent-window' seems
           ;; weird, but this works nicely for things like consult previews and scrolling through
           ;; `grep-mode' results. This way the result shows where I'm likely to be currently
           ;; looking, instead of some random out-of-the-way window. However I'm not positive that I
           ;; want to do it this way.
           cc/appearance--display-buffer-mru-window
           display-buffer-use-some-window ; Any existing window.
           ;; FALLBACKS: could create splits, change frames, etc.
           display-buffer-pop-up-window
           display-buffer-pop-up-frame)
          ;; ALIST PARAMETERS
          . ((direction . below))))
  (setq display-buffer-alist
        `((,(cc/appearance--mode-condition 'magit-diff-mode 'magit-process-mode)
           . (nil . ((inhibit-same-window . t))))
          (,(rx bos (or "*Org Select*" ; `org-capture' template selection.
                        "*Org Clock*" ; `org-resolve-clocks' (also called during clock in).
                        "*Clock Task Select*"  ; `org-clock-in-last' task selection.
                        "*Calendar*")
                eos)
           . ((display-buffer-at-bottom)))
          ;; For reasons I don't fully understand, capture windows may be either
          ;; "CAPTURE-foofilename.org" or "*Capture*". Also note that the
          ;; filename could end in .org.gpg, not just .org... at least in theory.
          (,(rx bos (or (seq "CAPTURE-" (* any) ".org") "*Capture*") eos)
           . ((display-buffer-pop-up-window)))
          (,(cc/appearance--mode-condition 'help-mode
                                           'compilation-mode ; includes `grep-mode'
                                           'occur-mode
                                           'embark-collect-mode)
           ;; Keep in mind that dedicated flag is cleared if the buffer is changed, so don't use
           ;; display functions like `display-buffer-reuse-mode-window'.
           . ((display-buffer--maybe-same-window
               display-buffer-reuse-window
               display-buffer-pop-up-window)
              . ((dedicated . t)
                 (window-height . shrink-window-if-larger-than-buffer))))))
  ;; It took me fucking six hours (if not longer) to figure this out while thinking the issue was
  ;; related to `display-buffer-alist' or `display-buffer-base-action'. Worst default ever. Note
  ;; that this affects `grep-mode' too, since it extends `compilation-mode' and binds commands like
  ;; `next-error-no-select' to "n".
  ;;
  ;; Reference: https://lists.gnu.org/archive/html/bug-gnu-emacs/2021-03/msg00943.html
  ;;
  ;; Also see https://lists.gnu.org/archive/html/bug-gnu-emacs/2018-09/msg00102.html where Emacs
  ;; maintainers discuss the weirdness of `grep-mode' and its usage of `next-error-no-select'.
  (setq next-error-find-buffer-function #'next-error-buffer-unnavigated-current))

(defun cc/next-error-find-buffer (&optional avoid-current
					                        extra-test-inclusive
					                        extra-test-exclusive)
  (when (and (derived-mode-p 'compilation-mode)
             (next-error-buffer-p (current-buffer)
                                  avoid-current
                                  extra-test-inclusive
                                  extra-test-exclusive))
    (current-buffer)))

(defun cc/appearance--mode-condition (&rest modes)
  "Generate a condition for `display-buffer-alist' based on `major-mode'."
  (lambda (buffer-name _action)
    (with-current-buffer buffer-name
      (apply #'derived-mode-p modes))))

(defun cc/appearance--display-buffer-mru-window (buffer alist)
  "Display buffer in most recently selected, non-dedicated window.

This is *not* the same as `display-buffer-in-previous-window', which picks a
window that recently displayed BUFFER."
  ;; Note: `get-mru-window' can return nil in surprising situations, such as when `org-agenda'
  ;; displays its "Agenda Commands" dispatch window. It might make sense to come up with another way
  ;; of tracking windows, but that would be somewhat complex when taking into considered killed
  ;; windows and so on.
  (when-let ((recent (get-mru-window nil nil 'not-selected)))
    (cc/appearance--do-display-buffer buffer recent 'reuse alist)))

(defun cc/appearance--do-display-buffer (buffer window type alist)
  (unless (or (window-minibuffer-p window)
              (window-dedicated-p window))
    (window--display-buffer buffer window type alist)))

(define-minor-mode cc/appearance-debug-display-buffer-mode
  "Log buffer window placement logic for debugging."
  :global t :group 'cc/appearance
  (if cc/appearance-debug-display-buffer-mode
      ;; Don't need to advise `pop-to-buffer' because it calls `display-buffer'.
      (advice-add #'display-buffer :around #'cc/appearance--display-buffer-debug-advice)
    (advice-remove #'display-buffer #'cc/appearance--display-buffer-debug-advice)))

(defvar cc/appearance-debug-display-buffer-mode-show-backtrace nil
  "Enable to include backtrace in `cc/appearance-debug-display-buffer-mode'")

(defun cc/appearance--display-buffer-debug-advice (fn &rest args)
  (message "***** DISPLAY-BUFFER DEBUG: *****")
  (cc/appearance--log 'selected-window (selected-window))
  (cc/appearance--log "[arg] buffer" (car args))
  (cc/appearance--log "[arg] action" (cadr args))
  (cc/appearance--log 'display-buffer-overriding-action display-buffer-overriding-action)
  ;; Sometimes `display-buffer-alist' gets temporarily modified, e.g. during Emacs shutdown
  ;; prompting to kill processes or by Embark when showing binding hints, so it's useful to show
  ;; this too.
  (seq-do-indexed (lambda (it index)
                    (cc/appearance--log (format "%s[%s]" 'display-buffer-alist index) it))
                  display-buffer-alist)
  (let ((display-buffer-alist
         (--map-indexed
          (cons (car it)
                (cc/appearance--wrap-display-actions
                 (cdr it)
                 (format "%s[%s]" 'display-buffer-alist it-index)))
          display-buffer-alist))
        (display-buffer-overriding-action
         (cc/appearance--wrap-display-actions display-buffer-overriding-action
                                              'display-buffer-overriding-action))
        (display-buffer-base-action
         (cc/appearance--wrap-display-actions display-buffer-base-action
                                              'display-buffer-base-action)))
    (prog1 (apply fn args)
      (when cc/appearance-debug-display-buffer-mode-show-backtrace
        (message "\n[backtrace]")
        (backtrace))
      (message "***** DISPLAY-BUFFER END *****"))))

(defun cc/appearance--wrap-display-actions (actions key)
  "ACTIONS is a cons cell like the value of `display-buffer-base-action'."
  (cons (--map (cc/appearance--log-wrapper it key)
               (if (sequencep (car actions))
                   (car actions)
                 (list (car actions))))
        (cdr actions)))

(defun cc/appearance--log-wrapper (fn-or-list key)
  (if (sequencep fn-or-list)
      (--map (cc/appearance--log-wrapper it key) fn-or-list)
    (lambda (&rest args)
      (let ((result (apply fn-or-list args)))
        (cc/appearance--log
         (format "[call] %s" key)
         (format "%-40s => %s" fn-or-list result))
        result))))

(defun cc/appearance--log (key value)
  (message "%40s: %s" key value))


(defun cc/appearance--hl-prog-init ()
  "`hl-prog-extra-mode': highlight URLs in comments and strings.

This is useful in and of itself, but more importantly it's used by `spell-fu' to
avoid marking URLs as spelling errors."
  (add-hook 'cc/source-code-hook #'hl-prog-extra-mode)
  (setq hl-prog-extra-list
        ;; This regex taken from the original value of `hl-prog-extra-list'.
        `(("\\<https?://[^[:blank:]]*" 0 comment font-lock-constant-face))))

(provide 'cc-appearance)
