;;; -*- lexical-binding: t; byte-compile-warnings: (not noruntime docstrings) -*-
;; - Runtime byte compiler warnings disabled because it complains about almost every function I use
;;   with hydra and it has never saved me from any actual issues.
;;   TODO revisit above (i.e. un-disable noruntime) after refactoring.
;;
;; - Docstring length warnings disabled because hydras with lambdas trigger it, which is pointless.
;;   It might make sense to revisit this sometime in the future, since it was just introduced within
;;   the last few days to Emacs master branch... but I don't care about this warning because this is
;;   my init file, not a published library, and it has nothing to do with correctness.

(require 'use-package)
(setq use-package-expand-minimally t)

(require 'cc-user-directories)
(require 'cc-utils)
(require 'cc-org-gtd)

;; bind-key and diminish are used by use-package's :bind and :diminish.
(use-package bind-key
  :demand t)
(use-package diminish
  :demand t)


;;; Core Emacs

(progn ; improved keybinds - partially taken from better-defaults.el
  (autoload 'zap-up-to-char "misc" nil t)
  (global-set-key (kbd "M-z") 'zap-up-to-char)
  (global-set-key (kbd "M-/") 'hippie-expand)
  ;; Accidentally hitting this and freezing emacs is annoying, especially when Emacs is also your
  ;; window manager (EXWM).
  (define-key global-map (kbd "C-z") nil)
  (define-key global-map (kbd "C-x C-z") nil))

(progn ; miscellaneous core config
  (setq-default indent-tabs-mode nil)
  (setq-default tab-width 4) ; Emacs default is 8, which is crazy.
  (setq save-interprogram-paste-before-kill t)
  (setq require-final-newline t)
  (setq visible-bell t)
  (put 'narrow-to-region 'disabled nil)
  (setq next-screen-context-lines 10)
  (setq sentence-end-double-space nil)
  ;; Please go burn in a fire, custom file.
  (let ((dest (concat "emacs-custom-" (number-to-string (emacs-pid)))))
    (setq custom-file (expand-file-name dest temporary-file-directory)))
  ;; Because of course emacs has HTTP cookies, it turns out.
  (require 'url-cookie)
  (setq url-cookie-file (expand-file-name "url-cookies" cc/user-cache))
  ;; Make all prompts consistently y/n instead of a mix, which is unnecessary
  ;; cognitive load.
  (defalias 'yes-or-no-p 'y-or-n-p))

;; Auto save/backup/lock files and auto reverting.
;;
;; See elisp man "27: Backups and Auto-Saving" and "18: Files", but TLDR is:
;; - Locks: prevent multiple Emacs instances editing same file.
;; - Backups: copy of original file before editing, made once when first visited. Also (typically)
;;   automatically disabled for version-controlled files.
;; - Auto saves: periodic copy of unsaved changes (`auto-save-mode', NOT `auto-save-visited-mode').
(progn
  ;; Lock files are completely useless to me and occasionally a nuisance when testing init changes
  ;; in a separate Emacs instance.
  (setq create-lockfiles nil)
  ;; Keep backup and auto save files out of the way, because I don't like them littering my working
  ;; dir, especially due to developer tools that do dumb things like trying to copy or compile them.
  (setq backup-directory-alist
        `(("." . ,temporary-file-directory)))

  ;; Tempting to put auto saves in ~/.cache for persistence across restarts, but I don't want to
  ;; thrash my SSD. The only time I'm likely to need these is if Emacs crashes, in which cases at
  ;; most a logout will be required to recover, not a restart.
  (setq auto-save-file-name-transforms
        `((".*" ,(concat temporary-file-directory) t)))
  (setq auto-save-list-file-prefix (expand-file-name "auto-save-list/saves-" cc/user-cache))
  (setq auto-save-timeout 10)

  (require 'cc-file-auto-save)
  (cc/file-auto-save-mode)
  (global-auto-revert-mode)
  ;; Modifying `revert-buffer-insert-file-contents-function' based on doc of `auto-revert-mode'.
  (setq revert-buffer-insert-file-contents-function #'revert-buffer-insert-file-contents-delicately)
  ;; Disabling `auto-revert-use-notify' eliminates delay when making changes via magit... wtf?
  (defvar auto-revert-use-notify) ;; Should probably do this the right way, but whatever.
  (setq auto-revert-use-notify nil)
  (add-hook 'dired-mode-hook #'auto-revert-mode))

(progn ; File encodings are fun and exciting said... no one.
  (prefer-coding-system 'utf-8)
  (setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING)))

;; Sizes are in bytes. For reference on undo limits, Doom Emacs uses:
;; - `undo-limit': 400kb (default is 160kb)
;; - `undo-strong-limit': 3mb (default is 240kb)
;; - `undo-outer-limit': 48mb (default is 25mb)
(let ((mb (* 1024 1024)))
  (setq undo-limit (* 1 mb)
        undo-strong-limit (* 6 mb)
        undo-outer-limit (* 96 mb)))

(progn ; `isearch'
  (setq isearch-allow-scroll t)
  (global-set-key (kbd "C-s") 'isearch-forward-regexp)
  (global-set-key (kbd "C-r") 'isearch-backward-regexp)
  (global-set-key (kbd "C-M-s") 'isearch-forward)
  (global-set-key (kbd "C-M-r") 'isearch-backward))

(use-package simple
  :demand t
  :bind (("M-SPC" . cycle-spacing)
         ("M-j" . #'cc/join-line))
  :preface
  (defun cc/join-line ()
    (interactive)
    (join-line -1))
  (defun cc/kill-ring-transform (value)
    "For `kill-transform-function'."
    (cond
     ((string-blank-p value)
      nil)
     ;; Copying a command from vterm sometimes results in trailing whitespace, which I think has
     ;; something to do with the right-hand prompt ($RPS1) but I'm not exactly sure what's going on.
     ((derived-mode-p 'vterm-mode)
      (string-trim-right value))
     (t
      value)))
  :config
  (setq search-ring-max 100)
  (setq kill-ring-max 200)
  (setq mark-ring-max 50)
  (setq global-mark-ring-max 250)
  (setq kill-transform-function #'cc/kill-ring-transform)
  (kill-ring-deindent-mode)
  (setq yank-pop-change-selection t)
  (setq kill-do-not-save-duplicates t)
  (setq history-delete-duplicates t)

  ;; Truncation when doing interactive things/debugging is annoying. Note: nil means unlimited.
  (setq eval-expression-print-level 12)
  (setq eval-expression-print-length 36))

(progn
  (require 'compile)

  ;; Based on https://rtime.ciirc.cvut.cz/~sojka/blog/compile-on-save/
  (defun cc/compile-on-save--run ()
    (when-let ((b (compilation-find-buffer)))
      (unless (get-buffer-process b) ; No-op if already compiling.
        (recompile))))

  (define-minor-mode cc/compile-on-save-mode
    "Automatically call `recompile' on save."
    :group 'cc/compile-on-save
    (if cc/compile-on-save-mode
        (progn  (make-local-variable 'after-save-hook)
                (add-hook 'after-save-hook #'cc/compile-on-save--run nil t))
      (kill-local-variable 'after-save-hook)))

  (setq compilation-scroll-output nil)
  ;; These prompts get really annoying after the millionth time or so. No Emacs, I don't want to
  ;; save every single buffer all the time.
  (setq compilation-ask-about-save nil)
  (setq compilation-save-buffers-predicate '(lambda () nil)))

(use-package newcomment
  :demand t
  :hook (prog-mode . auto-fill-mode)
  :diminish auto-fill-function
  :bind (([remap comment-line] . cc/comment-line-or-duplicate))
  :preface
  (defun cc/comment-line-or-duplicate (arg)
    "Similar to `comment-line', except prefix behavior is different.

If called with any prefix, the line/region to be commented will
also be duplicated below. Inspired by IntelliJ's 'c-d' behavior."
    (interactive "P")
    (if arg
        (let* ((is-region (use-region-p))
               ;; Start/end calculation based on `comment-line'.
               (start (if is-region
                          (save-excursion
                            (goto-char (region-beginning))
                            (line-beginning-position))
                        (line-beginning-position)))
               ;; Difference from comment-line: add 1 to end to copy the line ending too.
               (end (if is-region
                        (save-excursion
                          (goto-char (region-end))
                          (line-end-position))
                      (line-end-position)))
               (contents (buffer-substring start end)))
          ;; Reminder: comment-line modifies buffer, so start/end vars are now inaccurate.
          (comment-line nil)
          ;; Go to next line (after region) to match comment-line's non-region behavior.
          (when is-region
            (goto-char (region-end))
            (goto-char (line-end-position))
            (when (equal (point) (buffer-end 1))
              (newline))
            (forward-line 1))
          (goto-char (line-beginning-position))
          (insert contents)
          (newline))
      (comment-line nil)))
  :config
  (setq comment-auto-fill-only-comments t))

(use-package dired
  :defer 1
  :bind (:map dired-mode-map
         ("C-c C-e" . #'wdired-change-to-wdired-mode))
  :config
  (setq dired-listing-switches "-Alhv")
  (setq dired-recursive-copies 'always)
  (setq dired-recursive-deletes 'always)
  ;; With two dired buffers open, dired will suggest the other buffer as rename destination.
  (setq dired-dwim-target t))

(use-package wdired
  :defer 1
  :config
  (setq wdired-allow-to-change-permissions t)
  (setq wdired-use-dired-vertical-movement t))

(use-package help
  :defer 1
  :config
  (temp-buffer-resize-mode))

(use-package help-at-pt
  :config
  (setq help-at-pt-display-when-idle 'kbd-help)
  (setq help-at-pt-timer-delay 0.5)
  (help-at-pt-cancel-timer)
  (help-at-pt-set-timer))

(use-package man
  :defer 1
  :config
  (setq Man-notify-method 'pushy)
  (setq Man-width-max 100))

(use-package apropos
  :defer 1
  :config
  (setq apropos-do-all t))

(use-package uniquify
  :config
  (setq uniquify-buffer-name-style 'forward))

(use-package bookmark
  :demand t
  :config
  (setq bookmark-default-file (expand-file-name "bookmarks" cc/user-cache))
  (setq bookmark-watch-bookmark-file t)
  (setq bookmark-fringe-mark nil))

(use-package saveplace
  :defer 1
  :config
  (setq save-place-file (expand-file-name "places" cc/user-cache))
  (save-place-mode))

(use-package savehist
  :config
  (setq savehist-file (expand-file-name "savehist" cc/user-cache))
  (savehist-mode))

(use-package recentf
  :defer 1
  :config
  (setq recentf-save-file (expand-file-name "recentf" cc/user-cache))
  (setq recentf-max-saved-items 200)
  ;; recentf exclusion reasons
  ;; - org files: because org-agenda automatically opens them and spams the list. Also no eot in
  ;;   regexp to match e.g. foo.org.gpg
  ;; - tramp files: don't want them cluttering up recentf list. Also, on startup `recentf-mode' does
  ;;   shows a dumb message *even if I load tramp first*: "File is missing: Opening input file No
  ;;   such file or directory /nix/store/<hash>-lisp-all-merged/tramp". I don't think this breaks
  ;;   anything, but it's distracting/wtf-y.
  (dolist (exclude (list (rx bot (eval (expand-file-name "~/org-files/")) (0+ any) ".org")
                         (rx bot "/ssh:")
                         (rx bot "/sudo:")))
    (add-to-list 'recentf-exclude exclude))
  (recentf-mode))

(use-package transient
  :demand t
  :config
  (setq transient-history-file (expand-file-name "transient-history.el" cc/user-cache))
  (setq transient-values-file  (expand-file-name "transient-values.el" cc/user-config-repo))
  (setq transient-levels-file  (expand-file-name "transient-levels.el" cc/user-config-repo))
  ;; Because some other things trigger transient loading early. I could change my config to make
  ;; sure transient loads first, but then nothing prevents me from having the same issue in the
  ;; future again.
  (setq transient-history (transient--read-file-contents transient-history-file))
  (setq transient-values (transient--read-file-contents transient-values-file))
  (setq transient-levels (transient--read-file-contents transient-levels-file)))

(use-package calc
  :defer 1
  :config
  ;; This has a truly horrible default value, even by Emacs standards.
  (setq calc-multiplication-has-precedence nil))

;; `remote-file-name-access-timeout' applies to e.g. tramp, recentf, and `desktop-save-mode'.
(setq remote-file-name-access-timeout 10)

(use-package tramp
  :defer t
  :config
  (setq tramp-default-method "ssh")
  ;; Tramp's `tramp-remote-path' is a dumb misfeature from what I can tell, because it uses
  ;; hard-coded heuristics to set $PATH instead of using the remote shell's profile.
  ;; P.S. using "tramp-own-remote-path" symbol also magically makes Tramp use a login shell, for
  ;; better or worse.
  ;; https://emacs.stackexchange.com/questions/7673/how-do-i-make-trampeshell-use-my-environment-customized-in-the-remote-bash-p
  (setq tramp-remote-path '(tramp-own-remote-path tramp-default-remote-path))

  ;; Don't try to use authinfo, because it's gpg-encrypted and I don't use it for tramp (besides,
  ;; the correct answer for remote authentication is to use SSH keys, *not* passwords).
  ;; References:
  ;; - https://emacs.stackexchange.com/questions/64062/how-to-avoid-using-auth-sources-when-editing-with-sudo
  ;; - https://debbugs.gnu.org/cgi/bugreport.cgi?bug=46674 (linked in stackexchange answer)
  ;; - Tramp 4.13 Reusing passwords for several connections
  ;; - Emacs info 49.2.6: Per-Connection Local Variables
  (setq tramp-completion-use-auth-sources nil) ; nil is the default, but just in case it changes.
  (connection-local-set-profile-variables 'remote-without-auth-sources '((auth-sources . nil)))
  (connection-local-set-profiles '(:application tramp) 'remote-without-auth-sources))

(use-package password-cache
  :config
  ;; Note: cache can be cleared via `password-reset' if needed for... whatever reason.
  (setq password-cache t) ; t is the default, but just to be explicit
  (setq password-cache-expiry (* 60 60 8)))

;; Why did I not know about this until now? It's amazing for exploring code.
;; Activate by switching to `read-only-mode'.
(progn ; view-mode
  (setq view-read-only t)
  ;; `View-exit' ("q") will only exit `view-mode' but not kill the buffer if it was opened from a
  ;; link in help buffer. I want "q" to kill the buffer consistently, like it already does when
  ;; originally opened via xref or find-file. If there's one thing consistent about Emacs, it's
  ;; inconsistency with situations like this.
  (setq-default view-exit-action
                (lambda (buffer)
                  (with-current-buffer buffer
                    (when buffer-read-only
                      (kill-buffer-if-not-modified buffer))))))

(with-eval-after-load 'tablist
  (declare-function tablist-sort "tablist.el") ;; REALLY, Emacs? You can figure this out, come on.
  (define-key tabulated-list-mode-map (kbd "^") #'tablist-sort))


;;; Hydra - this goes early in init because I use it in so many places.
;; (It's easier to just load it early than mess with :after all over the place)
;; Also, since it's a macro I want it loaded immediately not deferred because of byte compilation.
(require 'hydra)
(setq hydra-look-for-remap t)
(defmacro cc/dynamic-hydra--create (name body docstring &rest conditional-heads)
  (declare (indent defun))
  `(progn
     (eval
      (append
       '(defhydra ,name ,body ,docstring)
       (cl-loop for ch in ',conditional-heads
                if (eval (car ch))
                collect (cdr ch))))))

(defmacro cc/dynamic-hydra (name body docstring &rest conditional-heads)
  "Hydra with heads that are conditionally evaluated/shown.

See https://github.com/abo-abo/hydra/issues/86 for context."
  (declare (indent defun))
  `(defun ,name ()
     (interactive)
     (cc/dynamic-hydra--create ,name ,body ,docstring ,@conditional-heads)
     (funcall-interactively ',(intern (concat (symbol-name name) "/body")))))


;;; Window management (Ace Window, etc)
(require 'winner)
;; Prevent `winner-undo' triggering a bug that breaks minibuffer/echo area, resulting in other
;; buffers being displayed there.
(setq winner-boring-buffers-regexp (rx (* any) "*Minibuf-" (+ digit) "*"))
(winner-mode 1)

(use-package transpose-frame)

(use-package ibuffer
  :bind (:map ibuffer-mode-map
         ;; Interferes with ace-window.
         ("M-o" . nil))
  :config
  (setq ibuffer-display-summary nil)
  (setq ibuffer-formats '((mark modified read-only " | "
                                (mode 16 16 :left :elide)
                                " | "
                                name)
                          (mark modified read-only " "
                                (name 40 40 :left :elide)
                                " " filename-and-process))))

(use-package ibuf-ext
  :config
  (setq ibuffer-never-show-predicates
        (list (rx "*Help*")
              (rx "*Completions*")
              (rx "*Buffer List*")
              (rx "*Disabled Command*"))))

(use-package ace-window
  :demand t
  :bind ("M-o" . cc/ace-window-or-mini)
  :preface
  (defun cc/ace-window-or-mini ()
    "Switch to active, unfocused minibuffer, otherwise `ace-window'."
    (interactive)
    (let ((mini (active-minibuffer-window)))
      (if (and mini
               (not (equal mini (selected-window))))
          (select-window mini)
        (ace-window nil))))
  :config
  (defun cc/delete-window-and-buffer (window)
    (aw-delete-window window t))
  (defun cc/kill-buffer-in-window (window)
    (with-selected-window window
      (kill-buffer)))
  (defun cc/switch-buffer-in-window (window)
    (aw-switch-to-window window)
    (call-interactively #'consult-buffer))
  (defun cc/move-buffer-kill-window (window)
    "Move current buffer to another window and kill old window"
    (let ((old-window (frame-selected-window)))
      (aw-move-window window)
      (aw-delete-window old-window)))
  (defun cc/move-window-and-point (window)
    "Like `aw-move-window', but the current buffer's point will
    be used instead of the target window if both windows are
    displaying the same buffer."
    (let ((buffer (current-buffer))
          (p (window-point)))
      (switch-to-buffer (other-buffer))
      (aw-switch-to-window window)
      (switch-to-buffer buffer)
      (set-window-point window p)))

  (defhydra cc/hydra-window-size (:color red)
    "Window size"
    ("j" (shrink-window 5)                     "Shrink vertical" :column "Vertical")
    ("J" (shrink-window 25)                    "    5x")
    ("k" (enlarge-window 5)                    "Enlarge vertical")
    ("K" (enlarge-window 25)                   "    5x")
    ("l" (enlarge-window-horizontally 5)       "Enlarge horizontal" :column "Horizontal")
    ("L" (enlarge-window-horizontally 25)      "    5x")
    ("h" (shrink-window-horizontally 5)        "Shrink horizontal")
    ("H" (shrink-window-horizontally 25)       "    5x")
    ("-" (shrink-window-if-larger-than-buffer) "Fit" :column "Misc")
    ("=" (balance-windows)                     "Balance")
    ("C-g" nil nil)
    ("RET" nil nil))

  ;; TODO: figure out some sort of layout presets
  (defhydra cc/hydra-transpose-frame (:color red)
    "Transpose frame"
    ("t" #'transpose-frame "Swap X/Y")
    ("v" #'flip-frame "Flip vertically")
    ("h" #'flop-frame "Flip horizontally")
    ("r" #'rotate-frame-clockwise "Rotate CW")
    ("R" #'rotate-frame-anticlockwise "Rotate CCW")
    ("C-g" nil nil)
    ("RET" nil nil))

  ;; EXWM can't render overlays on top of of X11 windows, so disable them
  ;; and use the mode line instead.
  (setq aw-display-mode-overlay nil)
  (ace-window-display-mode t)
  ;; KLUDGE: remove what ace-window-display-mode puts in the modeline in order to display it
  ;; differently... however, the mode still needs to be active for hooks to update the window
  ;; parameter for use from my own mode line.
  (set-default
   'mode-line-format
   (assq-delete-all
    'ace-window-display-mode
    (default-value 'mode-line-format)))

  ;; In EXWM, only a single frame is visible at a time since they are used as
  ;; workspaces.
  (setq aw-scope 'frame)

  (setq aw-keys '(?a ?s ?d ?f ?j ?k ?l ?\; ?q ?w ?e)
        aw-dispatch-always t
        aw-minibuffer-flag t
        ;; Regarding optional 2nd arg:
        ;; "If the action character is followed by a string, then ace-window
        ;; will be invoked again to select the target window for the action.
        ;; Otherwise, the current window is selected"
        aw-dispatch-alist
        '((?r cc/hydra-window-size/body)
          (?t cc/hydra-transpose-frame/body)
          (?o aw-flip-window)
          (?v aw-split-window-vert "Split: vertical")
          (?h aw-split-window-horz "Split: horizontal")
          (?x aw-delete-window "Delete window")
          (?X cc/delete-window-and-buffer "Delete window and buffer")
          (?k cc/kill-buffer-in-window "Kill buffer")
          (?z delete-other-windows "Maximize window")
          (?m aw-swap-window "Swap window (with current)")
          (?M cc/move-window-and-point "Move window")
          (?n cc/move-buffer-kill-window "Move and kill")
          (?c aw-copy-window "Copy window")
          (?b cc/switch-buffer-in-window "Switch buffer")
          (?? aw-show-dispatch-help)))

  ;; Org calendar integration (e.g. `org-goto-calendar').
  (add-to-list 'calendar-mode-line-format '(window-parameter (selected-window) 'ace-window-path))

  ;; Refresh mode-line with config changes - otherwise the default keys are
  ;; shown immediately after startup (e.g. "1" instead of "a").
  (aw-update))


;;; General packages

(use-package outline
  :demand t
  :bind (("C-c o" . #'cc/hydra-outline/body))
  :preface
  (defhydra cc/hydra-outline (:color pink)
    ;; Hide
    ("q" #'outline-hide-sublevels "sublevels" :column "Hide")
    ("t" #'outline-hide-body "body")
    ("o" #'outline-hide-other "other")
    ("c" #'outline-hide-entry "entry")
    ("l" #'outline-hide-leaves "leaves")
    ("d" #'outline-hide-subtree "subtree")
    ;; Show
    ("a" #'outline-show-all "all" :column "Show")
    ("e" #'outline-show-entry "entry")
    ("i" #'outline-show-children "children")
    ("k" #'outline-show-branches "branches")
    ("s" #'outline-show-subtree "subtree")
    ;; Move
    ("u" #'outline-up-heading "up" :column "Move")
    ("n" #'outline-next-visible-heading "next visible")
    ("p" #'outline-previous-visible-heading "previous visible")
    ("f" #'outline-forward-same-level "forward same level")
    ("b" #'outline-backward-same-level "backward same level")
    ("C-g" nil nil)
    ("RET" nil nil)))

(use-package symbol-overlay
  :demand t
  :diminish
  :hook (prog-mode . symbol-overlay-mode)
  :bind (("C-c s" . #'cc/do-symbol-overlay-hydra))
  :preface
  (defhydra cc/symbol-overlay-hydra (:color pink)
    ("i" #'symbol-overlay-put                          "put" :column "Navigation")
    ("n" #'symbol-overlay-jump-next                    "go     (n)")
    ("p" #'symbol-overlay-jump-prev                    "       (p)")
    ("M-<" #'symbol-overlay-jump-first                 "       (first)")
    ("M->" #'symbol-overlay-jump-last                  "(last)")
    ("M-n" #'symbol-overlay-switch-forward             "switch (n)")
    ("M-p" #'symbol-overlay-switch-backward            "       (p)")
    ("k" #'symbol-overlay-remove-all                   "remove all")
    ("t" #'symbol-overlay-toggle-in-scope              "scope (toggle)")

    ("w" #'symbol-overlay-save-symbol                  "save" :column "Misc")
    ("c" #'symbol-overlay-count                        "count")
    ("s" #'symbol-overlay-isearch-literally            "isearch")
    ("r" #'symbol-overlay-rename                       "rename")
    ("M-r" #'symbol-overlay-query-replace              "query-replace")
    ("z" #'symbol-overlay-mode                         "mode (toggle)")
    ("C-g" nil)
    ("RET" nil nil))

  (defun cc/do-symbol-overlay-hydra ()
    (interactive)
    (symbol-overlay-mode)
    (cc/symbol-overlay-hydra/body))

  :config
  ;; Inherits from 'highlight by default, which made it hard to distinguish against an active
  ;; transient region.
  (set-face-attribute 'symbol-overlay-default-face nil :inherit 'lazy-highlight)
  ;; By default there are eight different faces, and some are hard to distinguish.
  (set-face-background 'symbol-overlay-face-1 "dodger blue")
  (set-face-background 'symbol-overlay-face-2 "orange")
  (set-face-background 'symbol-overlay-face-3 "yellow")
  (set-face-background 'symbol-overlay-face-4 "orchid")
  (set-face-background 'symbol-overlay-face-5 "red")
  (set-face-background 'symbol-overlay-face-6 "spring green")
  (setq symbol-overlay-faces '(symbol-overlay-face-1
                               symbol-overlay-face-2
                               symbol-overlay-face-3
                               symbol-overlay-face-4
                               symbol-overlay-face-5
                               symbol-overlay-face-6)))

(require 'yasnippet)
(use-package autoinsert
  :demand t
  :config
  (defun cc/autoinsert-yasnippet-boilerplate ()
    "Expand yasnippet named 'boilerplate' for current major mode.
Intended for use with `autoinsert'."
    (yas-expand-snippet
     (yas-lookup-snippet "boilerplate")))

  (setq auto-insert-alist
        '((bash-ts-mode . cc/autoinsert-yasnippet-boilerplate)
          (emacs-lisp-mode . cc/autoinsert-yasnippet-boilerplate)))
  (setq auto-insert-query nil)
  (auto-insert-mode))

(use-package visual-regexp
  :defer t
  :bind ("C-c r" . vr/query-replace))

;; usage, with examples: M-x sp-cheat-sheet
;; Also, remember that =C-q= `quoted-insert' can be used to bypass strict mode (e.g. =C-q "= to
;; insert a single quote instead of a matching pair).
;; https://github.com/Fuco1/smartparens/wiki
;; https://github.com/Fuco1/smartparens/wiki/Working-with-expressions#navigation-functions (and next section)
;; https://ebzzry.io/en/emacs-pairs/
(use-package smartparens
  :defer 1
  :diminish
  :hook ((prog-mode ielm-mode) . smartparens-strict-mode)
  ;; See `smartparens-strict-mode-map' for remapped manipulation commands (e.g. sp-kill-hybrid-sexp
  ;; as "c-k").
  ;;
  ;; TODO: Modify bindings for non-s-exp languages (e.g. change sp-transpose-sexp =>
  ;; sp-transpose-hybrid-sexp)?
  :bind (:map smartparens-mode-map
         ;;
         ;; Navigation commands (replacements for Emacs built-in versions).
         ;;
         ("C-M-f" . sp-forward-sexp)
         ("C-M-b" . sp-backward-sexp)
         ("M-f" . sp-forward-symbol)
         ("M-b" . sp-backward-symbol)

         ("C-M-n" . sp-next-sexp)
         ("C-M-p" . sp-previous-sexp)

         ("C-M-e" . sp-up-sexp)
         ("C-M-u" . sp-backward-up-sexp)
         ("C-M-d" . sp-down-sexp)
         ("C-M-a" . sp-backward-down-sexp)

         ("C-S-d" . sp-beginning-of-sexp)
         ("C-S-a" . sp-end-of-sexp)

         ;;
         ;; Manipulation commands
         ;;
         ("C-M-t" . sp-transpose-sexp)
         ("C-M-k" . sp-kill-sexp)
         ("C-M-w" . sp-copy-sexp)

         ("M-D" . sp-splice-sexp)
         ("C-S-<backspace>" . sp-splice-sexp-killing-around)

         ("C-<right>" . sp-forward-slurp-sexp)
         ("C-<left>" . sp-forward-barf-sexp)
         ("C-M-<left>" . sp-backward-slurp-sexp)
         ("C-M-<right>" . sp-backward-barf-sexp)

         ("C-]" . sp-select-next-thing-exchange)
         ("C-M-]" . sp-select-next-thing)


         ("M-i" . sp-change-inner)
         ("M-I" . sp-change-enclosing))
  :config
  (require 'smartparens-config)
  (show-smartparens-global-mode t)
  (set-face-attribute 'sp-show-pair-match-content-face nil :inherit 'lazy-highlight)
  (setq sp-navigate-interactive-always-progress-point t)
  (setq sp-navigate-comments-as-sexps nil))

(use-package avy
  :bind (([remap goto-line] . #'avy-goto-char-timer)
         ("M-g r" . #'avy-resume)
         ;; Not sure how much I'll use `avy-next'/`avy-prev' in reality, but in theory they're nice.
         ("M-g an" . #'avy-next)
         ("M-g ap" . #'avy-prev)
         ;; `avy-goto-line' can replace `goto-line' since it also accepts numeric line numbers.
         ("M-g l" . #'avy-goto-line)
         ("M-g h" . #'avy-org-goto-heading-timer)
         :map isearch-mode-map
         ;; Start an isearch, then use `avy-isearch' to jump to a specific match.
         ("M-g" . #'avy-isearch))
  :preface
  (defmacro cc/def-avy-action (name &rest body)
    "Define a function for use with `avy-dispatch-alist'.

- NAME should start with 'avy-action-' for compatibility with avy's help.
- Window and buffer excursion will be saved and BODY will be called with point at avy selection.
- Point arg will be bound to 'pt'."
    (declare (indent 1))
    `(defun ,name (pt)
       (save-window-excursion
         (save-excursion
           (goto-char pt)
           ,@body))))

  (cc/def-avy-action avy-action-kill-whole-line
    ;; Action-only equivalent of `avy-kill-whole-line'
    (kill-whole-line))

  (cc/def-avy-action avy-action-copy-whole-line
    ;; Action-only equivalent of `avy-copy-line'
    (cl-destructuring-bind (start . end)
        (bounds-of-thing-at-point 'line)
      (copy-region-as-kill start end)))

  (defun avy-action-yank-whole-line (pt)
    ;; defun, not `cc/def-avy-action', because this is a wrapper around another.
    (avy-action-copy-whole-line pt)
    (yank))

  (defun avy-action-teleport-whole-line (pt)
    ;; defun, not `cc/def-avy-action', because this is a wrapper around another.
    (let ((line-start-p (<= (point) (save-excursion
                                      (back-to-indentation)
                                      (point))))
          (start-pt (point)))
      (avy-action-kill-whole-line pt)
      (save-excursion (yank))
      (if line-start-p
          (goto-char start-pt) ; Go to end of inserted region
        (progn
          ;; Merge lines together cleanly
          (cycle-spacing 1)
          (end-of-line)
          (delete-indentation t)))))

  (defun avy-action-mark-to-char (pt)
    (activate-mark)
    (goto-char pt))

  :config
  (setq avy-timeout-seconds 0.35)
  ;; Single candidate jump prevents using dispatch actions with a single candidate. Also it
  ;; increases cognitive burden for marginal efficiency benefit.
  (setq avy-single-candidate-jump nil)
  ;; I have too many windows for this, and I can already switch quickly with ace-window.
  (setq avy-all-windows nil)

  (setq avy-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l))
  ;; Note: built-in avy actions like `avy-action-kill-stay', `avy-action-teleport' tend to operate
  ;; on sexp unless name/docstring indicates otherwise.
  (setq avy-dispatch-alist '((?x . avy-action-kill-stay)
                             (?X . avy-action-kill-whole-line)
                             ;; Teleport is basically kill + yank.
                             (?t . avy-action-teleport)
                             (?T . avy-action-teleport-whole-line)
                             (?m . avy-action-mark)
                             (?M . avy-action-mark-to-char)
                             (?w . avy-action-copy)
                             (?W . avy-action-copy-whole-line)
                             (?y . avy-action-yank)
                             (?Y . avy-action-yank-line)
                             (?i . avy-action-ispell)
                             (?z . avy-action-zap-to-char))))

(use-package alert
  :defer t
  :config
  (setq alert-default-style 'libnotify))

;;; Version control/diff

(use-package smerge-mode
  :defer 1
  :after (hydra)
  :bind (:map smerge-mode-map
         ("C-c h" . cc/smerge-hydra/body))
  :hook (magit-diff-visit-file . cc/magit-diff-visit-smerge)
  :config
  (defun cc/magit-diff-visit-smerge ()
    (when smerge-mode
      (cc/smerge-hydra/body)))

  ;; Based on https://github.com/alphapapa/unpackaged.el#smerge-mode
  (defhydra cc/smerge-hydra
    (:color pink :hint nil :post (smerge-auto-leave))
    ("n" #'smerge-next "Next" :column "Move")
    ("p" #'smerge-prev "Prev")
    ("b" #'smerge-keep-base "Base" :column "Keep")
    ("u" #'smerge-keep-upper "Upper")
    ("l" #'smerge-keep-lower "Lower")
    ("a" #'smerge-keep-all "All")
    ("RET" #'smerge-keep-current "Current")
    ("<" #'smerge-diff-base-upper :column "Diff")
    ("=" #'smerge-diff-upper-lower)
    (">" #'smerge-diff-base-lower)
    ("R" #'smerge-refine)
    ("E" #'smerge-ediff)
    ("C" #'smerge-combine-with-next :column "Other")
    ("r" #'smerge-resolve)
    ("k" #'smerge-kill-current)
    ("Z" (lambda ()
           (interactive)
           (save-buffer)
           (bury-buffer))
     "Save and bury buffer" :color blue)
    ("C-g" nil nil))

  (set-face-attribute 'smerge-refined-changed nil :extend t)
  (set-face-attribute 'smerge-refined-removed nil :extend t)
  (set-face-attribute 'smerge-refined-added   nil :extend t))

(use-package diff-hl
  :demand t
  :config
  (setq diff-hl-draw-borders nil)
  (global-diff-hl-mode)
  (diff-hl-flydiff-mode)
  (diff-hl-dired-mode)
  (add-hook 'magit-pre-refresh-hook 'diff-hl-magit-pre-refresh)
  (add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh))


;;; Major modes/languages

;;; Major mode: Org
(require 'cc-org-utils)
(require 'visual-fill-column)

(defface cc/org-link-web-face '((t :inherit org-link :foreground "medium sea green"))
  "`org-link' for web URLs (e.g. http/https), as opposed to e.g. local files."
  :group 'cc/appearance)

(defface cc/org-link-file-remote-face '((t :inherit org-link :foreground "dark orange"))
  "`org-link' for remote (TRAMP) files."
  :group 'cc/appearance)

(defface cc/org-link-file-invalid-face '((t :inherit org-link :foreground "red"))
  "`org-link' for invalid (missing) files."
  :group 'cc/appearance)

(defface cc/org-link-org-id-face '((t :inherit org-link :slant italic))
  "`org-link' for Org IDs."
  :group 'cc/appearance)

(use-package org
  ;; Need to auto-load org-load-modules-maybe for org-store-link to work.
  :commands org-load-modules-maybe
  :bind (("C-c l" . #'org-store-link)
         (:map org-mode-map
          ("<return>" . cc/org-return)
          ("M-S-<return>" . cc/org-insert-todo-heading)
          ("C-S-<return>" . cc/org-insert-todo-heading-respect-content)
          ("C-c M-." . org-time-stamp-inactive)
          ;; C-c , defaults to `org-cycle-agenda-files', which I will never use. Also it conflicts
          ;; with my spell check bindings
          ("C-," . nil)
          ("C-c C-u" . #'cc/org-back-or-up-heading)))
  :hook (org-mode . cc/org-setup)
  :preface
  (defun cc/org-return (&optional arg)
    "Better version of `org-return'.

- With prefix arg or as fallback: `org-return'.
- When on heading (except at beginning): skips over property drawer and other metadata."
    (interactive "P")
    (cond
     (arg
      (org-return))
     ((and (org-at-heading-p)
           (equal (point) (line-end-position)))
      (org-end-of-meta-data t)
      (org-return)
      ;; Better than `previous-line', because this preserves blank line between headings if present.
      (org-back-over-empty-lines))
     (t
      (org-return))))

  (defun cc/org-back-or-up-heading (arg)
    "Better version of `outline-up-heading'.

If on a heading then do `outline-up-heading', otherwise (i.e. when within a
headings contents) then do `org-back-to-heading'."
    (interactive "p")
    (if (org-at-heading-p)
        (outline-up-heading arg)
      (org-back-to-heading)))

  (defun cc/org-insert-todo-heading ()
    (interactive)
    (org-insert-todo-heading '(4)))

  (defun cc/org-insert-todo-heading-respect-content ()
    "Equivalent to calling `org-insert-todo-heading-respect-content' with one prefix argument.

Or in other words, inserts a new todo heading with initial 'todo'
state instead of copying the state of the current heading."
    (interactive)
    ;; '(4) is equivalent to calling with c-u.
    (org-insert-todo-heading-respect-content '(4)))

  (defun cc/org-link-make-description (link description)
    "Used with `org-link-make-description-function'"
    (if (equal description nil)
        (cond
         ((string-match (rx "jira.naic.org/browse/" (group (>= 4 (any alphanumeric "-")))) link)
          (match-string 1 link))
         ((string-match (rx bos "https://" (group (or "gitlab" "github")) ".com/" (group (+ anychar))) link)
          (format "%s:%s" (match-string 1 link) (match-string 2 link)))
         ((string-match (rx bos "help:" (group (+ anychar))) link)
          (format "h:%s" (match-string 1 link)))
         (t description))
      description))

  (defun org-babel-execute:passthrough (body _params)
    "Return BODY as-is.

See https://emacs.stackexchange.com/a/51734"
    body)
  (dolist (alias (list #'org-babel-execute:json
                       #'org-babel-execute:yaml))
    (defalias alias #'org-babel-execute:passthrough))

  (defun cc/org-setup ()
    (add-hook 'before-save-hook #'cc/org-format-blank-lines nil 'local)
    (visual-line-mode)
    ;; `visual-fill-column' should be always be wider than `fill-column' to avoid source blocks
    ;; wrapping (which is a nuisance but not the end of the world).
    (setq visual-fill-column-enable-sensible-window-split t)
    (setq visual-fill-column-width (max 120 (+ fill-column 10)))
    (setq visual-fill-column-center-text t)
    (visual-fill-column-mode)
    (set-face-foreground 'org-headline-done "slate grey")
    (set-face-attribute 'org-level-1 nil :weight 'normal :height 1.2)
    (set-face-attribute 'org-level-2 nil :weight 'normal :height 1.1)
    (set-face-attribute 'org-level-3 nil :weight 'normal :height 1.0))

  ;; For .dir-locals.el customization of journal tags. I wish org would consistently add :safe to
  ;; their defcustoms as appropriate, but oh well.
  (put 'org-tag-persistent-alist 'safe-local-variable
        (lambda (value)
          (seq-every-p
           (lambda (item)
             ;; This doesn't cover "special" tag keywords like :startgroup since I'm not using them
             ;; with dir locals.
             (or (stringp item)
                 (and (consp item)
                      (stringp (car item))
                      (characterp (cdr item)))))
           value)))

  (defun cc/hack-local-variables-org-tags ()
    "Make org tags works in .dir-locals.el.

There's an org mailing list thread on this, but it's dying the death of a
million bike sheds (started in 2020, last updated October 2022). Maybe by the
end of 2023 there will be a better solution?

- https://list.orgmode.org/87v9gsdyhk.fsf@gnu.org/T/
- https://debbugs.gnu.org/cgi/bugreport.cgi?bug=57003

TODO check on the mailing list threads (above) in mid/late 2023...?"
    (when (and (derived-mode-p 'org-mode)
               ;; Don't care about file locals, because org files can already do #+TAGS etc.
               (or (alist-get 'org-tag-alist dir-local-variables-alist)
                   (alist-get 'org-tag-persistent-alist dir-local-variables-alist)))
      (org-set-regexps-and-options 'tags-only)))

  (add-hook 'hack-local-variables-hook #'cc/hack-local-variables-org-tags)

  :config
  ;; Unbind org-agenda-file-to-front and org-remove-file to avoid modifying
  ;; org-agenda-files.
  (unbind-key "C-c [" org-mode-map)
  (unbind-key "C-c ]" org-mode-map)
  ;; Also unbind word wrap, since I use soft wrapping. For some reason that I can't figure out,
  ;; using unbind-key doesn't work for M-q. I realize that sounds insane, but I just spent 15
  ;; minutes trying to figure it out and got nowhere.
  (bind-key "M-q" #'ignore org-mode-map)

  (setq org-directory (expand-file-name "~/org-files"))

  ;; IMPORTANT NOTES for todo keywords:
  ;; - Order matters for e.g. agenda's todo-state-up/-down sorting.
  ;; - Make sure to keep TODO as the first of its sequence, otherwise I need to make sure that
  ;;   `org-todo-repeat-to-state' is set as well as who knows how many other similar variables.
  ;; - Todo keywords are also set as file-local variables using "#+TODO:" in my GTD org files,
  ;;   because that's the only way supported by organice. However I still want this global variable
  ;;   set for the sake of agenda usage, etc. I'm not sure how necessary this is, but I don't want
  ;;   to figure it out. So KEEP FILE-LOCAL VALUES IN SYNC.
  (setq org-todo-keywords
        '((sequence
           "TODO(t)" "WORKING(w)" "BLOCKED(b)" "PENDING(p)" "|"
           "DONE(d)" "CANCELED(c)")
          ;; "|" so that BACKLOG isn't treated as a completed keyword.
          (sequence "BACKLOG(l)" "|")))
  (setq org-todo-keyword-faces
        '(("TODO" . (:inherit org-priority)) ; org-priority is org-todo without the background
          ("BACKLOG" . (:foreground "slate gray" :weight bold))
          ("WORKING" . (:foreground "green" :weight bold))
          ("PENDING" . (:foreground "yellow" :weight bold))
          ("BLOCKED" . (:foreground "red" :weight bold))
          ("DONE" . (:foreground "slate gray" :weight bold))
          ("CANCELED" . (:foreground "slate gray" :weight bold))))
  (setq org-fontify-done-headline t) ; Because spacemacs-theme annoyingly changes this.
  (setq org-hierarchical-todo-statistics nil) ; nil = compute recursively
  (setq org-enforce-todo-dependencies t)
  (setq org-enforce-todo-checkbox-dependencies nil) ; Use this at one point but decided against it.
  (setq org-treat-insert-todo-heading-as-state-change nil)
  (setq org-insert-heading-respect-content t)
  (setq org-log-done 'time)
  (setq org-log-into-drawer t)

  (setq org-tag-persistent-alist
        (append cc/org-tags-context-alist
                '(("urgent" . ?u)
                  ("delegated" . ?d)
                  ("bug" . ?b)
                  ("feature" . ?f)
                  ("maintenance" . ?m)
                  ("finances" . ?p)
                  ("web" . ?w)
                  ("family" . ?F))))

  (setq org-use-speed-commands t)
  (setq org-use-fast-tag-selection t)
  (setq org-fast-tag-selection-single-key nil)

  (setq org-priority-lowest 67)
  (setq org-priority-default org-priority-lowest)

  (setq org-global-properties
        '(("effort_ALL" . "0:05 0:15 0:30 1:00 2:00 4:00 8:00 16:00 24:00")))
  (setq org-effort-property "effort")

  (setq org-plain-list-ordered-item-terminator ?.)

  (setq org-startup-folded 'content)
  (setq org-blank-before-new-entry '((heading . t)
                                     (plain-list-item . nil)))
  (setq org-yank-adjusted-subtrees t)
  (setq org-fold-catch-invisible-edits 'show-and-error)
  (setq org-ctrl-k-protect-subtree t)
  ;; Enabled `org-adapt-indentation' in the past, but I regret it because:
  ;; - Reading content at different heading levels bugs my OCD.
  ;; - Comparing content at different levels is impossible.
  ;; - Editing as plain text (e.g. via organice capture) is harder.
  ;; - Changing heading levels makes git commits noisier
  (setq org-adapt-indentation nil)
  ;; New (as of Org 9.6, late 2022) org element cache persistence is buggy -- and since I use
  ;; long-running Emacs sessions I don't really need cross-session persistence. Specifically,
  ;; enabling `org-element-cache-persistent' causes `org-get-category' to return "???" for /some/ of
  ;; my GTD files.
  (setq org-element-cache-persistent nil)
  ;; Also put `org-persist' files in /tmp to clear on reboot, since it can't be disabled and I don't
  ;; trust it due to guilt by association.
  (setq org-persist-directory (expand-file-name "org-persist" temporary-file-directory))

  (setq org-columns-default-format "%TODO %3PRIORITY %80ITEM %TAGS")

  (setq org-confirm-babel-evaluate nil)
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp . t)
     (shell . t)
     (kubectl . t)
     (http . t)
     (awk . t)))

  (setq org-link-make-description-function #'cc/org-link-make-description)
  (dolist (type '("http" "https"))
    (org-link-set-parameters type :face 'cc/org-link-web-face))
  (org-link-set-parameters
   "file"
   :face (lambda (path)
           (cond
            ((file-remote-p path) 'cc/org-link-file-remote-face)
            ((not (file-exists-p (expand-file-name path))) 'cc/org-link-file-invalid-face)
            (t 'org-link))))
  (org-link-set-parameters "id" :face 'cc/org-link-org-id-face)

  ;; Change float *display* precision for org-calc, e.g. for org table formulas. Two digit precision
  ;; is obviously useful for money, but also a reasonable accuracy for other calculations.
  ;; (plist-put org-calc-default-modes 'calc-float-format '(fix 2))

  (add-to-list 'org-modules 'org-habit))

(use-package org-habit
  :config
  (setq org-habit-graph-column 80))

(require 'all-the-icons)
(use-package org-agenda
  :demand t
  :hook ((org-agenda-mode . cc/org-agenda-setup)
         (org-agenda-finalize . cc/org-agenda-finalize))
  :bind (("C-c a" . org-agenda)
         ("C-c p" . cc/org-gtd-process-start)
         :map org-agenda-mode-map
         ("i" . cc/org-agenda-switch-to-indirect)
         ("C-c b" . cc/org-agenda-filter-backlog))
  :preface
  (defun cc/org-agenda-setup ()
    ;; Associate agenda buffers with org files directory for the sake of project association.
    (setq-local default-directory org-directory))

  (defun cc/org-agenda--gc-threshold (fn &rest args)
    "`org-agenda' advice for performance optimization."
    ;; Optimum threshold determine based on benchmarking repeated (org-agenda "o") executions.
    ;; Temporarily increasing GC threshold from 16 MB to 64 MB makes a roughly 40% difference, for
    ;; example. Using `max' in case `gc-cons-threshold' is already set higher.
    (let ((gc-cons-threshold (max gc-cons-threshold (* 64 1024 1024))))
      (apply fn args)))
  (advice-add #'org-agenda :around #'cc/org-agenda--gc-threshold)

  (defun cc/clocktable-formatter-group-by-prop (ipos all-tables params)
    "Group by first :prop (e.g. category) of PARAMS and promote archived tasks to display normally."
    ;; TODO: refactor this
    ;;  - Split into multiple functions:
    ;;     1. grouping by prop (and associated label/display modifications)
    ;;     2. promoting/dealing with archived tasks
    ;;  - Consider adding getters (`defun') or getters + setters (`define-inline'? or `defun' +
    ;;    `gv-define-setter'?) for task/table lists.
    ;;         https://nullprogram.com/blog/2018/02/14/
    (let* ((result-table (make-hash-table :test #'equal))
           (maxlevel (plist-get params :maxlevel))
           (result-list nil)
           ;; Need to track archive state of previous task to distinguish the root "Archive" heading
           ;; from its children (which are the actual archived tasks).
           (prev-archive-p nil))
      (dolist (table all-tables)
        (dolist (task (nth 2 table)) ; task is '(level headline tags timestamp time properties)
          (let* ((task-key (cdr (car (car (last task)))))
                 (task-level (car task))
                 (task-tags (nth 2 task))
                 (task-time (nth 4 task))
                 (existing (gethash task-key result-table (list task-key 0 nil)))
                 (existing-time (nth 1 existing))
                 ;; If multiple top-level records (i.e. files) are merged, their times need to be
                 ;; combined as well. Example: if grouping by category and multiple files share the
                 ;; same category.
                 (merged-time (if (equal 1 task-level)
                                  (+ existing-time task-time)
                                existing-time))
                 (archive-heading-p (member org-archive-tag task-tags))
                 (archive-root-p (and (not prev-archive-p)
                                      archive-heading-p)))
            (when archive-heading-p
              ;; Promote to appear as a normal (unarchived) task.
              (cl-decf (car task)))
            (unless (or archive-root-p
                        (and maxlevel (> task-level maxlevel)))
              (puthash task-key
                       (list
                        task-key
                        merged-time
                        (cons task (car (last existing))))
                       result-table))
            (setq prev-archive-p archive-heading-p))
          result-table))
      ;; Reverse the list of tasks, since cons created it backwards. This is apparently the
      ;; idiomatic way to build a list from front to back in elisp, since cons is O(1) whereas
      ;; append is O(n) *for each operation*. Thus the whole runtime is O(n) for cons vs O(n^2) for
      ;; append. Of course worrying about big-O is absurd in a case like this where n will be fairly
      ;; small, but it's still the idiomatic way.
      (maphash (lambda (_key value)
                 (let ((last-index (1- (safe-length value))))
                   (setf (nth last-index value) (nreverse (nth last-index value)))
                   (push value result-list)))
               result-table)
      (let ((formatter (or org-clock-clocktable-formatter
                           #'org-clocktable-write-default))
            (group-name (capitalize (car (plist-get params :properties)))))
        ;; KLUDGE WARNING:
        ;; :properties and :tags are used by this formatter and not intended for display. This is a
        ;; heavy handed solution, but it works for my current use case. This may bite me in the
        ;; future though.
        (setf (plist-get params :properties) nil)
        (setf (plist-get params :tags) nil)
        (funcall formatter ipos result-list params)
        ;; "File" isn't the correct column title, so adjust it.
        ;; Reminder: Org tables are 1-indexed, just to make life confusing and painful.
        (org-table-put 1 1 group-name)
        (org-table-analyze) ;; Sets `org-table-dlines' (and others). Dynamic vars are fun.
        (seq-doseq (dl org-table-dlines)
          (when dl
            ;; dlines are 0-indexed, but org-table-goto-* functions are 1-indexed... *barfs*.
            (let ((line (1+ dl))
                  (col 2))
              (org-table-goto-line line)
              (org-table-goto-column col)
              (let ((field (org-table-get-field)))
                (when (string-match-p
                       (rx (any space) "*File time*" (any space))
                       field)
                  (org-table-put line col (format "*%s time*" group-name)))))))
        (org-table-align))))

  (defun cc/clocktable-maxlevel-args-advice (args)
    "Hack to adjust :maxlevel param for `cc/clocktable-formatter-group-by-prop'.

`cc/clocktable-formatter-group-by-prop' promotes archived sibling tasks to a
higher level to merge them in the clock report table, so it needs to receive an
extra level of tasks."
    (let* ((props (cadr args))
           (maxlevel (plist-get props :maxlevel)))
      (if (and maxlevel
               (equal (plist-get props :formatter) #'cc/clocktable-formatter-group-by-prop))
          (let ((ret (list (car args)
                           (plist-put (copy-sequence props) :maxlevel (1+ maxlevel)))))
            (message "new maxlevel: %s" (plist-get (cadr ret) :maxlevel))
            ret)
        args)))
  (advice-add #'org-clock-get-table-data :filter-args #'cc/clocktable-maxlevel-args-advice)

  (defconst cc/org-agenda-backlog-regexp "-^ *BACKLOG")

  (defun cc/org-agenda-filter-backlog ()
    "Toggle regexp filter to hide BACKLOG items."
    (interactive)
    (let* ((do-clear (memq cc/org-agenda-backlog-regexp
                           (get 'org-agenda-regexp-filter :preset-filter)))
           (preset (if do-clear
                       (remove cc/org-agenda-backlog-regexp org-agenda-regexp-filter)
                     (cons cc/org-agenda-backlog-regexp org-agenda-regexp-filter)))
           (regexp org-agenda-regexp-filter))
      (put 'org-agenda-regexp-filter :preset-filter preset)
      (org-agenda-filter-show-all-re)
      ;; Re-apply on-the-fly (non-preset) regexp filter.
      (setq org-agenda-regexp-filter regexp)
      (org-agenda-filter-apply org-agenda-regexp-filter 'regexp)
      (message "Backlog: %s" (if do-clear "visible" "hidden"))))

  (defun cc/org-agenda-finalize ()
    (org-agenda-forward-block)
    (org-agenda-next-item 1))

  (defun cc/org-agenda-switch-to-indirect (arg)
    "Remember follow mode (keybind: F) is also a thing in agenda view."
    (interactive "P")
    (funcall-interactively #'org-agenda-tree-to-indirect-buffer arg)
    (select-window (get-buffer-window org-last-indirect-buffer)))

  :config
  (cc/when-org-files
   (setq org-agenda-files '("~/org-files/gtd"
                            "~/org-files/gtd/areas"))
   ;; Add only the most recent handful of journal files for performance.
   (dolist (file (-->
                  "~/org-files/journal"
                  (directory-files it t (rx ".org" (? ".gpg") eos))
                  (seq-reverse it)
                  (seq-take it 3)))
     (cl-pushnew file org-agenda-files :test #'string=)))

  (cl-flet ((get-icon (icon face)
                      (list (all-the-icons-material icon :face face :height 1.0))))
    (setq org-agenda-category-icon-alist
          `(("work" ,(get-icon "work" 'all-the-icons-blue-alt))
            ("professional" ,(get-icon "code" 'all-the-icons-lsilver))
            ("hobbies" ,(get-icon "favorite" 'all-the-icons-lred))
            ("misc" ,(get-icon "assignment" 'all-the-icons-yellow))
            ("journal" ,(get-icon "bookmark" 'all-the-icons-lgreen))
            ;; Empty string category is used for e.g. placeholder lines in time grid.
            (,(rx string-start string-end) (space . (:width (20))))
            ;; Key is a regexp, so this is the default for any other categories. Need to exclude
            ;; empty string because that's used as category for e.g. empty time-grid lines.
            (,(rx (one-or-more anything)) ,(get-icon "remove" nil)))))
  (setq org-agenda-prefix-format
        '((agenda . " %i %?-12t% s")
          (todo . " %i [%5e] ")
          (tags . " %i ")
          (search . " %i ")))
  ;; Add separator between days in agenda for readability.
  (setq org-agenda-format-date (lambda (date)
                                 (concat "\n"
                                         (make-string (window-width) 9472)
                                         "\n"
                                         (org-agenda-format-date-aligned date))))
  (setq org-agenda-clockreport-parameter-plist
        '(:link t :maxlevel 2 :fileskip0 t :emphasize t :compact nil :narrow nil :formula %
          :properties ("CATEGORY") :tags t ; props + tags needed by my formatter.
          :formatter cc/clocktable-formatter-group-by-prop))
  (setq org-agenda-block-separator nil) ; redundant since date has a separator now.
  (setq org-agenda-confirm-kill nil) ; This prompts gets old after the 100th time or so.
  (setq org-agenda-bulk-custom-functions '((?k org-agenda-kill)))

  (setq org-agenda-window-setup 'current-window)
  (setq org-agenda-restore-windows-after-quit nil)
  (setq org-agenda-start-with-follow-mode nil)
  (setq org-agenda-follow-indirect t)
  (setq org-agenda-start-with-log-mode nil)
  (setq org-agenda-start-on-weekday nil) ; Start today, not a constant day of the week
  (setq org-agenda-compact-blocks nil)
  ;; See `org-agenda-search-view-always-boolean' documentation, but TLDR:
  ;; - Split on whitespaces, like orderless.
  ;; - Treat as a single search term with "double quotes around it".
  ;; - Surround regexp in {curly braces}
  ;; - Negate a phrase with -minus.
  (setq org-agenda-search-view-always-boolean t)

  (setq org-agenda-timegrid-use-ampm t)
  (setq org-agenda-todo-keyword-format "%-10s") ; fixed-width todo column
  (setq org-stuck-projects '("+LEVEL=1-TODO={.+}-CATEGORY=\"journal\""
                             ("TODO" "WORKING")
                             nil
                             "^* Other$"))

  (setq org-agenda-span 1)
  (setq org-agenda-skip-deadline-if-done t)
  (setq org-agenda-skip-scheduled-if-done t)
  (setq org-agenda-skip-scheduled-if-deadline-is-shown t)
  (setq org-deadline-warning-days 5)
  ;; ignore-scheduled: applies to todo views, not agenda. integer means ignore >= x days in future.
  (setq org-agenda-todo-ignore-scheduled nil)

  (setq org-agenda-sorting-strategy
        '((agenda habit-down time-up todo-state-down priority-down category-up)
          (todo todo-state-down priority-down category-up)
          (tags todo-state-down priority-down category-up)
          (search todo-state-down priority-down category-up)))

  ;; Explicitly disabling `org-agenda-sticky' (even though that's the default) as a reminder: *do
  ;; not* enable it because there are lots of edge cases. For example:
  ;; - `cc/org-agenda-filter-backlog' controls the "preset" regexp filter using `get' and `put',
  ;;   which don't have a buffer-local equivalent.
  ;; - `org-agenda-overriding-header' customization leaks between agenda instances.
  ;;
  ;; Even if the above get fixed, I assume there are other bugs as well because org agenda code is
  ;; crazy. It's not worth time fighting bugs in this obscure sticky feature.
  (setq org-agenda-sticky nil))

(use-package org-clock
  :demand t
  :bind ("s-c" . #'cc/org-clock-hydra)
  :hook (org-after-todo-state-change . cc/org-clock-in-if-working)
  :preface
  (defun cc/org-clock-goto-agenda-overview ()
    (interactive)
    (org-agenda nil "o")
    (org-agenda-clock-goto))

  (defun cc/org-clock-effort ()
    "Hybrid of `org-set-effort' and `org-clock-modify-effort-estimate'.

Because `org-clock-modify-effort-estimate' doesn't show completion candidates
of predefined effort values."
    (interactive)
    (save-window-excursion
      (save-excursion
        (org-clock-goto)
        (org-set-effort))))

  (cc/dynamic-hydra cc/org-clock-hydra (:color blue :post (cc/file-auto-save-do-save)) ""
    (t . ("i" (funcall-interactively #'org-clock-in-last '(4)) "Clock in (recent)" :column "Clock changes"))
    ((derived-mode-p 'org-mode 'org-agenda-mode) .
     ("M-i"
      (if (derived-mode-p 'org-agenda-mode)
          (org-agenda-clock-in)
        (org-clock-in))
      "         (point)"))
    ((org-clocking-p) . ("o" #'org-clock-out "Clock out"))
    ((org-clocking-p) . ("x" #'org-clock-cancel "Cancel clock"))
    (t . ("r" #'org-resolve-clocks "Resolve (adjust) clock"))
    ((org-clocking-p) . ("e" #'cc/org-clock-effort "Modify effort"))
    (t . ("jr" (funcall-interactively #'org-clock-goto '(4)) "Goto: recent" :column "View"))
    ((org-clocking-p) . ("jj"  #'org-clock-goto "Goto: current (file)"))
    ((org-clocking-p) . ("ja" #'cc/org-clock-goto-agenda-overview "Goto: current (agenda)"))
    ((derived-mode-p 'org-mode) . ("d" #'org-clock-display "Display summaries"))
    ((cc/org-clock-ticket-p) . ("tt" #'cc/org-clock-browse-ticket "Browse" :column "Ticket"))
    ((cc/org-clock-ticket-p) . ("tu" #'cc/org-clock-insert-ticket-url "Insert (URL)"))
    ((cc/org-clock-ticket-p) . ("ti" #'cc/org-clock-insert-ticket     "       (ID)")))

  (defun cc/org-clock-in-if-working ()
    (if (string= (org-get-todo-state) "WORKING")
        (org-clock-in)))

  (defun cc/org-clock-advice (fn &rest args)
    "Because showing a notification immediately when clocking in is a nuisance"
    (cc/with-advice #'org-notify :override (lambda (&rest _notify-args))
      (apply fn args)))

  :config
  (advice-add #'org-clock-in :around #'cc/org-clock-advice)
  (setq org-clock-clocked-in-display nil)
  (setq org-clock-persist-file (expand-file-name "org-clock-save.el" cc/user-cache))
  (setq org-clock-persist 'history)
  (setq org-clock-history-length 20)
  (setq org-clock-out-remove-zero-time-clocks t)
  (setq org-clock-report-include-clocking-task t)
  ;; `org-clock-idle-time' default is nil, but it's here explicitly to remind future me not to use
  ;; it. The reminder is a nuisance and doesn't really have any value since I have a polybar section
  ;; showing the current clock task. It's easier to just edit the clock directly, call
  ;; `org-clock-cancel', or whatever -- especially since the idle prompt doesn't work when an EXWM
  ;; buffer is focused.
  (setq org-clock-idle-time nil)
  (org-clock-persistence-insinuate))

(use-package org-duration
  :config
  ;; `org-duration-format' is used by e.g. clock report tables. I want to show durations exclusively
  ;; in hours, because trying to mentally multiply/divide by 24 when long durations are shown in
  ;; days (as is the default) is painful. I don't think of long durations in days, nor do most
  ;; people. Also, using "days" for durations really doesn't make sense because it could mean so
  ;; many things. 24 hours? 16ish waking hours? 8ish working hours? Exactly.
  (setq org-duration-format (remove '("d" . nil) org-duration-format)))

(use-package org-archive
  :bind (:map org-mode-map
         ;; Seems like using the default function should be the (ahem) default keybinding.
         ([remap org-archive-subtree] . #'org-archive-subtree-default)
         :map org-agenda-mode-map
         ([remap org-agenda-archive] . #'org-agenda-archive-default))
  :config
  ;; Archiving to a separate file makes it nearly impossible to work with historic data (e.g. in
  ;; agenda views and clock report tables). Of particular note:
  ;; - There isn't a built-in way to create a matching outline hierarchy in archive file (but it can
  ;;   at least be done).
  ;; - The ARCHIVE_OLPATH property is infuriatingly useless due to ambiguity with "/", which could
  ;;   either be a path separator or a literal slash character in the heading text.
  ;; - The archive file obviously won't be kept in sync with any edits to outline paths in the
  ;;   original file.
  ;; - There isn't a (sane) way to merge tasks from a flat archive file into a clockreport table.
  ;;   This could be mitigated by recreating the outline path hierarchy in the outline file, but
  ;;   then the previous problems apply here too.
  ;;
  ;; So TLDR: org mode is simultaneously awesome and infuriating, as usual.
  (setq org-archive-default-command #'org-archive-to-archive-sibling))

(use-package org-contrib
  :after (org)
  :config
  (add-to-list 'org-modules 'ol-man)
  (add-to-list 'org-modules 'org-expiry)
  (add-to-list 'org-modules 'org-checklist))

(require 'org-capture)

(use-package org-expiry
  :hook ((org-after-todo-state-change . cc/org-created-date)
         (org-capture-prepare-finalize . cc/org-created-date-capture))
  :config
  (defun cc/org-created-date ()
    "Adds created property EXCEPT during org-roam and journal captures.

Don't want to add created date during org-roam capture because it creates a new
file which may not have a heading, so there's no way to add a heading property
consistently. Also I just don't care about this property for org roam files,
since I can also check git history trivially, whereas using git history to
figure out a todo creation is painful due to how many times the todo item may
have been refiled."
    (unless (or (and (fboundp 'org-roam-capture-p)
                     (org-roam-capture-p))
                ;; `org-capture-prepare-finalize-hook' runs in capture buffer, which means I have to
                ;; do all this nonsense to get the capture destination file. Also since this
                ;; function is used for `org-after-todo-state-change-hook' too,
                ;; `org-capture-current-plist' might be nil.
                (when-let ((capture-file (and org-capture-current-plist
                                              (buffer-file-name
                                               (plist-get org-capture-current-plist :buffer)))))
                  (string-prefix-p
                   (expand-file-name "~/org-files/journal/")
                   (expand-file-name capture-file))))
      (org-expiry-insert-created)))

  (defun cc/org-created-date-capture()
    "`cc/org-created-date' wrapper for `org-capture'."
    (save-mark-and-excursion
      ;; Created date should go on top level heading of the task if there are subheadings. I'm not
      ;; *positive* how this works if capturing a subheading and manually widening to view the whole
      ;; buffer, but `org-capture-prepare-finalize-hook' says "The capture buffer is current and
      ;; still narrowed".
      (while (org-up-heading-safe))
      (cc/org-created-date)))

  (setq org-expiry-inactive-timestamps t))

;; https://github.com/misohena/phscroll
(with-eval-after-load 'org
  (require 'org-phscroll))

;; https://github.com/alphapapa/org-super-agenda
(use-package org-super-agenda
  :demand t
  :preface
  (defun cc/org-super-agenda-auto-category-path (item)
    (org-super-agenda--when-with-marker-buffer (org-super-agenda--get-marker item)
      (seq-reduce (lambda (a b) (concat a "/" b))
                  (org-get-outline-path)
                  (capitalize (org-get-category)))))

  (defun cc/org-super-agenda-future-habit-p (item)
    (org-super-agenda--when-with-marker-buffer (org-super-agenda--get-marker item)
      (and (org-is-habit-p)
           (when-let ((scheduled (org-entry-get (point) "SCHEDULED")))
             (> (round (org-time-string-to-seconds scheduled))
                (time-convert nil 'integer))))))

  (defun cc/cmp-date-property (prop)
    "Compare two `org-mode' agenda entries, `A' and `B', by some date property.

If a is before b, return -1. If a is after b, return 1. If they
are equal return nil.

From https://emacs.stackexchange.com/questions/26351/custom-sorting-for-agenda"
    #'(lambda (a b)
        (let* ((a-pos (get-text-property 0 'org-marker a))
               (b-pos (get-text-property 0 'org-marker b))
               (a-date (or (org-entry-get a-pos prop)
                           (format "<%s>" (org-read-date t nil "now"))))
               (b-date (or (org-entry-get b-pos prop)
                           (format "<%s>" (org-read-date t nil "now"))))
               (cmp (compare-strings a-date nil nil b-date nil nil)))
          (if (eq cmp t) nil (cl-signum cmp)))))

  (defun cc/org-agenda-time-prop (prop)
    ;; This is without a doubt the most ridiculous way to display a date in a different format of
    ;; any programming language I've seen.
    (if-let* ((created-str (org-entry-get (point) prop t))
              (created (org-read-date nil t created-str nil)))
        (format-time-string "%b %e, %Y" created)
      ""))

  (defun cc/org-agenda-create-project-views ()
    ;; Remove existing project views to be idempotent.
    (setq org-agenda-custom-commands
          (cl-delete-if (lambda (it) (string-prefix-p "p" (car it)))
                        org-agenda-custom-commands))
    (push '("p" . "Projects") org-agenda-custom-commands)
    (push '("p." "All projects (planning)" todo ""
            ((org-super-agenda-groups
              `((:discard (:category "inbox"))
                (:discard (:pred cc/org-super-agenda-future-habit-p))
                (:auto-map cc/org-super-agenda-auto-category-path)))
             (org-agenda-regexp-filter-preset `(,cc/org-agenda-backlog-regexp))))
          org-agenda-custom-commands)
    (cc/when-org-files
     (save-window-excursion
       (with-no-new-buffers
        (dolist (file (sort (cc/get-org-files "gtd/areas") #'string-greaterp))
          (with-current-buffer (find-file-existing file)
            (goto-char 0)
            (let* ((category (org-get-category))
                   ;; Warning: naively assumes every category starts with a unique letter.
                   (key (concat "p" (substring category 0 1)))
                   (title (concat "Projects: " category)))
              (push `(,key ,title tags-todo ,(format "CATEGORY=\"%s\"" category)
                           ((org-super-agenda-groups
                             '((:discard (:pred cc/org-super-agenda-future-habit-p))
                               (:auto-parent t)))
                            (org-agenda-overriding-header ,(concat "Projects: " category))
                            (org-agenda-regexp-filter-preset `(,cc/org-agenda-backlog-regexp))))
                    org-agenda-custom-commands))))))))

  (defun cc/org-agenda-create-historic-views ()
    (setq org-agenda-custom-commands
          (cl-delete-if (lambda (it) (string-prefix-p "h" (car it)))
                        org-agenda-custom-commands))
    (push '("h" . "Historic Logs") org-agenda-custom-commands)
    ;; Yeah I know, a month != 30 days, but it works well enough. Unfortunately I can't use an
    ;; actual 'month span here, since I need to calculate the start day offset.
    (dolist (config '((:name "month" :key "m" :span 30)
                      (:name "week" :key "w" :span 7)
                      (:name "day" :key "h" :span 1)))
      (let ((key (concat "h" (plist-get config :key)))
            (name (concat "Historic Log: " (plist-get config :name)))
            (start-day (format "-%sd" (1- (plist-get config :span)))))
        (push `(,key ,name agenda ""
                     ((org-agenda-overriding-header ,name)
                      (org-super-agenda-groups
                       '((:name "Journal" :category "journal")
                         (:name "Closed" :log closed)
                         (:name "Clocked" :log clocked)
                         ;; State changes only shown after "vL" agenda keybind
                         (:name "State changes" :log t)
                         (:discard (:anything t))))
                      (org-agenda-start-with-log-mode '(closed clock))
                      (org-agenda-start-with-clockreport-mode t)
                      (org-agenda-archives-mode t)
                      (org-agenda-span ,(plist-get config :span))
                      (org-agenda-start-day ,start-day)))
              org-agenda-custom-commands))))

  (defun cc/org-actionable-todo-p (&optional additional-states)
    (let ((state (org-get-todo-state)))
      (and
       (seq-some (lambda (s) (string= state s))
                 (cons "TODO" additional-states))
       (not (org-is-habit-p)))))

  (defun cc/org-count-prev-sibling-todos ()
    (let ((count 0))
      (save-excursion
        (while (org-goto-sibling t)
          (when (cc/org-actionable-todo-p)
            (cl-incf count))))
      count))

  (defun cc/org-prev-sibling-todos-p ()
    (let ((found nil))
      (save-excursion
        (while (and (not found)
                    (org-goto-sibling t))
          (when (cc/org-actionable-todo-p)
            (setq found t))))
      found))

  (defun cc/org-agenda-skip-gtd-siblings ()
    (unless (and (cc/org-actionable-todo-p (remove "BACKLOG" org-not-done-keywords))
                 (not (cc/org-prev-sibling-todos-p)))
      (or (outline-next-heading)
          (goto-char (point-max)))))

  :config
  (setq org-super-agenda-groups nil) ; Don't use for normal agenda views.

  ;; `org-agenda-custom-commands' format is... fun. Note that there are two different places
  ;; to modify variables for individual parts of a composite view vs the entire set, as
  ;; explained in the docstring and illustrated here:
  ;; https://orgmode.org/list/C2B9E4DB-138E-4E3E-83C6-57E7ADD2D5F7@gmail.com/
  (setq org-agenda-custom-commands
        `(("o" "Overview"
           ((alltodo "" ((org-super-agenda-groups
                          '((:name "Urgent"
                             :and (:tag "urgent" :not (:scheduled future)))
                            (:name "WIP"
                             :todo "WORKING")
                            (:name "Idle"
                             :and (:todo ("BLOCKED" "PENDING") :not (:scheduled future)))
                            (:discard (:anything t))))))
            (agenda "" ((org-agenda-overriding-header "")
                        (org-agenda-span 3)
                        (org-super-agenda-groups
                         '((:discard (:pred cc/org-super-agenda-future-habit-p))
                           (:name "Habits"
                            :order 2
                            :habit t)
                           (:name "Overdue"
                            :order 0
                            :deadline past
                            :scheduled past)
                           (:name "Day"
                            :order 1
                            :time-grid t
                            :deadline today
                            :scheduled today)
                           (:name "Upcoming"
                            :order 3
                            :deadline future
                            :scheduled future)
                           (:name "Other"
                            :order 4
                            :anything t)))))))
          ("u" "Upcoming" todo ""
           ((org-super-agenda-groups
             `((:discard (:habit t))
               (:name "Deadlines: Overdue" :deadline past )
               (:name "Deadlines: Today" :deadline today)
               (:name "Scheduled: Overdue" :scheduled past)
               (:name "Scheduled: Today" :scheduled today)
               (:auto-planning)
               (:discard (:anything t))))))
          ("n" . "Next")
          ("nn" "Next: project heads (GTD style)" todo ""
           ((org-super-agenda-groups
             `((:discard (:category "inbox"))
               (:auto-map cc/org-super-agenda-auto-category-path)))
            (org-agenda-skip-function #'cc/org-agenda-skip-gtd-siblings)))
          ("n@" "Next: @context" todo ""
           ((org-super-agenda-groups
             `((:discard (:category "inbox"))
               (:discard (:pred cc/org-super-agenda-future-habit-p))
               ;; Dynamically generate @context tag groups
               ,@(seq-map
                  (lambda (next)
                    (let ((context (if (stringp next)
                                       next
                                     (car next))))
                      `(:name ,context :tag ,context)))
                  cc/org-tags-context-alist)))
            (org-agenda-regexp-filter-preset `(,cc/org-agenda-backlog-regexp))))
          ("nc" "Next: category" todo ""
           ((org-super-agenda-groups
             `((:discard (:category "inbox"))
               (:discard (:pred cc/org-super-agenda-future-habit-p))
               (:auto-category t)))
            (org-agenda-regexp-filter-preset `(,cc/org-agenda-backlog-regexp))))
          ("ne" "Next: effort" todo ""
           ((org-super-agenda-groups
             `((:discard (:category "inbox"))
               (:discard (:pred cc/org-super-agenda-future-habit-p))
               ,@(seq-map
                  (lambda (value)
                    ;; +1 because there isn't an effort-less-than-or-equal selector.
                    `(:name ,(format "<= %s minutes" value)
                      :effort< ,(number-to-string (1+ value))))
                  '(5 15 30 60 120 240))))
            (org-agenda-regexp-filter-preset `(,cc/org-agenda-backlog-regexp))))
          ("r" "Review completed"
           ((todo "DONE|CANCELED"
                  ((org-agenda-cmp-user-defined (cc/cmp-date-property "CLOSED"))
                   (org-agenda-sorting-strategy '(user-defined-up))
                   (org-agenda-prefix-format '((todo . " %i %(cc/org-agenda-time-prop \"CLOSED\") [%5e] ")))
                   (org-agenda-todo-list-sublevels nil)
                   (org-super-agenda-groups
                    `((:auto-category t)))))))
          ("c" "By created date" todo ""
           ((org-agenda-cmp-user-defined (cc/cmp-date-property "CREATED"))
            (org-agenda-sorting-strategy '(user-defined-down))
            (org-agenda-prefix-format '((todo . " %i %(cc/org-agenda-time-prop \"CREATED\") ")))
            (org-super-agenda-groups '((:name "Created date" :anything t)))))
          ("b" . "Buffer (occur trees)")
          ("bd" "Buffer: done/canceled" occur-tree "^\\*+ +\\(DONE\\|CANCELED\\)")
          ("bl" "Buffer: backlog" occur-tree "^\\*+ +BACKLOG")
          ("bb" "Buffer: active" occur-tree "^\\*+ +\\(TODO\\|WORKING\\|BLOCKED\\|PENDING\\)")))

  ;; Re-run this to regenerate if a category is added/removed.
  (cc/org-agenda-create-project-views)
  (cc/org-agenda-create-historic-views)
  (org-super-agenda-mode))

(use-package org-list
  :defer t
  :preface
  (defun cc/org-toggle-item-fix-point (fn &rest args)
    "Fix quirk with `org-toggle-item': prevent going to beginning of line if item is empty."
    (cl-flet ((at-bol-p () (equal (point) (line-beginning-position))))
      (let ((start-at-bol (at-bol-p)))
        (apply fn args)
        (when (and (not start-at-bol)
                   (at-bol-p))
          (goto-char (line-end-position))))))
  :config
  (advice-add #'org-toggle-item :around #'cc/org-toggle-item-fix-point)

  (setq org-list-allow-alphabetical t))

(use-package org-refile
  :defer t
  :bind (:map org-mode-map
         ("C-c C-e" . cc/org-refile-file))
  :preface
  (require 'seq)
  (defun cc/org-refile-sanitize-history (&rest _ignored)
    "Strip trailing slashes in `org-refile-history' to prevent duplicates.

Related bug (see whole thread):
https://www.mail-archive.com/emacs-orgmode@gnu.org/msg124695.html"
    (setq org-refile-history
          (seq-map (lambda (item)
                     (replace-regexp-in-string (rx (1+ "/") string-end) "" item))
                   org-refile-history)))
  (defun cc/org-refile-file (&optional arg)
    "Like `org-refile', but with current file headings as targets."
    (interactive "P")
    (let ((org-refile-targets '((nil . (:maxlevel . 99))))
          ;; t instead of 'file because this is scoped to only the current file.
          (org-refile-use-outline-path t))
      (org-refile arg)))
  :config
  (advice-add #'org-refile :before #'cc/org-refile-sanitize-history)
  ;; For more complex refile targeting, use `org-refile-target-verify-function'.
  (cc/when-org-files
   (setq org-refile-targets
         `((,(cc/get-org-files "gtd") . (:todo . "fake-todo-force-top-level"))
           (,(cc/get-org-files "gtd/areas") . (:maxlevel . 1)))))
  ;; 'file is the only way to allow refiling as a top-level item.
  (setq org-refile-use-outline-path 'title)
  (setq org-outline-path-complete-in-steps nil))

(use-package doct
  :defer t
  :config
  ;; Use doct with `org-roam-capture-templates'
  ;; https://github.com/progfolio/doct/issues/16#issuecomment-633822107
  (defun +doct-org-roam (groups)
    (let (converted)
      (dolist (group groups)
        (let* ((props (nthcdr (if (= (length group) 4) 2 5) group))
               (roam-properties (plist-get (plist-get props :doct) :org-roam)))
          (push `(,@group ,@roam-properties) converted)))
      (setq doct-templates (nreverse converted))))
  (setq doct-after-conversion-functions '(+doct-org-roam)))

(use-package org-capture
  :bind (("C-c c" . org-capture))
  :preface
  ;; Stop org-capture from messing with my window layout.
  ;; https://stackoverflow.com/questions/54192239/open-org-capture-buffer-in-specific-window
  (defun cc/org-capture-place-template-fix-windows (fn &rest args)
    (cl-letf (((symbol-function 'delete-other-windows) 'ignore))
      (apply fn args)))

  (defun cc/org-capture-after-finalize ()
    "Hack to fix org quirk with `auto-revert-mode'.

Auto revert mode triggers after capture as expected, except that the newly
capture heading won't be visible if the previous heading is folded and has a
:PROPERTIES: section. This is probably an org bug that should be reported,
but... one day.

This was reverse-engineered from deep within `org-capture-goto-last-stored'. Of
particular interest are the calls to `bookmark-jump' and (via hook)
`org-bookmark-jump-unhide'."
    (when-let* ((buffer (marker-buffer org-capture-last-stored-marker)))
      (when (buffer-live-p buffer)
        (with-current-buffer buffer
          (goto-char (marker-position org-capture-last-stored-marker))
          (org-fold-show-context)))))

  (defun cc/org-capture--template-append (maybe-link maybe-region)
    (let ((link-p (not (string-empty-p maybe-link)))
          (region-p (not (string-empty-p maybe-region))))
      (cond
       ((and link-p region-p)
        (format "%%? [[%s][%s]]" maybe-link maybe-region))
       (link-p
        (format "%%? [[%s]]" maybe-link))
       (region-p
        (format "%%? =%s=" maybe-region))
       (t
        ""))))

  :config
  (advice-add 'org-capture-place-template
              :around #'cc/org-capture-place-template-fix-windows)

  (setq org-capture-templates
        (doct
         `((:group "Inbox"
            :file "~/org-files/gtd/inbox.org"
            :after-finalize cc/org-capture-after-finalize
            ;; Even though I have a save hook for blank lines, doing it here in the capture template
            ;; prevents quirks with org heading folding and refile markers.
            :empty-lines-before 1
            :children
            (("Task"
              :keys "c" ;; c because this is my main type, so I can spam "C-c c c"
              :template ("* TODO %?"))
             ("[Work] Jira ticket"
              :keys "w"
              :template ("* TODO [[https://jira.naic.org/browse/%^{Ticket}][%\\1]]: %? :ticket:"))))
           ("[Work] Meeting"
            :keys "m"
            :id "b920c2a8-1ef0-44a6-8e67-c3f4a1ff41b3"
            :empty-lines-before 1
            :jump-to-captured t
            :template ("* %?\n  %T"))
           ("Append to clocked"
            :keys "a"
            :clock t
            :type item
            :template ("%(cc/org-capture--template-append \"%L\" \"%i\")")
            ;; Narrow to clocked task by combining :unnarrowed and :hook, since org capture
            ;; templates don't have a ":unnarrowed 'subtree" option.
            :unnarrowed t
            :hook ,#'org-narrow-to-subtree)
           ("Journal"
            :keys "j"
            :file ,(expand-file-name (format-time-string "%Y%m.org") "~/org-files/journal")
            :before-finalize (lambda ()
                               ;; Make sure current journal file is in agenda, in case this is the
                               ;; first capture of a new file.
                               (cl-pushnew (buffer-file-name
                                            (plist-get org-capture-current-plist :buffer))
                                           org-agenda-files
                                           :test #'string=))
            :empty-lines-before 1
            :datetree t
            ;; %^g is tags in file, %T is active timestamp (with date /and/ time)
            :template ("* %? %^g"
                       "%T"))))))

(use-package org-attach
  :config
  (setq org-attach-id-dir (expand-file-name "org-attach" org-directory))
  (setq org-attach-dir-relative t)
  (setq org-attach-store-link-p 'attached))

(use-package org-src
  :defer t
  :config
  (setq org-src-window-setup 'split-window-below))

(use-package ob-async
  :demand t)

(use-package org-id
  :demand t
  :config
  (setq org-id-link-to-org-use-id 'create-if-interactive)
  (setq org-id-locations-file (expand-file-name "org-id-locations" cc/user-cache)))

(use-package todoist
  :defer t ; To avoid GPG prompt from auth-source until necessary
  :config
  (setq todoist-token (auth-source-pick-first-password
                       :host "todoist.com")))

(use-package ob-http
  :config
  (setq ob-http:remove-cr t))

;;; Major mode: PDF
;; https://github.com/politza/pdf-tools/
;; M-x `pdf-tools-help', but of particular note:
;; - "SPC"/"S-SPC" or "n"/"p" for next/prev page.
;; - Zoom: "P" to fit to page, "+" and "-" to zoom in/out
;; - Outline: imenu or "o" for outline in separate buffer
;; - Search with occur
(use-package pdf-tools
  :config
  (pdf-tools-install t)
  (setq-default pdf-view-display-size 'fit-page))

(use-package org-pdftools
  :hook (org-mode . org-pdftools-setup-link))

;;; Major modes: Java/Kotlin
;; TODO: lsp-java

(require 'cc-source-code)
(use-package kotlin-mode
  :defer t
  :init
  (cc/source-code-enable-eglot 'kotlin-mode))

;;; Major mode: JavaScript/TypeScript

;; LSP doesn't have syntax highlighting, so typescript-mode takes care of that. Syntax highlighting
;; seems like it would be a fundamental part of the LSP protocol, but apparently not. Oh well.
;; TODO: use tree-sitter `typescript-ts-mode' and `tsx-ts-mode'
(use-package typescript-mode
  :defer t
  :mode ("\\.ts\\'" "\\.tsx\\'")
  :init
  (cc/source-code-enable-eglot 'typescript-mode))

;; js/jsx mode
;; TODO: use tree-sitter `js-ts-mode'
(cc/with-eval-after-load 'js
  ;; js keymap is useless to me, because it e.g. rebinds M-. which I use with lsp.
  (setq js-mode-map (make-sparse-keymap)))
(cc/source-code-enable-eglot 'js-mode)

;;; Major modes: HTML and CSS
;; TODO: use tree-sitter `css-ts-mode'
(use-package web-mode
  :mode "\\.html?\\'"
  :init
  (cc/source-code-enable-eglot 'web-mode)
  :config
  (setq web-mode-attr-indent-offset 4))

(cc/source-code-enable-eglot 'css-mode)

;;; Major modes: Docker/Kubernetes/related
;; Kubel doesn't have an info page, but doc is here: https://github.com/abrochard/kubel
(use-package kubel
  :bind (("C-c k" . cc/hydra-kubel/body))
  :preface
  (defun cc/aws-sso ()
    (interactive)
    (cc/with-home-dir
     (start-process-shell-command "aws-sso" nil
                                  "aws sso login")))
  :config
  (defhydra cc/hydra-kubel (:color blue)
    ("k" kubel "Kubel" :column "Kubel")
    ("r" kubel-set-resource        "Resource")
    ("c" kubel-set-context         "Context")
    ("n" kubel-set-namespace       "Namespace")
    ("l"  cc/aws-sso               "AWS login" :column "Misc"))
  ;; kubel-use-namespace-list's default value ('auto) doesn't work quite right and likely won't be
  ;; fixed anytime soon due to kubectl edge cases. See:
  ;; https://github.com/abrochard/kubel/pull/44
  (setq kubel-use-namespace-list 'on)
  (kubel-vterm-setup))

(add-to-list 'auto-mode-alist `(,(rx (or bot "/" "\\") "Dockerfile" eot) . dockerfile-ts-mode))

;;; Major mode: conf
(use-package conf-mode
  :hook (conf-mode . cc/conf-mode-hook)
  :config
  (defun cc/conf-mode-hook ()
    (setq-local tab-width 4)))

;;; Major mode: log files
(progn
  (add-to-list 'auto-mode-alist '("\\.log\\'" . auto-revert-tail-mode))
  (add-to-list 'auto-mode-alist '("\\.log\\'" . read-only-mode)))

(provide 'cc-kitchen-sink)
