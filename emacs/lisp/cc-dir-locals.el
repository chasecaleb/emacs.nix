;;; -*- lexical-binding: t -*-

(defun cc/dir-locals-init ()
  ;; Emacs internals INCONSISTENTLY mix tabs and spaces, to atrocious effect... frequently within
  ;; neighboring lines of the same function, even. Half their indentation doesn't look right unless
  ;; you use the same `tab-width'... which completely defeats the point of the (insane) "tabs for
  ;; indentation and spaces for alignment" crowd. It's particularly frustrating because lisp is hard
  ;; to read with messy indentation.
  (let ((class 'gross-indentation)
        (base (format "/run/current-system/sw/share/emacs/%s" emacs-version)))
    (dir-locals-set-class-variables class
                                    '((emacs-lisp-mode . ((tab-width . 8)))
                                      (c-mode          . ((tab-width . 8)))))
    ;; Setting dir locals on one directory higher doesn't seem to take effect, so set for both C
    ;; sources and lisp. Not sure what's going on and I'm doing fucking with this.
    (dolist (dir (list (expand-file-name "src"  base)
                       (expand-file-name "lisp"  base)))
      ;; Add both the symlink /run/current-system and the underlying /nix/store paths, since either
      ;; one could be used depending on how the file is visited.
      (dir-locals-set-directory-class dir class)
      (dir-locals-set-directory-class (file-truename dir) class))))

(provide 'cc-dir-locals)
