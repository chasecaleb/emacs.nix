;;; -*- lexical-binding: t -*-
(require 'vterm)
(require 'hl-line)
(require 'cc-utils)
(require 'cc-completions-utils)

(defun cc/vterm--mode-setup ()
  (setq-local global-hl-line-mode nil)
  (set-process-query-on-exit-flag (get-buffer-process (current-buffer)) nil))

(cc/completions-buffer-switcher "vterm"
                                (derived-mode-p 'vterm-mode)
                                (vterm t))

(defun cc/vterm-init ()
  (add-hook 'vterm-mode-hook #'cc/vterm--mode-setup)
  (global-set-key (kbd "s-v") #'cc/completions-buffer-switcher-vterm)
  ;; Use nil to remove keybinds from vterm-mode-map (to use the normal Emacs bindings instead).
  ;; DO NOT mess with `vterm-keymap-exceptions' because that breaks other bindings, see:
  ;; https://github.com/akermu/emacs-libvterm/issues/215
  (cc/define-key-list vterm-mode-map
                      `(("C-x g" . nil)
                        ("M-:"   . nil)
                        ("C-q"   . ,#'vterm-send-next-key)
                        ("M->"   . ,#'vterm-reset-cursor-point)
                        ("C-x x r" . ,#'cc/vterm-rename-buffer)
                        ;; "C-x #" chosen for symmetry with `server-edit' (global binding).
                        ("C-x #" . ,#'cc/vterm-zsh-edit-external)))
  (setq vterm-max-scrollback (* 5 1000))
  ;; Keep buffer around for reference in case I e.g. accidentally hit ctrl-d one too many times.
  (setq vterm-kill-buffer-on-exit nil)
  (setq vterm-buffer-name-string (cc/vterm--buffer-name-template))
  ;; Because other machines may not have zsh.
  (add-to-list 'vterm-tramp-shells '("ssh" "bash")))

(defun cc/vterm-zsh-edit-external ()
  "Send keys to edit zsh command externally (in Emacs).

EMACSCLIENT USAGE TIP: Use \\='C-x #\\=' to save and exit buffer. For zsh side of
config, search for \\='edit-command-line\\='."
  (interactive)
  (vterm-send "C-x")
  (vterm-send "#"))

(defun cc/vterm--buffer-name-template (&optional extra)
  (concat "*vterm"
          (if (and extra (not (string-empty-p extra)))
              (concat " " extra)
            "")
          "* %s"))

(defun cc/vterm-rename-buffer (name)
  "Like `rename-buffer', but adapted for `vterm-buffer-name-string'."
  (interactive "sNew vterm name: ")
  (setq-local vterm-buffer-name-string (cc/vterm--buffer-name-template name))
  ;; Unfortunately vterm doesn't store the title used for %s with `vterm-buffer-name-string', so I'm
  ;; semi-naively parsing it from `buffer-name' instead.
  (rename-buffer (format (cc/vterm--buffer-name-template name)
                         (replace-regexp-in-string
                          (rx bos "*" (1+ (not "*")) "* ")
                          ""
                          (buffer-name)))))

(provide 'cc-vterm)
