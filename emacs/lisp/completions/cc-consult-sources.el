;;; -*- lexical-binding: t -*-
(require 'consult)
(require 'cc-utils)
(require 'cc-exwm-utils)
(require 'cc-org-utils)

(defconst cc/completions--consult-narrow-major-mode-key ?c)
(defconst cc/completions--consult-narrow-web-key ?w)

(defun cc/completions-consult-sources-init ()
  ;; Default buffer sources, redeclared here to make this function idempotent.
  (setq consult-buffer-sources '(consult--source-hidden-buffer
                                 consult--source-buffer
                                 consult--source-recent-file
                                 consult--source-bookmark
                                 consult--source-project-buffer
                                 consult--source-project-recent-file))
  (dolist (args `(("Major mode" ,cc/completions--consult-narrow-major-mode-key
                   (:predicate cc/completions--consult-mode-predicate))
                  ("EXWM" ?e
                   (:predicate (lambda (buffer)
                                 (with-current-buffer buffer
                                   (and (equal major-mode 'exwm-mode)
                                        ;; I have a separate source for Firefox buffers.
                                        (not (cc/firefox-buffer-p)))))))
                  ("Web" ,cc/completions--consult-narrow-web-key
                   (:predicate (lambda (buffer)
                                 (cc/firefox-buffer-p buffer)))
                   cc/completions--firefox-fallback)
                  ("Org" ?o
                   (:mode org-mode))))
    (apply #'cc/completions--consult-narrowed args)))


;;; Miscellaneous sources

(defun cc/completions--consult-mode-predicate (buffer)
  (let* ((buffer-mode (buffer-local-value 'major-mode buffer))
         ;; Some modes have a handful of child modes that should all be shown together.
         (ancestor (provided-mode-derived-p
                    major-mode
                    '(magit-mode ; For `magit-status-mode', `magit-diff-mode', etc
                      emacs-lisp-mode ; Include `lisp-interaction-mode' for *scratch*.
                      org-mode
                      comint-mode))))
    (provided-mode-derived-p buffer-mode (list major-mode ancestor))))

(defun cc/completions--consult-narrowed (name narrow-key buffer-query-args &optional fallback)
  "Create a consult source for buffers matching PREDICATE.

BUFFER-QUERY-ARGS plist is passed along to `consult--buffer-query'."
  (let ((entry `(:name     ,name
                 :narrow   (,narrow-key . ,name)
                 ;; Buffers are already shown by `consult--source-buffer', so don't show this narrowed source
                 ;; until explicitly narrowed.
                 :hidden   t
                 :category buffer
                 :face     consult-buffer
                 :history  buffer-name-history
                 :state    ,#'consult--buffer-state
                 :items
                 ,(lambda ()
                    (apply #'consult--buffer-query
                           :sort 'visibility
                           :as #'buffer-name
                           buffer-query-args)))))
    (when fallback
      (plist-put entry :new fallback))
    (push entry consult-buffer-sources)))

(defun cc/completions--firefox-fallback (&optional name)
  ;; Sometimes the simplest solution is the best... instead of trying to be clever by e.g. doing
  ;; firefox --search or attempting to automatically insert text into the address bar, just add it
  ;; to kill ring to make it easy to yank.
  (when name
    (kill-new name))
  (cc/with-home-dir (start-process-shell-command "firefox" nil "firefox --new-window")))

(defun cc/completions-consult-buffer-major-mode ()
  "Like `consult-buffer', but pre-narrowed to current major mode."
  (interactive)
  (cc/with-minibuffer-input (concat (list cc/completions--consult-narrow-major-mode-key ? ))
    (consult-buffer)))

(defun cc/completions-consult-buffer-web (&optional arg)
  "Like `consult-buffer', but pre-narrowed to web.

If no existing buffers or prefix ARG, then always open a new buffer."
  (interactive "P")
  (if (and (not arg)
           (seq-some #'cc/firefox-buffer-p (buffer-list)))
      (cc/with-minibuffer-input (concat (list cc/completions--consult-narrow-web-key ? ))
        (consult-buffer))
    (cc/completions--firefox-fallback)))

(defun cc/completions-consult-org-files ()
  "Navigate to a subset of my org files.

Excludes e.g. journal and roam files."
  (interactive)
  (let* ((files (sort (apply #'append
                             (seq-map #'cc/get-org-files '("gtd" "gtd/areas" ".")))
                      #'string-lessp))
         (source
          (list :name     "Org files"
                :category 'file
                :face     'consult-file
                :history  'file-name-history
                ;; Display files relatively for UX, then make absolute to open.
                :action   (lambda (f) (consult--file-action (expand-file-name f org-directory)))
                :items    (seq-map (lambda (f) (file-relative-name f org-directory))
                                   files))))
    (consult--multi (list source) :sort nil)))


;;; Project-related sources


(provide 'cc-consult-sources)
