;;; -*- lexical-binding: t -*-
(require 'consult)

(defun cc/completions--buffer-switcher-impl (arg predicate create-new)
  (if arg
      (funcall create-new)
    (let ((existing (seq-filter (lambda (buf)
                                  (with-current-buffer buf
                                    (funcall predicate)))
                                (buffer-list))))
      (pcase (length existing)
        (0 (funcall create-new))
        (1 (switch-to-buffer (car existing)))
        (_ (consult-buffer (list (list :category 'buffer
                                       :face 'consult-buffer
                                       :history 'buffer-name-history
                                       :state #'consult--buffer-state
                                       :items (mapcar #'buffer-name existing)
                                       :new create-new))))))))

(defmacro cc/completions-buffer-switcher (name predicate create-new)
  "Generates a buffer switcher function."
  (declare (debug (form def-form def-form)))
  `(defun ,(intern (format "cc/completions-buffer-switcher-%s" name)) (&optional arg)
     (:documentation ,(format "Buffer switcher for %s." name))
     (interactive "P")
     (cc/completions--buffer-switcher-impl
      arg
      (lambda ()
        ,predicate)
      (lambda (&rest _)
        ,create-new))))

(provide 'cc-completions-utils)
