;;; -*- lexical-binding: t -*-
;;; project.el and project-related consult config.
(require 'consult)
(require 'project)
(require 'cc-user-directories)

(defvar cc/consult-project--history nil)

(defvar cc/consult-project--original-dir nil
  "`default-directory' at the time `cc/consult-project-switch' was invoked.")

(defconst cc/consult-project--buffer-source
  (let ((result (copy-sequence consult--source-project-buffer)))
    (plist-put result :items (lambda ()
                               (let ((default-directory (or cc/consult-project--original-dir
                                                            default-directory)))
                                 (funcall (plist-get consult--source-project-buffer :items)))))
    result)
  "Modified variant of `consult--source-project-buffer'.

Prevents the current buffer being incorrectly treated as part of the selected
project, which is caused by `cc/consult-project-switch' manipulating
`default-directory'. Welcome to Emacs, where it's kludges all the way down.")

(defconst cc/consult-project--file-source
  `(:name      "Project File"
    :narrow    (?f . "File")
    :category  file
    :face      consult-file
    :history   file-name-history
    :action
    ,(lambda (candidate)
       (find-file (expand-file-name candidate (project-root (project-current)))))
    :enabled  ,(lambda () consult-project-function)
    :items
    ,(lambda ()
       (let* ((project (project-current))
              (root (project-root project))
              (files (project-files project)))
         (seq-map (lambda (f)
                    (file-relative-name f root))
                  files)))))

(defconst cc/consult-project--project-source
  `(:name      "Known Project"
    :category  cc/consult-project
    :face      consult-file
    :history   cc/consult-project--history
    :annotate  ,(lambda (dir)
                  (format "[%s]"
                          (file-name-nondirectory (directory-file-name dir))))
    :action    ,(lambda (dir)
                  (let ((cc/consult-project--original-dir default-directory)
                        (default-directory dir))
                    (cc/consult-project-buffer)))
    :items     ,#'project-known-project-roots))

(defun cc/consult-project-init ()
  (setq project-list-file (expand-file-name "projects" cc/user-cache))
  (setq project-kill-buffers-display-buffer-list t)
  ;; I don't want Tramp (SSH) projects to persist in list.
  (dolist (proj (project-known-project-roots))
    (when (string-prefix-p "/ssh:" proj)
      (project-forget-project proj)))
  ;; Clean up other dead projects.
  (project-forget-zombie-projects)
  (dolist (root-dir cc/user-repo-search-dirs)
    (when (file-exists-p root-dir)
      ;; `project-remember-projects-under' either looks at the exact directory (not under) or all
      ;; children recursively, when I want to look exactly one level deep... so recurse myself.
      (dolist (dir (seq-filter (lambda (f)
                                 (and (file-readable-p f) ;; In case e.g. only root can read
                                      (file-directory-p f)))
                               (directory-files root-dir t directory-files-no-dot-files-regexp)))
        ;; `message-log-max': prevent spam from writing EVERY project to *Messages*.
        (let ((message-log-max nil))
          (project-remember-projects-under dir)))))
  (define-key global-map (kbd "C-; C-s") #'cc/consult-project-buffer)
  (define-key global-map (kbd "C-; p") #'cc/consult-project-switch))

(defun cc/consult-project-buffer ()
  "`consult-buffer' for the current project."
  (interactive)
  (if (project-current)
      (let ((consult-project-buffer-sources (list cc/consult-project--buffer-source
                                                  cc/consult-project--file-source)))
        (consult-project-buffer))
    (cc/consult-project-switch)))

(defun cc/consult-project-switch ()
  "Select a project then do `consult-project-buffer' on it."
  (interactive)
  ;; Fortunately `project-forget-zombie-projects' is fast since it just checks for existence of each
  ;; directory... `benchmark-run' shows 100 repetitions took 0.2 seconds (total) with 130 projects.
  (project-forget-zombie-projects)
  (consult-buffer (list cc/consult-project--project-source)))

(provide 'cc-consult-project)
