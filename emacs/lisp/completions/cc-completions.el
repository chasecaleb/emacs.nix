;;; -*- lexical-binding: t -*-
;;; Completion configuration. I used to use Helm, which was nice for its batteries-included
;;; functionality, but over time I've become less satisfied with its inconsistencies and how
;;; difficult it is to extend.

(require 'cl-lib)
(require 'frame)
(require 'mode-local)
(require 'vertico)
(require 'vertico-repeat)
(require 'orderless)
(require 'marginalia)
(require 'embark)
(require 'embark-consult)
(require 'consult)
(require 'consult-yasnippet)
(require 'company)
(require 'company-capf)
(require 'project)
(require 'vterm)
(require 'exwm-input)
(require 'cc-utils)
(require 'cc-user-directories)
(require 'cc-exwm-utils)
(require 'cc-consult-sources)
(require 'cc-source-code)

(defun cc/completions-init ()
  (cc/completions--company-init)
  (cc/completions--vertico-init)
  (cc/completions--orderless-init)
  (cc/completions--marginalia-init)
  (cc/completions--embark-init)
  (cc/completions--consult-init)
  (cc/completions--yasnippet-init)
  (cc/completions--keybinds-init))

(defun cc/completions--keybinds-init ()
  (cc/define-key-list
   global-map
   `(;; Embark
     ;; Note: *do not* bind "M-." to `embark-dwim' as the readme suggests, just use default
     ;; `embark-act' action via "C-. <return>". `embark-dwim' handles xref poorly, because it
     ;; let-binds the xref backends. See https://github.com/oantolin/embark/issues/349
     ("C-."                      . ,#'embark-act)
     ("C-M-."                    . ,#'embark-become)
     ([remap describe-bindings]  . ,#'embark-bindings)
     ;; File/buffer switching
     ("C-; r"                    . ,#'vertico-repeat-last)
     ("C-; C-r"                  . ,#'vertico-repeat-select)
     ("C-; f"                    . ,#'find-file)
     ("C-; C-f"                  . ,#'consult-find)
     ("C-; s"                    . ,#'consult-buffer)
     ("C-; w"                    . ,#'cc/completions-consult-buffer-web)
     ("C-; C-w"                  . ,#'cc/firefox-ibuffer)
     ("C-; c"                    . ,#'cc/completions-consult-buffer-major-mode)
     ("C-; n"                    . ,#'cc/completions-consult-org-files)
     ;; Searching and navigation
     ("C-; g"                    . ,#'consult-ripgrep)
     ("C-; i"                    . ,#'consult-imenu)
     ("C-; C-i"                  . ,#'consult-imenu-multi)
     ;; There's also `consult-org-heading', but even in org files I like `consult-outline' better
     ;; because it shows the full heading (i.e. including todo keywords and tags).
     ("C-; o"                    . ,#'consult-outline)
     ("M-s o"                    . ,#'consult-line)
     ("M-s M-o"                  . ,#'consult-line-multi)
     ("C-; SPC"                  . ,#'consult-mark)
     ("C-; C-SPC"                . ,#'consult-global-mark)
     ("C-; e"                    . ,#'consult-flymake)
     ("C-; C-e"                    . (lambda ()
                                       "`consult-flymake' for entire project"
                                       (interactive)
                                       (consult-flymake t)))
     ;; Registers and bookmarks
     ("C-; x x"                  . ,#'consult-register)
     ("C-; x l"                  . ,#'consult-register-load)
     ("C-; x s"                  . ,#'consult-register-store)
     ("C-; m"                    . ,#'consult-bookmark)
     ;; Other
     ("M-y"                      . ,#'consult-yank-pop)
     ("C-; k"                    . ,#'consult-kmacro)
     ("C-; h"                    . ,#'man)
     ("C-; M-h"                  . ,#'consult-man)
     ("C-; M-x"                  . ,#'consult-mode-command)
     ("C-; y"                    . ,#'consult-yasnippet)
     ;; Miscellaneous built-in command overrides
     ("C-x M-:"                  . ,#'consult-complex-command)
     ("C-x r b"                  . ,#'consult-bookmark))))


;;; Company

(defun cc/completions--company-init ()
  (advice-add #'company-capf :around #'cc/completions--company-style-advice)
  (global-company-mode)
  ;; Intentionally *not* function-quoting `tags-completion-at-point-function' to avoid autoloading,
  ;; because fuck etags. Anyways, removing this from capf for two reasons:
  ;; 1. I don't use tags and so I don't want to waste performance on this function.
  ;; 2. It has an infuriating tendency to go rogue and start offering completions in places that it
  ;;    shouldn't, such as lisp comment strings, after a while. Honestly no idea what's triggering
  ;;    it, but my wild ass guess would be when I visit a file or project that has a tags file.
  ;;    Maybe core Emacs files?
  (remove-hook 'completion-at-point-functions 'tags-completion-at-point-function)
  (diminish 'company-mode)
  (setq company-tooltip-limit 20)
  (setq company-tooltip-minimum-width 40)
  (setq company-tooltip-width-grow-only t)
  (setq company-tooltip-align-annotations t)
  (setq company-tooltip-offset-display 'lines)
  (setq company-format-margin-function #'company-text-icons-margin)
  (setq company-text-face-extra-attributes '(:slant italic))
  (setq company-idle-delay 0.2)
  (setq company-selection-wrap-around t)
  (setq company-backends (list #'company-capf #'company-files))
  ;; Use `company-complete' instead of built-in `completion-at-point', except in minibuffer where
  ;; company doesn't work.
  (global-set-key (kbd "M-<tab>") #'company-complete)
  (define-key minibuffer-local-map (kbd "M-<tab>") #'completion-at-point)

  ;; Need to change these because company changed default keybinds from M-n/M-p to C-n/C-p. Making
  ;; C-n/C-p do two things depending on context (navigate buffer or navigate completions) is
  ;; a cognitive burden and error-prone since company completions pop up automatically.
  ;;
  ;; HOWEVER, some major modes (e.g. magit's commit message mode and ielm) have M-n and M-p bindings
  ;; that I don't want to conflict with. So the compromise is M-N and M-P (capital N and P).
  (dolist (map (list company-mode-map company-active-map company-search-map))
    (cc/define-key-list map `(("C-n" . nil)
                              ("C-p" . nil)
                              ("M-n" . nil)
                              ("M-p" . nil)
                              ("M-N" . ,#'cc/company-activate-or-next)
                              ("M-P" . ,#'cc/company-activate-or-previous)))))

(defun cc/completions--company-style-advice (fn &rest args)
  "Override `completion-styles' for Company."
  (let ((completion-styles '(basic partial-completion orderless))
        (orderless-component-separator "&"))
    (apply fn args)))

(defun cc/company--activate-or-call (fn)
  (if (company--active-p)
      (funcall-interactively fn)
    (company-complete)))

(defun cc/company-activate-or-next ()
  (interactive)
  (cc/company--activate-or-call #'company-select-next))

(defun cc/company-activate-or-previous ()
  (interactive)
  (cc/company--activate-or-call #'company-select-previous))


;;; Vertico

(defun cc/completions--vertico-init ()
  (vertico-mode)
  (setq vertico-count 25)
  (setq vertico-cycle t)
  ;; vertico-repeat extension
  (add-hook 'minibuffer-setup-hook #'vertico-repeat-save)
  ;; vertico-directory extension
  (cc/define-key-list vertico-map
                      `(("RET" . ,#'vertico-directory-enter)
                        ("M-<backspace>" . ,#'vertico-directory-delete-word)))
  (add-hook 'rfn-eshadow-update-overlay-hook #'vertico-directory-tidy)
  ;; vertico-quick extension (avy-like selection)
  (cc/define-key-list vertico-map
                      `(("M-g g" . ,#'vertico-quick-insert)
                        ("M-g M-g" . ,#'vertico-quick-insert))))


;;; Orderless

(defun cc/completions--orderless-init ()
  (setq completion-styles '(orderless))
  (setq orderless-component-separator #'orderless-escapable-split-on-space)
  (setq completion-category-defaults nil)
  (setq completion-category-overrides
        ;; partial-completion on files is amazing, because "/r/c/sw/sh/e/s" or even "/r/c/s/s/e/s"
        ;; can complete "/run/current-system/sw/share/emacs/site-lisp/".
        '((file (styles . (partial-completion orderless)))
          (project-file (styles . (partial-completion orderless)))))

  (setq orderless-matching-styles '(orderless-prefixes
                                    orderless-regexp
                                    orderless-initialism))
  (setq orderless-style-dispatchers '(cc/completions--orderless-dispatcher-literal
                                      cc/completions--orderless-dispatcher-not
                                      cc/completions--orderless-dispatcher-initialism
                                      cc/completions--orderless-dispatcher-flex)))

(defun cc/completions--orderless-dispatcher-literal (pattern _index _total)
  (when (string-suffix-p "=" pattern)
    `(orderless-literal . ,(substring pattern 0 -1))))

(defun cc/completions--orderless-dispatcher-not (pattern _index _total)
  (cond
   ((equal "!" pattern)
    '(orderless-literal . ""))
   ((string-prefix-p "!" pattern)
    `(orderless-without-literal . ,(substring pattern 1)))))

(defun cc/completions--orderless-dispatcher-initialism (pattern _index _total)
  (when (string-suffix-p "," pattern)
    `(orderless-initialism . ,(substring pattern 0 -1))))

(defun cc/completions--orderless-dispatcher-flex (pattern _index _total)
  (when (string-suffix-p "~" pattern)
    `(orderless-flex . ,(substring pattern 0 -1))))


;;; Marginalia

(defun cc/completions--marginalia-init ()
  (marginalia-mode)
  (define-key vertico-map (kbd "M-A") #'marginalia-cycle)
  ;; `marginalia-field-width' is an upper limit, which will be lowered on the fly based on window
  ;; width. So basically set something high here and then let marginalia adjust it to fit.
  (setq marginalia-field-width 500)
  (add-to-list 'marginalia-annotator-registry
               '(buffer cc/completions--marginalia-firefox builtin none)))

(defun cc/completions--marginalia-firefox (candidate)
  (when-let (buffer (get-buffer candidate))
    (if-let (url (and (fboundp 'cc/firefox-url)
                      (cc/firefox-url buffer)))
        ;; `marginalia--fields' usage based on `marginalia-annotate-buffer'.
        (marginalia--fields
         ((marginalia--buffer-status buffer))
         (url :truncate -0.5 :face 'marginalia-file-name))
      (marginalia-annotate-buffer candidate))))


;;; Embark

(defun cc/completions--embark-init ()
  (defvar-keymap embark-project-map
    :parent embark-file-map
    :doc "`cc/completions-project-switch' actions.")
  (add-to-list 'embark-keymap-alist '(cc/consult-project . embark-project-map))
  (advice-add #'embark--cd :around #'cc/completions--embark-cd-advice)
  (cl-labels ((add-map (key fn)
                       (dolist (map (list embark-file-map
                                          embark-buffer-map))
                         (define-key map key fn)
                         (cl-pushnew #'embark--cd
                                     (alist-get fn embark-around-action-hooks)))))
    (add-map "g" #'magit-status)
    (add-map "v" #'cc/completions--embark-vterm))
  (define-key embark-file-map "l" #'cc/embark-org-link-file)
  (define-key embark-buffer-map "l" #'cc/embark-org-link-buffer)
  (dolist (map (list embark-symbol-map embark-command-map))
    (define-key map "l" #'cc/embark-org-link-symbol))
  (define-key embark-general-map ".f" #'cc/embark-firefox)
  (define-key embark-general-map ".s" #'cc/embark-firefox-search)
  ;; `embark-consult-search-map' is prefixed to C for other embark maps, so git grep easy.
  (define-key embark-consult-search-map "C" #'consult-ripgrep)
  (setq prefix-help-command #'embark-prefix-help-command)
  (setq embark-quit-after-action '((kill-buffer . nil)
                                   (describe-symbol . nil)
                                   (t . t)))
  (advice-add #'embark--restart :before #'cc/embark--restore-narrowing)
  ;; Remove some overly-cautious nuisance confirmation prompts.
  ;; This is obviously not the most efficient solution to remove elements from a list, but it works
  ;; fine for a couple elements.
  (dolist (elt '((kill-buffer embark--confirm)
                 (embark-kill-buffer-and-window embark--confirm)))
    (setq embark-pre-action-hooks (delete elt embark-pre-action-hooks))))

(defun cc/completions--embark-vterm ()
  "vterm"
  (interactive)
  (vterm t))

(defun cc/embark-org-link-file (file)
  "Store link to file with `org-store-link'"
  (cc/embark-org-link-buffer (find-file-noselect file)))

(defun cc/embark-org-link-buffer (buffer)
  "Store link for buffer with `org-store-link'."
  (with-current-buffer buffer
    (save-excursion
      (goto-char 0) ;; Make org file links refer to the file, not wherever point happens to be.
      (org-store-link '(4) t))))

(defun cc/embark-org-link-symbol (symbol)
  "`describe-symbol' and store as an org link."
  (interactive "SSymbol: ")
  (describe-symbol symbol)
  (cc/embark-org-link-buffer (help-buffer)))

(defun cc/embark-firefox (_input)
  "Create new Firefox buffer."
  (cc/with-home-dir
   (start-process-shell-command "firefox" nil "firefox --new-window")))

(defun cc/embark-firefox-search (input)
  "New web search."
  (cc/with-home-dir
   (let ((cmd (format "firefox --search %s" (shell-quote-argument input))))
     (start-process-shell-command "firefox" nil cmd))))

(defun cc/embark--restore-narrowing (&rest _ignored)
  "Kludge to restore consult narrowing after a non-quitting embark action.

See https://github.com/oantolin/embark/issues/509"
  ;; `minibuffer-setup-hook' doesn't work, so using `add-hook'. I think minibuffer isn't triggered
  ;; until after (outside of) the function being advised here? It looks like it should be, but
  ;; Embark is doing some clever things with repeating commands so I'm probably confused.
  (when-let ((narrow consult--narrow))
    (cl-labels ((my-fn ()
                       (unwind-protect
                           ;; 32 is ASCII space.
                           ;; (cc/simulate-input (list narrow 32))
                           (setq unread-command-events
                                 (append unread-command-events (list narrow 32)))
                         (remove-hook 'minibuffer-setup-hook #'my-fn))))
      (add-hook 'minibuffer-setup-hook #'my-fn))))

(defun cc/completions--embark-cd-advice (fn &rest args)
  "Fix `embark--cd' to handle actions on projects."
  ;; Treat project list as a file (since the target value is an absolute directory).
  (when (eq (plist-get args :type) 'cc/consult-project)
    (plist-put args :type 'file))
  (apply fn args))


;;; Consult

(defun cc/completions--consult-init ()
  (cc/completions-consult-sources-init)
  (setq register-preview-delay 0.01)
  (setq register-preview-function #'consult-register-format)
  (setq consult-narrow-key "<")
  (define-key consult-narrow-map (vconcat consult-narrow-key "?") #'consult-narrow-help)
  ;; ripgrep args changes from default:
  ;; - Include hidden files such as .gitlab-ci.yml.
  ;; - Follow symlinks, because Nix.
  (setq consult-ripgrep-args
        (concat "rg --hidden --glob !.git/** --follow --search-zip "
                "--null --line-buffered --color=never --max-columns=1000 "
                "--path-separator /   --smart-case --no-heading --line-number"))
  (advice-add #'consult--with-preview-1 :around #'cc/completions--consult-with-preview-fix)
  (advice-add #'consult-yank-pop :around #'cc/completions--yank-pop-advice)
  (advice-add #'consult-org-heading :before #'cc/completions--consult-set-point-advice)
  (advice-add #'consult-outline :before #'cc/completions--consult-set-point-advice)
  (advice-add #'vertico--update :after #'cc/completions--consult-outline-vertico-advice)
  ;; Adjust preview behavior for "expensive" (i.e. file-opening) commands.
  (consult-customize
   consult-ripgrep
   consult-git-grep
   consult-grep
   consult-xref
   :preview-key '("M-."
                  :debounce 0.2 any))
  (consult-customize
   consult-bookmark
   consult--source-bookmark
   consult--source-recent-file
   consult--source-project-recent-file
   consult-recent-file
   :preview-key "M-."))

(defun cc/completions--consult-with-preview-fix (fn preview-key state &rest rest)
  "Kludge to fix EXWM focus stealing during consult previews."
  (let ((wrapper (when state
                   (lambda (&rest args)
                     (prog1 (apply state args)
                       ;; EXWM buffers steal focus *after* preview, so we need to fix it if we were
                       ;; just previewing an EXWM buffer are now previewing a non-EXWM buffer.
                       ;; Switching between multiple EXWM buffers does not trigger this bug.
                       ;; However, we don't have to test for that exact scenario since it's okay to
                       ;; call `exwm-input--on-buffer-list-update' more often than necessary.
                       ;; There's clearly an EXWM bug going on here but I have no idea what it is.
                       (when (eq (car args) 'preview)
                         ;; I don't know why this needs to be run on a timer instead of immediately,
                         ;; but it doesn't work if called immediately.
                         ;;
                         ;; I advised `run-at-time' (which is also called by `run-with-timer') and
                         ;; didn't see any consult functions, although EXWM does add
                         ;; `exwm-input--update-focus-commit' when previewing a buffer using
                         ;; consult, which seems incredibly suspicious. It could also have nothing
                         ;; to do with timers and just be that this needs to run after some other
                         ;; consult or exwm logic (since elisp runs single threaded so the currently
                         ;; executing code has to finish before timer can run).
                         (run-at-time nil nil #'exwm-input--on-buffer-list-update)))))))
    (apply fn preview-key wrapper rest)))

(defun cc/completions--yank-pop-advice (fn &rest args)
  "Wrapper around `consult-yank-pop' to handle vterm and EXWM quirks."
  ;; Lesser-known advice use: adding an interactive spec will override the spec of the advised
  ;; function. In this case, need to modify it from "*p" in order to allow use in readonly buffers
  ;; like EXWM and vterm.
  (interactive "p")
  (pcase major-mode
    ('vterm-mode
     (let ((inhibit-read-only t)
           (yank-undo-function (lambda (_start _end) (vterm-undo))))
       (cl-letf (((symbol-function 'insert-for-yank)
                  (lambda (str) (vterm-send-string str t))))
         (apply fn args))))
    ('exwm-mode
     ;; I don't want to (require 'exwm) since I don't use it in child Emacs processes, so do this
     ;; instead to make compiler happy.
     (when (and (fboundp 'exwm-input--set-focus)
                (fboundp 'exwm--buffer->id)
                (fboundp 'exwm-input--fake-key))
       (let ((inhibit-read-only t))
         ;; Disgusting hack to send selection to X11 clipboard, since consult doesn't seem to do
         ;; this automatically (whereas Helm did, surprisingly).
         (cc/with-advice #'insert-for-yank :before
                         (lambda (&rest insert-args)
                           (if-let ((contents (car insert-args)))
                               (gui-select-text contents)))
           ;; Cycling through kill ring in EXWM buffers isn't a thing, so always show selection menu
           ;; instead. Alternatively I could bind `yank-undo-function' to send a simulated "C-z"
           ;; undo, but I don't want to deal with potential edge cases or add complexity.
           ;; Also, for whatever reason this doesn't seem to behave with `funcall-interactively'...
           ;; so I copied the interactive spec into args here.
           (consult-yank-from-kill-ring (consult--read-from-kill-ring) current-prefix-arg))
         ;; https://github.com/ch11ng/exwm/issues/413#issuecomment-386858496
         (exwm-input--set-focus (exwm--buffer->id (window-buffer (selected-window))))
         (exwm-input--fake-key ?\C-v))))
    (_ (apply fn args))))

(defvar cc/completions--consult-previous-point nil
  "Location of point before entering minibuffer.")

(defun cc/completions--consult-set-point-advice (&rest _args)
  "Pre-select entry nearest to point.

https://github.com/minad/consult/wiki#pre-select-nearest-heading-for-consult-org-heading-and-consult-outline-using-vertico"
  (setq cc/completions--consult-previous-point (point)))

;; https://github.com/minad/consult/wiki#pre-select-nearest-heading-for-consult-org-heading-and-consult-outline-using-vertico
(defun cc/completions--consult-outline-vertico-advice (&rest _)
  "Select nearest heading for `consult-org-heading' and `consult-outline'."
  (when (and cc/completions--consult-previous-point
             (memq current-minibuffer-command
                   '(consult-org-heading consult-outline)))
    (setq vertico--index
          (max 0 ; if none above, choose the first below
               (1- (or (seq-position
                        vertico--candidates
                        cc/completions--consult-previous-point
                        (lambda (cand point-pos) ; counts on candidate list being sorted
                          (> (cl-case current-minibuffer-command
                               (consult-outline
                                (car (consult--get-location cand)))
                               (consult-org-heading
                                (get-text-property 0 'consult--candidate cand)))
                             point-pos)))
                       (length vertico--candidates))))))
  (setq cc/completions--consult-previous-point nil))


;;; Yasnippet

(defun cc/completions--yasnippet-init ()
  (diminish 'yas-minor-mode)
  ;; Use config in repo instead of system location because I want to modify these on the fly easily.
  (setq yas-snippet-dirs `(,(expand-file-name "snippets" cc/user-config-repo)))
  (setq yas-indent-line 'fixed)
  (yas-global-mode t))

(provide 'cc-completions)
