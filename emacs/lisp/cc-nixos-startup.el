;;; -*- lexical-binding: t -*-
(require 'seq)
(require 'info)

(defun cc/nixos-startup--autoloads ()
  (dolist (dir load-path)
    (when (file-exists-p dir)
      (dolist (autoloads (directory-files-recursively dir (rx "-autoloads.el" eos) nil nil t))
        (load autoloads nil t)))))

(defun cc/nixos-startup--info ()
  (info-initialize) ; Populate `Info-directory-list' from $INFOPATH.
  (dolist (dir load-path)
    (when (file-exists-p dir)
      (dolist (info-marker (directory-files-recursively dir (rx bos "dir" eos) nil nil t))
        (push (file-name-directory info-marker) Info-directory-list)))))

(defun cc/nixos-startup-init ()
  (cc/nixos-startup--autoloads)
  (cc/nixos-startup--info)
  ;; Nix > package.el
  (setq package-enable-at-startup nil))

(provide 'cc-nixos-startup)
