;;; -*- lexical-binding: t -*-
;;; See description of `cc/file-auto-save-mode'.

(defvar cc/file-auto-save--inhibit nil
  "Use with let binding to temporarily inhibit auto saving.")

(defvar cc/file-auto-save--prev-buffer nil
  "Tracks currently active (focused) buffer")

(defvar cc/file-auto-save--idle-timer nil
  "Idle timer.")

(defvar cc/file-auto-save--focus-timer nil
  "Timer to debounce frame focus changes, which may fire repeatedly.")

(defcustom cc/file-auto-save-interval 60
  "Idle interval in seconds to save all `cc/file-auto-save-buffer-mode' buffers.

You likely do not need to modify this, since changes will generally be saved
sooner by switching windows/buffers. This idle timer exists as a failsafe (in
case of some sort of bug preventing focus change detection) and to catch any
changes that are made immediately before going idle, such as editing the current
buffer and then walking away.

IMPORTANT: Either use custom to set this, or else set this *before* enabling
`cc/file-auto-save-mode'."
  :group 'cc/file-auto-save
  :type 'number
  :set (lambda (symbol value)
         (set-default symbol value)
         (when cc/file-auto-save--idle-timer
           (timer-set-idle-time cc/file-auto-save--idle-timer value :repeat))))

(define-minor-mode cc/file-auto-save-mode
  "Automatically saves buffers on window/buffer/frame change and on idle.

To use:
- Enable this mode globally (once)
- Enable `cc/file-auto-save-buffer' for each buffer that should be auto-saved.

This is similar to `auto-save-visited-mode', but requires opting-in for each
buffer and saves more often than just when idle."
  :group 'cc/file-auto-save
  :global t
  (cc/file-auto-save--setup-hooks)
  (cc/file-auto-save--setup-timer))

(define-minor-mode cc/file-auto-save-buffer-mode
  "Required for each buffer used by `cc/file-auto-save-mode'.

Also enables/disables `auto-revert-mode'."
  :group 'cc/file-auto-save
  (when (and cc/file-auto-save-buffer-mode
             (not cc/file-auto-save-mode))
    (message "WARNING: `cc/file-auto-save-buffer-mode' enabled without `cc/file-auto-save-mode'."))
  ;; This isn't currently necessary since I also have `global-auto-revert-mode' enabled, but it will
  ;; be necessary if for whatever reason I ever decide to disable it.
  (auto-revert-mode cc/file-auto-save-buffer-mode))

(defun cc/file-auto-save--setup-hooks ()
  (dolist (hook '(window-buffer-change-functions
                  window-selection-change-functions))
    (if cc/file-auto-save-mode
        (add-hook hook #'cc/file-auto-save--on-window-change)
      (remove-hook hook #'cc/file-auto-save--on-window-change)))
  ;; Using `add-function' because this is advising a variable, not a function. Um... okay.
  (if cc/file-auto-save-mode
      (add-function :after after-focus-change-function #'cc/file-auto-save--on-focus-change)
    (remove-function after-focus-change-function #'cc/file-auto-save--on-focus-change)))

(defun cc/file-auto-save--setup-timer ()
  (when cc/file-auto-save--idle-timer
    (cancel-timer cc/file-auto-save--idle-timer))
  (setq cc/file-auto-save--idle-timer
        (when cc/file-auto-save-mode
          (run-with-idle-timer cc/file-auto-save-interval t #'cc/file-auto-save-do-save))))

(defun cc/file-auto-save--on-window-change (it)
  "IT may be a window, frame, or buffer."
  (let ((new-buffer (cc/file-auto-save--get-buffer it)))
    (unless (or (minibufferp new-buffer)
                (eq new-buffer cc/file-auto-save--prev-buffer))
      (setq cc/file-auto-save--prev-buffer new-buffer)
      ;; Ignore error if a prompt about saving an externally-modified file is canceled, because
      ;; sometimes `auto-revert-mode' ends up in a weird broken state.
      (ignore-errors
        (cc/file-auto-save-do-save)))))

(defun cc/file-auto-save--on-focus-change ()
  ;; References (both by same author):
  ;; https://emacs.stackexchange.com/q/62783
  ;; https://github.com/emacsmirror/auto-dim-other-buffers/blob/62c936d502f35d168b9e59a66c994d74a62ad2cf/auto-dim-other-buffers.el
  (unless (timerp cc/file-auto-save--focus-timer)
    (setq cc/file-auto-save--focus-timer
          ;; auto-dim-other-buffers uses a shorter timer (0.015), but a longer debounce makes sense
          ;; here since this is silently saving files as opposed to doing UI things.
          (run-with-timer 0.05 nil #'cc/file-auto-save-do-save))))

(defun cc/file-auto-save-do-save ()
  (when (and cc/file-auto-save--focus-timer
             (timerp cc/file-auto-save--focus-timer))
    (cancel-timer cc/file-auto-save--focus-timer)
    (setq cc/file-auto-save--focus-timer nil))
  (unless cc/file-auto-save--inhibit
    ;; Suppress "Wrote %s" and "Saving file %s" message spam, which are especially annoying when they
    ;; obscure a prompt.
    (let ((message-log-max nil))
      (save-some-buffers t (lambda ()
                             (and cc/file-auto-save-buffer-mode
                                  (buffer-file-name)
                                  ;; When creating a new roam file, `buffer-file-name' is set but
                                  ;; the file doesn't actually exist yet. WTF?
                                  (file-exists-p (buffer-file-name))
                                  ;; Don't try to save if modified on disk and thus in need of revert.
                                  ;; Point of this is to avoid getting spammed by save prompts on
                                  ;; every buffer switch, which can make it hard to e.g. cancel an org
                                  ;; capture and deal with the issue. Yeah, this is stupid... but
                                  ;; `auto-revert-mode' seems to occasionally fail/miss an inotify
                                  ;; event/something like that.
                                  (verify-visited-file-modtime)))))))

(defmacro cc/with-inhibited-file-auto-save (&rest body)
  (declare (debug body))
  ;; Track initial value in order to behave correctly if I inexplicably nest this macro.
  `(let ((initial-value cc/file-auto-save--inhibit))
     (unwind-protect
         (progn
           (setq cc/file-auto-save--inhibit t)
           ,@body)
       (setq cc/file-auto-save--inhibit initial-value))))

(defun cc/file-auto-save--get-buffer (it)
  "Get current buffer for IT, which may be a buffer, window, or frame."
  (cond
   ((bufferp it) it)
   ((windowp it) (cc/file-auto-save--get-buffer (window-buffer it)))
   ((framep it) (cc/file-auto-save--get-buffer (frame-selected-window it)))))

(provide 'cc-file-auto-save)
