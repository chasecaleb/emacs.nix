;;; -*- lexical-binding: t -*-
(require 'xref)
(require 'consult)
(require 'dash)
(require 'dumb-jump)
(require 'eglot)
(require 'embark)
(require 'flymake)
(require 'project)
(require 'cc-utils)
(require 'cc-user-directories)
(require 'cc-utils)

(defvar cc/source-code-hook nil
  "Called for all source code-like major modes.

This is a superset of `prog-mode-hook', since it is intended to more broadly
include e.g. config file modes such as `conf-mode' and `yaml-mode'.")

(defun cc/source-code-init ()
  (dolist (hook '(prog-mode-hook
                  conf-mode-hook
                  toml-ts-mode-hook
                  yaml-mode-hook
                  terraform-mode-hook))
    (add-hook hook #'cc/source-code-run-hooks))

  (cc/source-code--flymake-init)
  (cc/source-code--xref-init)
  (cc/source-code--prettier-init)
  (cc/source-code--eglot-init)
  ;; Technically `editorconfig-mode' is a global minor mode that isn't source code exclusive, but
  ;; it's more or less only relevant for source code so whatever.
  (editorconfig-mode)
  (diminish 'editorconfig-mode))

(defun cc/source-code-run-hooks ()
  "Runs `cc/source-code-hook' hooks.

Add this function to the (major) mode's hook to treat it as source code."
  (run-hooks 'cc/source-code-hook))


;;; Flymake
(defun cc/source-code--flymake-init ()
  (add-hook 'cc/source-code-hook #'flymake-mode)
  ;; `elisp-flymake-byte-compile' only runs for `trusted-content' as of Emacs 30. Also, note that
  ;; `trusted-content' needs a trailing slash for directories.
  (add-to-list 'trusted-content
               (concat (string-remove-suffix "/" (concat cc/user-source-repo "/")) "/"))
  (define-key flymake-mode-map (kbd "M-n") #'flymake-goto-next-error)
  (define-key flymake-mode-map (kbd "M-p") #'flymake-goto-prev-error))


;;; xref and dumb jump

(defun cc/source-code--xref-init ()
  (setq xref-show-xrefs-function #'consult-xref)
  (setq xref-show-definitions-function #'consult-xref)
  (setq xref-prompt-for-identifier '(not xref-find-definitions
                                         xref-find-definitions-other-window
                                         xref-find-definitions-other-frame
                                         xref-find-references))
  ;; I never use tags/etags style backends, and the tags file prompt is a nuisance.
  (remove-hook 'xref-backend-functions #'etags--xref-backend)

  (add-hook 'xref-backend-functions #'dumb-jump-xref-activate)
  (setq dumb-jump-default-project "~/code")
  ;; See also `consult-ripgrep-args'.
  ;; "--search-zip --type-add elisp:*.el.gz" are necessary to search *.el.gz files.
  (setq dumb-jump-rg-search-args (concat "--pcre2 --search-zip"
                                         " --type-add " (shell-quote-argument "elisp:*.el.gz")
                                         " --hidden --glob " (shell-quote-argument "!.git/**")
                                         " --follow"))

  ;; Add a xref variant dedicated to dumb-jump, particularly useful when working on my own lisp code
  ;; (i.e. this project).
  (cc/define-key-list global-map
                      `(("C-; ."    . ,#'cc/dumb-jump-xref-find-definitions)
                        ("C-; ?"    . ,#'cc/dumb-jump-xref-find-references)
                        ("C-; 4 ."  . ,#'cc/dumb-jump-xref-find-definitions-other-window)
                        ("C-; 5 ."  . ,#'cc/dumb-jump-xref-find-definitions-other-frame)))

  ;; There's a `xref-find-definitions-other-window' but no equivalent for `xref-find-references', so
  ;; advice to the rescue. Plus using an advice works for my dumb-jump specific xref functions.
  (dolist (fn (list #'xref-find-definitions
                    #'xref-find-references))
    (advice-add fn :around #'cc/switch-to-buffer-with-display-actions-advice)))

(defmacro cc/source-code--dumb-jump (command)
  `(defun ,(intern (format "cc/dumb-jump-%s" (symbol-name (eval command)))) (&rest args)
     (:documentation ,(format "`%s' using dumb-jump backend." (eval command)))
     ;; - Need to set `this-command' for interactive spec to work correctly, because xref does some
     ;;   cleverness with `xref-prompt-for-identifier'. See `xref--prompt-p'. This was painful to
     ;;   figure out.
     ;; - Also need to modify `xref-backend-functions' for both the body *and* the interactive spec.
     (interactive (let ((this-command ,command)
                        (xref-backend-functions (list #'dumb-jump-xref-activate)))
                    (advice-eval-interactive-spec
                     (cadr (interactive-form
                            #'xref-find-definitions)))))
     ;; Save all project buffers so that results from external search command are accurate.
     (when-let ((project (project-current)))
       (cc/source-code--save-project-buffers project))
     (let ((this-command ,command)
           (xref-backend-functions (list #'dumb-jump-xref-activate)))
       (apply ,command args))))

(defun cc/source-code--save-project-buffers (project)
  (dolist (buf (project-buffers project))
    (with-current-buffer buf
      (when (and (buffer-file-name)
                 (buffer-modified-p))
        (save-buffer)))))

(cc/source-code--dumb-jump #'xref-find-definitions)
(cc/source-code--dumb-jump #'xref-find-references)
(cc/source-code--dumb-jump #'xref-find-definitions-other-window)
(cc/source-code--dumb-jump #'xref-find-definitions-other-frame)


;;; Prettier: formatter for a variety of languages
(reformatter-define prettier
  :program "prettier"
  :args (cc/source-code--prettier-args)
  :lighter " Prettier"
  :group 'cc-source-code)

(defvar-local cc/source-code--prettier-parser nil
  "Parser to use with prettier as fallback filename-less buffers.

See also: https://prettier.io/docs/en/options.html#parser")

(defun cc/source-code--prettier-init ()
  ;; Reminder: also update the cond in `cc/source-code--prettier-setup' if adding a new mode.
  (pcase-dolist (`(,mode ,feature) '((web-mode web-mode)
                                     (css-mode css-mode)
                                     (typescript-mode typescript-mode)
                                     (js-mode js)
                                     (json-ts-mode json-ts-mode)))
    (add-hook (derived-mode-hook-name mode) #'cc/source-code--prettier-mode-setup)
    (with-eval-after-load feature
      (let ((map (symbol-value (derived-mode-map-name mode))))
        (define-key  map (kbd "<f12>") #'prettier-buffer)
        (define-key map (kbd "C-M-\\") #'prettier-region)))))

(defun cc/source-code--prettier-mode-setup ()
  (setq-local cc/source-code--prettier-parser
              (cond
               ((derived-mode-p 'web-mode) "html")
               ((derived-mode-p 'css-mode) "css")
               ((derived-mode-p 'typescript-mode) "typescript")
               ((derived-mode-p 'json-ts-mode) "json")
               ((derived-mode-p 'js-mode) "babel")))
  (prettier-on-save-mode))

(defun cc/source-code--prettier-args ()
  (if-let ((filename (buffer-file-name)))
      (list "--stdin-filepath" (shell-quote-argument filename))
    (list "--parser" cc/source-code--prettier-parser)))


;;; LSP server config for eglot
;; Eglot makes per-server/per-mode server config way too hard, so I'm doing it globally here. In
;; theory `eglot-server-programs' values can specify :initializationOptions, but this doesn't seem
;; to work consistently. For example: setting config for nil (`nix-mode') doesn't work via
;; :initializationOptions but does work via `eglot-workspace-configurations'. Eglot events buffers
;; shows the init options are sent to the server, but they don't take effect. Not sure if it's an
;; eglot or nil issue though.
;;
;; See:
;; - https://github.com/oxalica/nil/issues/101
;; - https://github.com/joaotavora/eglot/discussions/876
;; - https://github.com/joaotavora/eglot/discussions/967

(defvar cc/source-code--eglot-workspace-config nil)

(defun cc/source-code--eglot-init ()
  (setq-default eglot-workspace-configuration #'cc/source-code--eglot-workspace-config))


(cl-defun cc/source-code-enable-eglot (modes &key auto-format config)
  "Configures `eglot' LSP for (major) modes.

MODES may be a single symbol or a list.
AUTO-FORMAT will enable `eglot-format-buffer' if non-nil.
CONFIG is used as the default `eglot-workspace-configuration' if not overridden
by a dir local. Note that the server for a workspace may need to be restarted
for this to take effect. This affects all buffers in a project using the same
server."
  (dolist (m (if (listp modes)
                 modes
               (list modes)))
    (let ((mode-hook (derived-mode-hook-name m)))
      (add-hook mode-hook #'eglot-ensure)
      (when auto-format
        (add-hook mode-hook #'cc/source-code--enable-format-on-save)))
    (setf (alist-get m cc/source-code--eglot-workspace-config) config)))

(defun cc/source-code--enable-format-on-save ()
  "Enable buffer-local `eglot-format-buffer' on save.

Intended for use with a major-mode hook."
  ;; Use -10 depth for precedence to run before eglot's `eglot--signal-textDocument/willSave'. Not
  ;; absolutely positive this is necessary, but it seems reasonable, makes the order of the hooks
  ;; consistent instead of being dependent on which is added first, and is recommended by
  ;; https://go.googlesource.com/tools/+/refs/heads/master/gopls/doc/emacs.md
  (add-hook 'before-save-hook #'eglot-format-buffer -10 t))

(defun cc/source-code--eglot-workspace-config (server)
  ;; A single LSP server instance may be used for multiple modes (at least in theory), so have to
  ;; find a matching config value for any of the server's modes.
  ;;
  ;; `cl-loop' note: "thereis" returns the first non-nil match and terminates the loop.
  (cl-loop for server-mode in (eglot--major-modes server)
           thereis (alist-get server-mode cc/source-code--eglot-workspace-config)))

(provide 'cc-source-code)
