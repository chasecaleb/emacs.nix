;;; -*- lexical-binding: t -*-
;;; Authentication-related config such as `auth-source' and GPG.
(require 'cc-utils)
(require 'auth-source)
(require 'auth-source-pass)
(require 'epg)
(require 'epa)

(defun cc/auth-init ()
  (auth-source-pass-enable)
  ;; Don't want Emacs to add an extra layer of caching because more caching = more problems. I would
  ;; rather read from the backing source (i.e. password store) each time, since gpg agent already
  ;; caches my GPG passphrase.
  (setq auth-source-do-cache nil)
  ;; Don't save/ask about saving passwords to authinfo file.
  (setq auth-source-save-behavior nil)
  (setq epg-pinentry-mode 'loopback)
  ;; The "Decrypting <foo>... done" spam is out of control during `forge-pull' in particular, since
  ;; it fetches token repeatedly. Also the message is pointless in general.
  (advice-add #'epa-progress-callback-function :around #'cc/silence-messages-advice))

(provide 'cc-auth)
