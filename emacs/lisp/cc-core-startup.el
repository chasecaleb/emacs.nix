;;; -*- lexical-binding: t -*-
;;; Core startup-related functionality.
(defvar last-log-time before-init-time)
(defun cc/log-init-time (msg)
  (let ((now (current-time))
        (elapsed (float-time
                  (time-subtract (current-time) before-init-time)))
        (delta   (float-time
                  (time-subtract (current-time) last-log-time))))
    (message "[init][%06.3fs total, +%05.3fs delta] %s"
             elapsed delta msg)
    (setq last-log-time now)))

(defmacro cc/init-feature (feature &optional init-fn)
  `(progn
     (cc/log-init-time (format "%-18s %s" "Feature loading:" ,feature))
     (require ,feature)
     (when ,init-fn
       (cc/log-init-time (format "%-18s %s" "Initializing:" ,init-fn))
       (funcall ,init-fn))
     (cc/log-init-time (format "%-18s %s" "Finished:" ,feature))))

(defun cc/core-startup-init ()
  (progn ; Startup/GUI things.
    (setq inhibit-startup-buffer-menu t)
    (setq inhibit-startup-screen t)
    (setq inhibit-compacting-font-caches t)
    (setq initial-buffer-choice t)
    (setq initial-scratch-message "")
    (scroll-bar-mode 0)
    (horizontal-scroll-bar-mode 0)
    (tool-bar-mode 0)
    (menu-bar-mode 0)))

(provide 'cc-core-startup)
