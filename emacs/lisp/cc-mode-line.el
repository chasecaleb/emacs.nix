;;; -*- lexical-binding: t -*-
(require 'eglot)
(require 'project)

(defvar-local cc/mode-line-id-length 45)

(defun cc/mode-line-init ()
  (set-face-attribute 'mode-line-buffer-id nil
                      :background "#5d4d7a" :foreground "#b2b2b2" :inherit nil)
  (set-face-attribute 'mode-line-highlight nil
                      :background "DarkGoldenrod2" :foreground "black")
  ;; Bold makes the purple-on-black more legible.
  (set-face-attribute 'mode-line-emphasis nil
                      :foreground "#bc6ec5" :background 'unspecified :weight 'bold)

  ;; Remove `eglot' from `mode-line-misc-info' because it's excessive: I don't care about the mouse
  ;; menus, nor do I need it to show the server nickname (which is the same as the already-displayed
  ;; project name).
  (setq mode-line-misc-info (seq-filter (lambda (it)
                                          (not (eq (car it) 'eglot--managed-mode)))
                                        mode-line-misc-info))
  ;; buffer id truncation done in a separate function
  (setq-default mode-line-buffer-identification "%b")
  (setq mode-line-right-align-edge 'right-fringe)
  (setq-default mode-line-format
                `("%e" ;; %e: Emacs OOM warning (which should never happen)
                  (:propertize (:eval (cc/mode-line--ace-window))
                   face mode-line-highlight)
                  " %* " ;; Buffer modification/read-only state
                  (:propertize (:eval (cc/mode-line--remote-info))
                   face mode-line-emphasis)
                  (:eval (cc/mode-line--project))
                  (:eval (cc/mode-line--buffer-id))
                  "%[" ;; %[ is recursive edit level
                  (mode-name (:eval (cc/mode-line--major-mode)))
                  mode-line-process
                  (minor-mode-alist (:eval
                                     (cond
                                      ((and (mode-line-window-selected-p)
                                            (not ace-window-mode))
                                       (format-mode-line minor-mode-alist))
                                      (ace-window-mode (propertize ace-window-mode
                                                                   'face 'mode-line-buffer-id)))))
                  "%] "
                  mode-line-misc-info
                  (:eval (cc/mode-line--eglot))
                  mode-line-format-right-align ;; Everything following is right-aligned.
                  "%n" ;; "Narrow" if narrowed
                  (mark-active (:propertize (:eval (cc/mode-line--region))
                                face mode-line-buffer-id))
                  " %l"
                  (column-number-mode ":%c")
                  " |"
                  (:eval (cc/mode-line--position-percent)))))

(defun cc/mode-line--pad (value)
  "Left/right pad a segment. Important for background colors."
  (when value
    (concat " " value " ")))

(defun cc/mode-line--right (segments)
  ;; Have to format the right side in order to determine how wide it is and then align it. See:
  ;; https://occasionallycogent.com/custom_emacs_modeline/index.html
  ;; https://github.com/protesilaos/dotfiles/blob/fdcac0071d6d9533fe8c8b1dd0653389816a478d/emacs/.emacs.d/prot-lisp/prot-modeline.el
  ;;
  ;; TODO: refactor right-alignment once Emacs 30 is out:
  ;;   https://old.reddit.com/r/emacs/comments/14u6tsl/news_rightaligned_modeline_and_more/
  (when (mode-line-window-selected-p)
    (let ((formatted (format-mode-line segments)))
      (concat (propertize " "
                          'display `((space :align-to (- (+ right right-fringe right-margin)
                                                         ,(string-width formatted)))))
              formatted))))


;;; Individual segments

(defun cc/mode-line--ace-window ()
  (let ((path (window-parameter (selected-window) 'ace-window-path)))
    (when path ; ace-window might not be running yet.
      (cc/mode-line--pad (char-to-string (seq-elt path 0))))))

(defun cc/mode-line--project ()
  ;; Performance optimization: skip remote TRAMP buffers and /nix/store/*, both of which take an
  ;; excessive amount of time. Since this is called whenever modeline is updated, this is
  ;; expensive.
  (unless (or (not default-directory)
              (file-remote-p default-directory 'host)
              (string-prefix-p "/nix/store/" default-directory))
    (when-let* ((project (project-current))
                (name (project-name project)))
      (concat "(" name ") "))))

(defun cc/mode-line--buffer-id ()
  (let ((f (if (mode-line-window-selected-p)
               'mode-line-buffer-id
             'mode-line-inactive)))
    (propertize
     (cc/mode-line--pad
      (truncate-string-to-width (format-mode-line mode-line-buffer-identification)
                                cc/mode-line-id-length nil nil "…"))
     'face f)))

(defun cc/mode-line--major-mode ()
  (propertize (cc/mode-line--pad (format-mode-line mode-name))
              'face 'mode-line-emphasis))

(defun cc/mode-line--remote-info ()
  "TRAMP remote info for e.g. /ssh: and /sudo: buffers."
  (when default-directory
    ;; It's possible for host but not user to be set depending on the SSH connection.
    (when-let ((host (file-remote-p default-directory 'host)))
      (let ((user (or (file-remote-p default-directory 'user)
                      "")))
        (concat user "@" host " ")))))

(defun cc/mode-line--eglot ()
  (when (eglot-managed-p)
    (propertize " LSP " 'face 'mode-line-emphasis)))

(defun cc/mode-line--region ()
  (let ((lines (count-lines (region-beginning) (min (1+ (region-end)) (point-max)))))
    (cc/mode-line--pad
     (if (> lines 1)
         (format "[%s lines]" lines)
       (format "[%s chars]" (- (region-end) (region-beginning)))))))

(defun cc/mode-line--position-percent ()
  "Point's position in buffer as percentage of total lines.

By comparison, the built-in %p computes position based on top of the window as
opposed to the point."
  (concat
   (format "%3d" (* 100 (/ (float (line-number-at-pos (point))) (line-number-at-pos (point-max)))))
   ;; "%" has to be escaped as "%%".
   "%%"))

(provide 'cc-mode-line)
