((forge-dispatch
  (forge-list-requested-reviews . 4))
 (magit-log
  (transient:magit-log:--sparse . 3)
  (transient:magit-log:--invert-grep . 3)
  (transient:magit-log:--regexp-ignore-case . 3)
  (magit-log:--since . 3)))
