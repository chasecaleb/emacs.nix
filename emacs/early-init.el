;;; -*- lexical-binding: t -*-
;; I don't use package.el, so disable it (which has to be done in early-init.el).
(setq package-enable-at-startup nil)

;; Add native comp output from my build (for both my own lisp and packages). Intentionally NOT using
;; /run/current-system in order to allow testing/running multiple independent Emacs at once with
;; --init-directory. Even though eln filenames are hashed so collision risk is negligible, the /run
;; symlinks are volatile so previous files would disappear on rebuild.
(startup-redirect-eln-cache (expand-file-name "eln" (file-name-directory user-init-file)))
(add-to-list 'native-comp-eln-load-path "/tmp/emacs-eln")

;; Normally packages go in /share/emacs/site-lisp (or NixOS /run equivalent), but I put everything
;; in my init directory so I can use "emacs --init-directory /nix/store/<whatever>" to test changes
;; without affecting my long-running Emacs instance.
(let ((default-directory (expand-file-name "lisp" (file-name-directory user-init-file))))
  (add-to-list 'load-path default-directory)
  (normal-top-level-add-subdirs-to-load-path))
