# shellcheck shell=bash #; -*- sh-shell: bash; -*-
# Install script for lisp packages.
#
# Note: no shebang (above) because that doesn't work the way I'm using it for a Nix builder phase.

set -Eeuo pipefail
die() {
    echo -e "$0" ERROR: "$@" >&2
    exit 1
}
# shellcheck disable=2154
trap 's=$?; die "line $LINENO - $BASH_COMMAND"; exit $s' ERR

# Inform shellcheck that I expect this to be defined.
declare out name

while IFS= read -rd '' file; do
    # Remove ./ from find output (not strictly necessary but it's awkward).
    file=$(realpath --relative-base=. "$file")

    # Native compilation (eln) files need to go in a special place, whereas other files (including
    # elc byte compilation files) all stay together.
    extension=${file##*.}
    if [[ "$extension" == "eln" ]]; then
        dest=$out/native-lisp/$file
    else
        dest=$out/lisp/$name/$file
    fi

    install -Dm644 "$file" "$dest"
    if [[ -x "$file" ]]; then
        # Preserve executable bit if necessary. This doesn't matter for lisp files but there may be
        # others (e.g. a compiled binary) that need to be executable.
        chmod +x "$dest"
    fi
done < <(find . -type f -print0 | sort -z)
