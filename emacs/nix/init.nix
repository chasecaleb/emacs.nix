{ pkgs, emacs, libraries }:
let
  buildPackage = import ./build-package.nix { inherit pkgs emacs; };
  doBuildPackage = args: buildPackage (args // {
    byteCompileErrorOnWarn = true;
    postInstall = ''
      # Instead of directory per derivation like I do for libraries, put all my init lisp in a
      # single distinct directory.
      mv --no-target-directory $out/lisp/$name $out/lisp/chasecaleb
      ${args . postInstall or ""}
    '';
    extraArgs = (args.extraArgs or { }) // {
      # src subdirectory awkwardness is to prevent Nix from packaging env-vars file - see
      # nixpkg file pkgs/stdenv/generic/setup.sh.
      sourceRoot = "./src";
      preUnpack = ''mkdir src'';
      unpackCmd = ''cp -r $curSrc ./src/$(stripHash $curSrc)'';
    };
  });

  lispPath = ./../lisp;

  computeFeatureName = path: pkgs.lib.pipe path [
    builtins.toString
    builtins.baseNameOf
    (pkgs.lib.removeSuffix ".el")
  ];

  toDrvName = path: pkgs.lib.pipe path [
    computeFeatureName
    (builtins.replaceStrings [ "/" ] [ "-" ])
    (it: "lisp-${it}")
  ];

  # right: individual feature files, wrong: tests. init.el/early-init.el are elsewhere.
  lispFiles = pkgs.lib.pipe lispPath [
    pkgs.lib.filesystem.listFilesRecursive
    (builtins.partition (path:
      let name = builtins.baseNameOf path;
      in !pkgs.lib.hasSuffix "-test.el" name))
  ];

  mapToAttrs = fn: attrs: builtins.listToAttrs (builtins.map fn attrs);

  parseMatches = regexp: path: pkgs.lib.pipe path [
    builtins.readFile
    # Split into lines instead of using builtins.match on the entire file string due to a stack
    # overflow bug that's half a decade old at this point:
    # https://github.com/NixOS/nix/issues/2147
    (pkgs.lib.splitString "\n")
    (builtins.map (builtins.match regexp))
    # Remove non-matching lines.
    (builtins.filter (it: !builtins.isNull it))
    # List of matches per line -> list of matches.
    pkgs.lib.flatten
  ];

  # Lookup map of feature name -> path. Memoized for performance (however significant that may be).
  featuresMap =
    let
      parseFeature = path:
        let
          result = parseMatches "^[[:space:]]*\\(provide '([^) ].*)\\).*" path;
          length = builtins.length result;
        in
        if length == 1
        then builtins.elemAt result 0
        else builtins.abort "Error parsing feature for ${path}: found ${length} matches.";
      toAttrs = path:
        let
          # Emacs "require" function expects feature name and file name to match, so verify this.
          actual = parseFeature path;
          expected = computeFeatureName path;
        in
        if actual == expected then
          { name = actual; value = path; }
        else
          throw "Expected ${path} to have feature ${expected}, but actual value is ${actual}.";
    in
    mapToAttrs toAttrs lispFiles.right;

  # Null (no map entry) means the feature is from a library/Emacs itself, not my own.
  getFeature = name: featuresMap.${name} or null;

  # Map of file -> its required files (NOT recursive/transitive, just direct).
  requiresMap =
    let
      parseRequired = path: pkgs.lib.pipe path [
        (parseMatches "^[[:space:]]*\\(require '([^) ]*)\\).*")
        (builtins.map getFeature)
        (builtins.filter (it: !builtins.isNull it))
      ];
      toAttrs = path: {
        name = builtins.toString path;
        value = parseRequired path;
      };
    in
    mapToAttrs toAttrs lispFiles.right;

  directRequires = path: requiresMap.${builtins.toString path};

  allRequires = path:
    let
      direct = directRequires path;
      recursive = builtins.map allRequires direct;
    in
    pkgs.lib.pipe (direct ++ recursive) [
      pkgs.lib.flatten
      pkgs.lib.unique
    ];

  lispDerivations =
    let
      requiredDerivations = path: pkgs.lib.pipe path [
        allRequires
        (builtins.map toDrvName)
        (builtins.map (it: builtins.getAttr it lispDerivations))
      ];
      toAttrs = path: {
        name = toDrvName path;
        value = doBuildPackage {
          name = toDrvName path;
          src = [ path ];
          preInstall =
            let
              subdir = builtins.dirOf (computeFeatureName path);
            in
            if subdir == "." then
              ""
            else
              ''
                # Move files into subdirectory if necessary to match source repo layout, which is
                # particularly necessary for Emacs "require" function to work with slashes in
                # feature names.
                shopt -s extglob
                mkdir -p ${subdir}
                mv !(${subdir}) ${subdir}
                shopt -u extglob
              '';
          lispInputs = (builtins.attrValues libraries) ++ (requiredDerivations path);
        };
      };
    in
    mapToAttrs toAttrs lispFiles.right;

  lispInit = doBuildPackage {
    name = "lisp-init";
    src = [ ./../init.el ./../early-init.el lispFiles.wrong ];
    # Even though I'm skipping buildPhase (compilation), lispInputs are still necessary for tests.
    lispInputs = (builtins.attrValues lispDerivations) ++ (builtins.attrValues libraries);
    # init.el/early-init.el need to be at top level of emacs --init-directory
    postInstall = ''
      mv $out/lisp/chasecaleb/{init,early-init}.el $out
    '';
    extraArgs = {
      # Skip byte compiling my main (entry-point) init file, because:
      # 1. It's expensive to compile, since Emacs has to load *all* my other init files.
      # 2. There's virtually zero performance benefit, since it just calls a few functions.
      # 3. It would have to be recompiled every time I change any other file.
      #
      # TLDR: by skipping this I'm able to cut compilation down when making a change to a single
      # file from something like 20s to 5s -- worth it.
      dontBuild = true;
      # Run all tests together (instead of attempting to associate tests with their respective
      # source files) because:
      # 1. This is easier and more robust.
      # 2. The tests themselves are fast, whereas starting up Emacs to run the tests is not as
      #    fast... so it's faster to run the tests all at once than run an Emacs process for each
      #    file.
      # 3. It's possible for a change to cause a seemingly-unrelated test to fail, so I want to be
      #    sure all my tests pass.
      #
      # There is one minor downside: the load path of each test file is not isolated
      # (unless buttercup does something on its own), but I'm not too concerned about that.
      doCheck = true;
      checkPhase = ''
        "${emacs}/bin/emacs" -Q -nw --batch --load "${./builder.el}" --funcall cc/nix-test
      '';
    };
  };
in
lispDerivations // { lisp-init = lispInit; }
