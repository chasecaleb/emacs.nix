{ pkgs, emacs }:
{ name
, src ? [ ]
, lispInputs ? [ ]
, buildInputs ? [ ]
, propagatedBuildInputs ? [ ]
  # Disable configure because I don't want magic happening unexpectedly.
, dontConfigure ? true
, nativeBuildInputs ? [ ]
, preBuild ? ""
, preInstall ? ""
, postInstall ? ""
, byteCompileErrorOnWarn ? false
, extraArgs ? { }
}: pkgs.stdenv.mkDerivation ({
  inherit name src dontConfigure nativeBuildInputs propagatedBuildInputs;
  buildInputs = [ emacs pkgs.texinfo ] ++ buildInputs ++ lispInputs;
  # Prefixing arguments used by my builder.el lisp logic with "arg*" for my sanity to help
  # me remember what they're used for.
  argLibs = lispInputs;
  argByteCompileErrorOnWarn = if byteCompileErrorOnWarn then "true" else null;
  buildPhase = ''
    set -Eeuo pipefail

    ${preBuild}

    fd_rm() {
      ${pkgs.fd}/bin/fd --max-depth 1 "$@" . --exec-batch rm -r
    }

    # Remove directories containing a .nosearch marker file: {//} is fd syntax for parent dir.
    ${pkgs.fd}/bin/fd --type f --unrestricted '^\.nosearch$' . --exec rm -r {//}

    # Remove tests
    fd_rm --type d '^(ert-)?test(|s|ing|-files)$'
    fd_rm --type f '^(.*-)?ert\.el$'
    # Match buttercup tests (e.g. flycheck-buttercup.el) but NOT the actual buttercup.el library.
    fd_rm --type f '^.*-buttercup\.el$'
    fd_rm --type f '^(.*-)?test(s)?\.el'

    # Remove hidden dirs (e.g. .github, .circleci, and .travis) and files (e.g. .gitignore).
    # Keep .dir-locals.el because it /can/ be useful, e.g. for tab-width in org.
    fd_rm --unrestricted '^\.' --exclude '.dir-locals.el'

    "${emacs}/bin/emacs" -Q -nw --batch --load "${./builder.el}" --funcall cc/nix-build "${name}"
  '';
  installPhase = ''
    set -Eeuo pipefail

    ${preInstall}
    ${./installer.sh}
    ${postInstall}
    # Restore +u to prevent errors on edge cases with e.g. patch-shebangs.
    # https://github.com/NixOS/nixpkgs/issues/247410
    set +u
  '';
  preFixup = if extraArgs ? dontBuild && extraArgs.dontBuild then null else ''
    "${emacs}/bin/emacs" -Q -nw --batch --load "${./eln-fixup.el}" --funcall cc/eln-fixup "$out"
  '';
} // extraArgs)
