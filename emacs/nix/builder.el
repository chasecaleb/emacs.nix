;;; -*- lexical-binding: t -*-
(require 'bytecomp)
(require 'comp)
(require 'seq)
(require 'subr-x)

(defun cc/nix-build ()
  (when (string= (getenv "argByteCompileErrorOnWarn") "true")
    ;; I use this for my own init.
    (setq byte-compile-error-on-warn t))
  (cc/compile-lisp)
  (cc/make-info))

(defun cc/nix-test ()
  (cc/setup-load-path)
  ;; Don't need to require buttercup because this has an autoload cookie, which is a good thing
  ;; because buttercup isn't always available to load (e.g. when compiling buttercup itself, or
  ;; other libraries that don't use it).
  (buttercup-run-discover))

(defun cc/compile-lisp ()
  (cc/setup-load-path)
  (let* ((project-dirs (cc/project-dirs default-directory))
         (total (seq-reduce #'+ (seq-map #'cc/do-compile project-dirs) 0)))
    (if (zerop total)
        ;; Safeguard against bugs.
        (error "No files compiled")
      (message "*** Total files compiled: %s ***" total)))
  (let ((name (car command-line-args-left))
        (dirs (cons default-directory
                    (seq-filter #'file-directory-p
                                (directory-files-recursively default-directory "" t)))))
    (make-directory-autoloads dirs (concat name "-autoloads.el"))))

(defun cc/make-info ()
  (dolist (texi (directory-files-recursively default-directory (rx ".texi" (* "nfo") eos)))
    (let ((info (concat (file-name-sans-extension texi) ".info")))
      (unless (member (file-name-nondirectory texi) (list "fdl.texi" "gpl.texi"))
        (message "Building info file from %s to %s" texi info)
        (cc/shell-command "makeinfo --no-split %s -o %s" (list texi info)))))
  ;; Search for .info files separately to avoid missing some info that don't have a corresponding
  ;; .texi. For example: org-super-agenda builds its info page from README.org.
  (dolist (info (directory-files-recursively default-directory (rx ".info" eos)))
    (message "Installing info file: %s" info)
    (cc/shell-command "install-info %s --dir=%s"
                      (list info (expand-file-name "dir" (file-name-directory info))))))

(defun cc/setup-load-path ()
  ;; Need library to be able to load its own files.
  (seq-map #'cc/add-load-path (cc/project-dirs default-directory))
  (seq-doseq (dir (apply #'append
                         (seq-map (lambda (it)
                                    (cc/project-dirs it))
                                  (split-string (getenv "argLibs") " " t))))
    (cc/add-load-path dir))
  ;; Need autoloads, too. Especially for my own init code.
  (dolist (lib (split-string (getenv "argLibs") " " t))
    ;; Presumably only one autoload file, but dolist is simultaneously simpler and more robust.
    (dolist (autoload (directory-files-recursively lib (rx "-autoloads.el" eos)))
      (load autoload))))

(defun cc/add-load-path (path)
  (push (directory-file-name path) load-path))

(defun cc/project-dirs (base)
  "Return list of BASE directory and all of its subdirectories."
  (cons base (seq-filter #'file-directory-p
                         (directory-files-recursively base "" t))))

(defun cc/directory-files-flat (dirs &optional full match)
  (apply #'append (seq-map (lambda (d)
                             (directory-files d full match))
                           dirs)))

(defun cc/do-compile (dir)
  "Similar to and partially based on `batch-byte+native-compile'."
  (let ((count 0))
    (dolist (file (directory-files dir t (rx ".el" eos)))
      (cc/do-compile-file file)
      (cl-incf count))
    (message "Files compiled in %s: %s" dir count)
    count))

(defun cc/do-compile-file (file)
  "Roughly a non-batch re-implementation of `batch-byte+native-compile'."
  (let ((name (file-name-nondirectory file))
        ;; Do compilation in the current process (instead of spawning a child Emacs). See
        ;; `comp-final', which spawns a process unless `comp-running-batch-compilation' or
        ;; `comp-async-compilation' are set... which seems to be a protection against GCC memory
        ;; leaks, or something along those lines. Enabling `comp-running-batch-compilation' also
        ;; significantly speeds up build times. For example, org went from 12 minutes down to 7.5.
        ;; This also makes a *huge* difference for yaml.el in particular: from an hour down to 1.5
        ;; minutes, which I suspect is due to defmacro expansion madness.
        (comp-running-batch-compilation t)
        ;; Output native compilation file to Nix's build directory and let Nix's installPhase handle
        ;; moving to the right directory based on file extension, since Emacs can't write to
        ;; anything in `native-comp-eln-load-path' during Nix build.
        (native-compile-target-directory default-directory)
        ;; `byte-compile-debug' is misleadingly named - it actually means raise a signal (throw an
        ;; error) instead of just outputting a message on errors. See its docstring.
        (byte-compile-debug t)
        (byte+native-compile t)
        ;; Need to reset `byte-to-native-output-file' for each file due to `setf' mutation.
        ;; See `batch-byte+native-compile' function.
        (byte-to-native-output-file nil))
    (when (and (not (string-match-p (rx bos ".") name))
               (not (string-match-p (rx "-autoloads.el" eos) name))
               (not (string-equal dir-locals-file name)))
      (message "Compiling: %s" file)
      (native-compile file)
      ;; pcase logic taken from `batch-byte+native-compile': `byte-to-native-output-file' is a
      ;; cons cell of (temp . target) files, where temp is foo.eln<some-hash> and target is
      ;; foo.eln. Apparently `byte-compile-file' writes to a temp file and defers renaming to
      ;; the file target for... reasons that I don't fully understand or care about. Something
      ;; to do with hard links, disk space, and recompiling.
      (pcase byte-to-native-output-file
        (`(,tempfile . ,target-file)
         (rename-file tempfile target-file t))))))

;; Taken from borg, an awesome Emacs build tool that I used to use. Fun fact: the developer is the
;; same genius who wrote magit, forge, transient, and more.
;; https://github.com/emacscollective/borg/
(defmacro borg-silencio (regexp &rest body)
  "Execute the forms in BODY while silencing messages that don't match REGEXP."
  (declare (indent 1))
  (let ((msg (make-symbol "msg")))
    `(let ((,msg (symbol-function 'message)))
       (cl-letf (((symbol-function 'message)
                  (lambda (format-string &rest args)
                    (unless (string-match-p ,regexp format-string)
                      (apply ,msg format-string args)))))
         ,@body))))

(defun cc/shell-command (cmd &optional args)
  "Sane version of `shell-command', sans multiple foot-guns."
  (borg-silencio
   "\\`(Shell command succeeded with %s)\\'"
   (let* ((real-cmd (if args
                        (apply #'format (cons cmd (seq-map #'shell-quote-argument args)))
                      cmd))
          (exit (shell-command real-cmd)))
     ;; Use `message' because long strings get truncated in the backtrace from `error'... which is
     ;; mind-bogglingly dumb.
     (unless (zerop exit)
       (let ((msg (format "Command failed (exit %s): %s" exit real-cmd)))
         (message msg)
         (error msg))))))
