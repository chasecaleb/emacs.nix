;;; -*- lexical-binding: t -*-
;;; Fix hashes in .eln filenames due to differences in the absolute path of .el source files at
;;; build time vs runtime.
;;;
;;; Reference: source of lisp function `comp-el-to-eln-rel-filename', defined in comp.c, which has
;;; the hashing algorithm for .eln filenames. The first hash is an md5 of the *absolute* filename,
;;; which is a problem because the Nix build occurs in /build/source but runtime path (after
;;; resolving symlinks) is /nix/store/<whatever>.

(defun cc/eln-fixup ()
  (let* ((out (car command-line-args-left))
         ;; Keep in mind the actual eln's are in an Emacs version-specific subdirectory.
         (native (expand-file-name "native-lisp" out))
         (count 0))
    (message "Fixing eln filename hashes in %s" native)
    (dolist (source (directory-files-recursively out (rx ".el" eos) nil nil t))
      ;; eln might not exist since e.g. autoloads aren't compiled.
      (when-let ((expected-relative (comp-el-to-eln-rel-filename source))
                 (existing (cc/maybe-file native (cc/eln-regexp expected-relative))))
        (message "Renaming eln: %s -> %s"
                 (file-name-nondirectory existing)
                 expected-relative)
        (rename-file existing (expand-file-name expected-relative (file-name-directory existing)))
        (cl-incf count)))
    (when (zerop count)
      ;; Safeguard against bugs.
      (error "No eln files found to fix in %s" out))))

(defun cc/eln-regexp (eln-file)
  "Regexp matching ELN-FILE regardless of the filename hash portion."
  (let ((original (file-name-nondirectory eln-file)))
    ;; eln file naming: <feature>-<absolute filename hash>-<content hash>.eln
    (string-match (rx bos (group (+ any) "-") (+ alnum) (group "-" (+ any) ".eln") eos) original)
    (rx bos (literal (match-string 1 original)) (+ alnum) (literal (match-string 2 original)))))

(defun cc/maybe-file (dir regexp)
  (let ((matches (directory-files-recursively dir regexp)))
    (pcase (length matches)
      (0 nil)
      (1 (car matches))
      (_ (error "Found multiple files matching %s: %s" regexp matches)))))
