{ pkgs, emacs, inputs }:
let
  buildPackage = import ./build-package.nix { inherit pkgs emacs; };

  # Is this a gross hack or a stroke of elegance? Both, probably. But it's a simple solution,
  # it's reliable, and I like doing this within the package at issue instead of hidden
  # somewhere in my init lisp.
  lispStringQuote = value:
    let
      quoted = builtins.replaceStrings [ ''"'' ] [ ''\"'' ] value;
    in
    ''"${quoted}"'';
  lispSet = var: value: ''
    echo ${pkgs.lib.escapeShellArg "(setq ${var} ${value})"} >> ./*-autoloads.el
  '';
  lispSetString = var: value:
    lispSet var (lispStringQuote value);
  lispSetList = var: value:
    let
      quoted = builtins.map (elem: lispStringQuote elem) value;
      joined = builtins.concatStringsSep " " quoted;
    in
    lispSet var "'(${joined})";
in
# Keep a blank line between each library so that I can format via M-x sort-paragraphs
rec {

  ace-window = buildPackage {
    name = "ace-window";
    src = inputs.ace-window;
    lispInputs = [ avy ];
  };

  alert = buildPackage {
    name = "alert";
    src = inputs.alert;
  };

  all-the-icons = buildPackage {
    name = "all-the-icons";
    src = inputs.all-the-icons;
  };

  async = buildPackage {
    name = "async";
    src = inputs.async;
  };

  async-await = buildPackage {
    name = "async-await";
    src = inputs.async-await;
    lispInputs = [ iter2 promise ];
  };

  atomic-chrome = buildPackage {
    name = "atomic-chrome";
    src = inputs.atomic-chrome;
    lispInputs = [ websocket ];
  };

  avy = buildPackage {
    name = "avy";
    src = inputs.avy;
    # Remove extraneous build files, especially since targets/checkdoc.el could shadow the real
    # checkdoc. Discovered by running (list-load-path-shadows t)
    preBuild = "rm -rf ./targets";
  };

  buttercup = buildPackage {
    name = "buttercup";
    src = inputs.buttercup;
  };

  closql = buildPackage {
    name = "closql";
    src = inputs.closql;
    lispInputs = [ compat emacsql ];
  };

  company-mode = buildPackage {
    name = "company-mode";
    src = inputs.company-mode;
  };

  compat = buildPackage {
    name = "compat";
    src = inputs.compat;
  };

  consult = buildPackage {
    name = "consult";
    src = inputs.consult;
    lispInputs = [ compat org ];
  };

  consult-yasnippet = buildPackage {
    name = "consult-yasnippet";
    src = inputs.consult-yasnippet;
    lispInputs = [ compat consult yasnippet ];
  };

  dash = buildPackage {
    name = "dash";
    src = inputs.dash;
  };

  deferred = buildPackage {
    name = "deferred";
    src = inputs.deferred;
  };

  diff-hl = buildPackage {
    name = "diff-hl";
    src = inputs.diff-hl;
  };

  diminish = buildPackage {
    name = "diminish";
    src = inputs.diminish;
  };

  doct = buildPackage {
    name = "doct";
    src = inputs.doct;
  };

  dumb-jump = buildPackage {
    name = "dumb-jump";
    src = inputs.dumb-jump;
    lispInputs = [ dash popup-el s ];
  };

  edit-indirect = buildPackage {
    name = "edit-indirect";
    src = inputs.edit-indirect;
  };

  emacs-sqlite-api = buildPackage {
    name = "emacs-sqlite-api";
    src = inputs.emacs-sqlite-api;
    buildInputs = [ pkgs.sqlite ];
    preBuild = ''
      make module
      # "make module" outputs to ./sqlite3-<version>/, the top-level ./sqlite3.el shouldn't be used.
      rm ./sqlite3.el
    '';
  };

  emacsql = buildPackage {
    name = "emacsql";
    src = inputs.emacsql;
    buildInputs = [ pkgs.sqlite ];
    lispInputs = [ emacs-sqlite-api ];
    preBuild = ''
      # Don't care about postgresql functionality and don't want the dependency.
      rm emacsql-pg.el
    '';
  };

  embark = buildPackage {
    name = "embark";
    src = inputs.embark;
    lispInputs = [ avy compat consult org ];
  };

  epl = buildPackage {
    name = "epl";
    src = inputs.epl;
  };

  exwm = buildPackage {
    name = "exwm";
    src = inputs.exwm;
    lispInputs = [ compat xelb ];
  };

  exwm-edit = buildPackage {
    name = "exwm-edit";
    src = inputs.exwm-edit;
    lispInputs = [ compat exwm xelb ];
  };

  f = buildPackage {
    name = "f";
    src = inputs.f;
    lispInputs = [ s dash ];
  };

  forge = buildPackage {
    name = "forge";
    src = inputs.forge;
    lispInputs = [ closql compat dash emacsql ghub llama magit markdown-mode transient treepy with-editor yaml ];
  };

  gcmh = buildPackage {
    name = "gcmh";
    src = inputs.gcmh;
  };

  ghub = buildPackage {
    name = "ghub";
    src = inputs.ghub;
    lispInputs = [ compat llama treepy ];
  };

  git-link = buildPackage {
    name = "git-link";
    src = inputs.git-link;
  };

  git-modes = buildPackage {
    name = "git-modes";
    src = inputs.git-modes;
    lispInputs = [ compat ];
  };

  git-timemachine = buildPackage {
    name = "git-timemachine";
    src = inputs.git-timemachine;
  };

  go-mode = buildPackage {
    name = "go-mode";
    src = inputs.go-mode;
  };

  hcl-mode = buildPackage {
    name = "hcl-mode";
    src = inputs.hcl-mode;
  };

  hl-prog-extra = buildPackage {
    name = "hl-prog-extra";
    src = inputs.hl-prog-extra;
  };

  hl-todo = buildPackage {
    name = "hl-todo";
    src = inputs.hl-todo;
    lispInputs = [ compat ];
  };

  ht = buildPackage {
    name = "ht";
    src = inputs.ht;
    lispInputs = [ dash ];
  };

  hydra = buildPackage {
    name = "hydra";
    src = inputs.hydra;
    # org seems like a weird dependency, but it's because of hydra-ox.el
    lispInputs = [ org ];
    preBuild = ''rm -r ./targets'';
  };

  iter2 = buildPackage {
    name = "iter2";
    src = inputs.iter2;
  };

  kotlin-mode = buildPackage {
    name = "kotlin-mode";
    src = inputs.kotlin-mode;
  };

  kubel = buildPackage {
    name = "kubel";
    src = inputs.kubel;
    # Trigger warning: evil mode is evil.
    preBuild = "rm kubel-evil.el";
    lispInputs = [ dash s yaml-mode ];
  };

  lab = buildPackage {
    name = "lab";
    src = inputs.lab;
    lispInputs = [ alert compat async-await dash embark f iter2 memoize promise s request ];
  };

  llama = buildPackage {
    name = "llama";
    src = inputs.llama;
    lispInputs = [ compat ];
  };

  magit = buildPackage {
    name = "magit";
    src = inputs.magit;
    lispInputs = [ compat dash llama transient with-editor ];
    # Magit crashes during byte-compilation without pkgs.git... as do any packages which
    # load it, thus git needs to be propagated.
    propagatedBuildInputs = [ pkgs.git ];
  };

  magit-delta = buildPackage {
    name = "magit-delta";
    src = inputs.magit-delta;
    lispInputs = [ compat dash llama magit xterm-color with-editor ];
    preInstall = lispSetString "magit-delta-delta-executable" "${pkgs.delta}/bin/delta";
  };

  marginalia = buildPackage {
    name = "marginalia";
    src = inputs.marginalia;
    lispInputs = [ compat ];
  };

  markdown-mode = buildPackage {
    name = "markdown-mode";
    src = inputs.markdown-mode;
  };

  memoize = buildPackage {
    name = "memoize";
    src = inputs.memoize;
  };

  mermaid-mode = buildPackage {
    name = "mermaid-mode";
    src = inputs.mermaid-mode;
    lispInputs = [ dash f s ];
  };

  nginx-mode = buildPackage {
    name = "nginx-mode";
    src = inputs.nginx-mode;
  };

  nix-mode = buildPackage {
    name = "nix-mode";
    src = inputs.nix-mode;
    lispInputs = [ compat dash llama magit reformatter ];
    preBuild = "rm nix-company.el nix-mode-mmm.el";
  };

  ob-async = buildPackage {
    name = "ob-async";
    src = inputs.ob-async;
    lispInputs = [ async dash org ];
  };

  ob-http = buildPackage {
    name = "ob-http";
    src = inputs.ob-http;
    lispInputs = [ org s ];
  };

  ob-kubectl = buildPackage {
    name = "ob-kubectl";
    src = inputs.ob-kubectl;
    lispInputs = [ org yaml-mode ];
  };

  orderless = buildPackage {
    name = "orderless";
    src = inputs.orderless;
  };

  org = buildPackage {
    name = "org";
    src = inputs.org;
    preBuild = ''
      # The way targets.mk determines ORGVERSION is broken (or at least not working for me),
      # so need to do it myself.
      org_version=$(grep -E "^;;\s*Version:\s+" lisp/org.el | awk '{print $NF}')
      flags=("prefix=$out/lisp" "lispdir=$out/lisp/org" "infodir=$out/lisp/org/info" "ORGVERSION=$org_version")
      # Verify prefix config worked (in case e.g. Org changes their make files)
      # P.S. the double single quotes escape the $... super intuitive, right?
      make "''${flags[@]}" config | grep "lispdir\s*=\s* $out/lisp/org"
      make "''${flags[@]}"
      make "''${flags[@]}" install-info

      # Make autoloads file match my naming convention so that I can load it the same way.
      # However still keep the original too, just in case it's needed for... whatever.
      cp ./lisp/org-{loaddefs,autoloads}.el
    '';
  };

  org-contrib = buildPackage {
    name = "org-contrib";
    src = inputs.org-contrib;
    # This will actually compile *without* org as an input, but that's because it will end
    # up relying on the grossly outdated version of org built into Emacs. Someday I should
    # look into removing the built-in version of org from Emacs.
    lispInputs = [ org ];
    preBuild = ''
      # ob-stata.el fails to compile due to ess-custom dependency, whatever that is.
      rm ./lisp/ob-stata.el
    '';
  };

  org-noter = buildPackage {
    name = "org-noter";
    src = inputs.org-noter;
    lispInputs = [ org ];
    # Circular dependency of org-noter <-> org-pdftools, but fortunately I don't actually
    # need this. P.S. circular dependencies are evil.
    preBuild = "rm other/org-noter-integration.el";
  };

  org-pdftools = buildPackage {
    name = "org-pdftools";
    src = inputs.org-pdftools;
    lispInputs = [ org org-noter pdf-tools tablist ];
    preBuild = "rm org-noter-pdftools.el && export HOME=/tmp";
  };

  org-roam = buildPackage {
    name = "org-roam";
    src = inputs.org-roam;
    lispInputs = [ compat dash emacsql llama magit org ];
    preBuild = "make docs";
    preInstall = lispSetString "org-roam-graph-executable" "${pkgs.graphviz}/bin/dot";
  };

  org-roam-ui = buildPackage {
    name = "org-roam-ui";
    src = inputs.org-roam-ui;
    lispInputs = [ compat dash emacsql llama magit org org-roam web-server websocket ];
  };

  org-super-agenda = buildPackage {
    name = "org-super-agenda";
    src = inputs.org-super-agenda;
    lispInputs = [ dash ht org s ts ];
  };

  org-ql = buildPackage {
    name = "org-ql";
    src = inputs.org-ql;
    lispInputs = [ compat dash f ht org org-super-agenda ov s ts ];
    # Examples don't build (and I don't need to package them anyways).
    preBuild = ''rm -r helm-org-ql.el examples'';
  };

  orgit = buildPackage {
    name = "orgit";
    src = inputs.orgit;
    lispInputs = [ compat dash llama magit org with-editor ];
  };

  ov = buildPackage {
    name = "ov";
    src = inputs.ov;
  };

  page-break-lines = buildPackage {
    name = "page-break-lines";
    src = inputs.page-break-lines;
  };

  pdf-tools = buildPackage {
    name = "pdf-tools";
    src = inputs.pdf-tools;
    lispInputs = [ org tablist ];
    nativeBuildInputs = [ pkgs.autoconf pkgs.automake pkgs.pkg-config ];
    buildInputs = [ pkgs.libpng pkgs.zlib pkgs.poppler ];
    preBuild = "(cd server && ${pkgs.bash}/bin/bash ./autobuild)";
  };

  phscroll = buildPackage {
    name = "phscroll";
    src = inputs.phscroll;
    lispInputs = [ org ];
  };

  pkg-info = buildPackage {
    name = "pkg-info";
    src = inputs.pkg-info;
    lispInputs = [ epl ];
  };

  popup-el = buildPackage {
    name = "popup-el";
    src = inputs.popup-el;
  };

  promise = buildPackage {
    name = "promise";
    src = inputs.promise;
  };

  pulsar = buildPackage {
    name = "pulsar";
    src = inputs.pulsar;
  };

  rainbow-delimiters = buildPackage {
    name = "rainbow-delimiters";
    src = inputs.rainbow-delimiters;
  };


  reformatter = buildPackage {
    name = "reformatter";
    src = inputs.reformatter;
  };

  request = buildPackage {
    name = "request";
    src = inputs.request;
    lispInputs = [ deferred ];
  };

  s = buildPackage {
    name = "s";
    src = inputs.s;
  };

  shfmt = buildPackage {
    name = "shfmt";
    src = inputs.shfmt;
    lispInputs = [ reformatter ];
  };

  smartparens = buildPackage {
    name = "smartparens";
    src = inputs.smartparens;
    lispInputs = [ dash ];
  };

  spacemacs-theme = buildPackage {
    name = "spacemacs-theme";
    src = inputs.spacemacs-theme;
  };

  spell-fu = buildPackage {
    name = "spell-fu";
    src = inputs.spell-fu;
  };

  spinner = buildPackage {
    name = "spinner";
    src = inputs.spinner;
  };

  svg-lib = buildPackage {
    name = "svg-lib";
    src = inputs.svg-lib;
  };

  symbol-overlay = buildPackage {
    name = "symbol-overlay";
    src = inputs.symbol-overlay;
  };

  tablist = buildPackage {
    name = "tablist";
    src = inputs.tablist;
  };

  terraform-mode = buildPackage {
    name = "terraform-mode";
    src = inputs.terraform-mode;
    lispInputs = [ dash hcl-mode ];
  };

  todoist = buildPackage {
    name = "todoist";
    src = inputs.todoist;
    lispInputs = [ dash org ];
  };

  transient = buildPackage {
    name = "transient";
    src = inputs.transient;
    lispInputs = [ compat ];
  };

  transpose-frame = buildPackage {
    name = "transpose-frame";
    src = inputs.transpose-frame;
  };

  # Emacs by default expects tree-sitter modules to be named libtree-sitter-<name>.so and will look
  # in a few places including <user-emacs-dir>/tree-sitter. See treesit-load-name-override-list and
  # treesit-extra-load-path.
  tree-sitter-languages =
    let all = pkgs.tree-sitter.withPlugins (p: pkgs.tree-sitter.allGrammars);
    in pkgs.runCommand "tree-sitter-languages" { } ''
      mkdir -p $out/tree-sitter
      while IFS= read -rd "" file; do
        cp $file $out/tree-sitter/libtree-sitter-$(basename $file)
      done < <(find -L ${all} -type f -print0)
    '';

  treepy = buildPackage {
    name = "treepy";
    src = inputs.treepy;
  };

  ts = buildPackage {
    name = "ts";
    src = inputs.ts;
    lispInputs = [ dash s ];
  };

  typescript = buildPackage {
    name = "typescript";
    src = inputs.typescript;
  };

  use-package = buildPackage {
    name = "use-package";
    src = inputs.use-package;
  };

  vertico = buildPackage {
    name = "vertico";
    src = inputs.vertico;
    lispInputs = [ compat ];
  };

  visual-regexp = buildPackage {
    name = "visual-regexp";
    src = inputs.visual-regexp;
  };

  visual-fill-column = buildPackage {
    name = "visual-fill-column";
    src = inputs.visual-fill-column;
  };

  vterm = buildPackage {
    name = "vterm";
    src = inputs.vterm;
    dontConfigure = false;
    nativeBuildInputs = [ pkgs.cmake ];
    buildInputs = [ pkgs.libvterm-neovim ];
    preBuild = ''
      make
      # Make script changes to ./build subdirectory, which breaks my build logic.
      cd ../
      # Cleanup build so that it doesn't get packaged by my install script.
      rm -r ./build
    '';
  };

  web-mode = buildPackage {
    name = "web-mode";
    src = inputs.web-mode;
  };

  web-server = buildPackage {
    name = "web-server";
    src = inputs.web-server;
  };

  websocket = buildPackage {
    name = "websocket";
    src = inputs.websocket;
  };

  wgrep = buildPackage {
    name = "wgrep";
    src = inputs.wgrep;
    # build-package.nix's cleanup rules match e.g. *-test.el but not *-subtest.el
    preBuild = ''rm wgrep-subtest.el'';
  };

  with-editor = buildPackage {
    name = "with-editor";
    src = inputs.with-editor;
    lispInputs = [ compat ];
  };

  xelb = buildPackage {
    name = "xelb";
    src = inputs.xelb;
    lispInputs = [ compat ];
  };

  xterm-color = buildPackage {
    name = "xterm-color";
    src = inputs.xterm-color;
  };

  yaml = buildPackage {
    name = "yaml";
    src = inputs.yaml;
  };

  yaml-mode = buildPackage {
    name = "yaml-mode";
    src = inputs.yaml-mode;
  };

  yasnippet = buildPackage {
    name = "yasnippet";
    src = inputs.yasnippet;
  };

}
