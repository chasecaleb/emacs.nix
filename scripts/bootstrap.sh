#!/usr/bin/env bash
# USAGE:
#   1. Run bootstrap-key.sh on an existing machine to generate a new SOPS key.
#   2. Commit and push secrets changes.
#   3. Boot the NixOS live ISO.
#   4. Transfer the generated SOPS private key (via e.g. USB drive or SSH).
#   5. Run this script: sudo bash -c "$(curl -fsSL https://bit.ly/emacsdotnix)"
#
# To transfer key via ncat:
#   *On current machine*:
#   1. Set VM network adapter to bridge mode if needed to get past NAT.
#   2. Open up firewall port TEMPORARILY:
#      networking.firewall.allowedTCPPorts = [ 31337 ]
#   3. sudo cat <new-key-file> | ncat -l --send-only
#
#   *On new machine*: ncat <ip> --recv-only > some-destination
#   Or possibly nix --extra-experimental-features flakes shell nixpkgs#nmap -c ...
#
# Note: ncat can also be done the other way around (new machine listens, current machine sends) if
# that works better for firewall/NAT reasons.

set -Eeuo pipefail
die() {
    echo -e "$0" ERROR: "$@" >&2
    exit 1
}
# shellcheck disable=2154
trap 's=$?; die "line $LINENO - $BASH_COMMAND"; exit $s' ERR

confirm_or_die() {
    PROMPT=$1
    local confirm
    read -re -p "$PROMPT [y/N] " confirm
    if [[ "$confirm" != "y" && "$confirm" != "Y" ]]; then
        die "Cancelled."
    fi
}

# Can't modify /etc/nix/nix.conf on the live ISO, so this will have to do instead.
do_nix() {
    nix --extra-experimental-features nix-command --extra-experimental-features flakes "$@"
}

get_nixos_machine() {
    local machine_list=()
    local i=0
    while IFS= read -r line; do
        i=$((i + 1))
        machine_list+=("$line" "")
    done < <(do_nix eval '.#nixosConfigurations' \
        --apply 'builtins.attrNames' --json \
        | jq --raw-output --exit-status '.[]')
    machine_name=$(dialog --stdout --menu "Select NixOS configuration" 0 0 0 "${machine_list[@]}")
    clear
}

if [[ $UID != 0 ]]; then
    # So technically most of this script could run as a normal user and then elevate a couple parts
    # (e.g. nixos-install) with sudo, but considering this is running in a live ISO there's no harm
    # in running the whole thing as root.
    die "This script must be run as root"
fi

read -re -p "SOPS key file: " sops_key_file_input
if [[ ! -e "$sops_key_file_input" ]]; then
    die "Key file does not exist: $sops_key_file_input"
elif ! grep --quiet "AGE-SECRET-KEY" "$sops_key_file_input"; then
    die "Expected \$1 to be an AGE private key for SOPS: $sops_key_file_input"
fi
# Potentially need key on ISO for activation script.
sops_key_file_iso=/var/lib/private/sops.key
mkdir -p "$(dirname "$sops_key_file_iso")"
cp "$sops_key_file_input" "$sops_key_file_iso"

# nix-env is bad and I should feel bad for using it, but this is a live ISO throwaway environment so
# I just don't care.
nix-env -iA nixos.git nixos.jq nixos.dialog

repo_dir=/tmp/emacs.nix
if [[ -e "$repo_dir" ]]; then
    rm -rf "$repo_dir"
fi
git clone https://gitlab.com/chasecaleb/emacs.nix "$repo_dir"
cd "$repo_dir"
get_nixos_machine
./scripts/bootstrap-hardware.sh

sops_key_destination=/mnt/$sops_key_file_iso
# Transfer SOPS key to the installed system.
mkdir -p "$(dirname "$sops_key_destination")"
cp "$sops_key_file_iso" "$sops_key_destination"

cd /
# No root password to avoid prompt at end of install.
nixos-install --no-root-password --root /mnt --flake "$repo_dir/#${machine_name}"
