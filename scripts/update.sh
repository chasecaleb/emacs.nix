#!/usr/bin/env bash
# Nix update utility script.

set -Eeuo pipefail
die() {
    echo -e "$0" ERROR: "$@" >&2
    exit 1
}
# shellcheck disable=2154
trap 's=$?; die "line $LINENO - $BASH_COMMAND"; exit $s' ERR

help() {
    echo "Usage: [command] [command arguments]"
    echo ""
    echo "Commands:"
    while IFS= read -r name; do
        # Magic to extract no-op colon lines as doc strings.
        doc=$(type "$name" | sed -nE 's/^[[:space:]]*: ?"(.*)";/\1/p')
        if [[ "$doc" != "" ]]; then
            echo "* $name:"
            # shellcheck disable=2001
            echo "$doc" | sed 's/^/    /g'
        fi
    done < <(compgen -A function)
}

# shellcheck disable=2120 # Args may be passed from command line.
fetch() {
    : "Update flake lock file and corresponding git repo clones."
    : "To update only specific inputs, specify them as positional arguments."

    local lock_args=()
    if [[ "$#" == 0 ]]; then
        lock_args=("--recreate-lock-file")
    else
        for arg in "$@"; do
            lock_args+=(--update-input "$arg")
        done
    fi
    nix flake lock "${lock_args[@]}"

    "$script_dir/fetch-sources.sh" fetch
}

fetch-os() {
    : "Update lock file for system (i.e. non-Emacs) inputs."
    fetch nixpkgs homeManager nixIndexDatabase nixosHardware sopsNix
}

sources() {
    : "Show magit repo list for flake input sources."
    "$script_dir/fetch-sources.sh" view
}

build() {
    : "Build NixOS config, check it, and show changed versions."
    nixos-rebuild build -L
    nix flake check
    nix store diff-closures /run/current-system ./result
}

full-diff() {
    : "Show full diff of changes using nix-diff."
    nix-diff /run/current-system ./result
}

images() {
    : "Update docker image digests in pseudo lock file"
    local file image locked
    while IFS= read -rd '' file; do
        image=$(jq -er '.image' "$file")
        echo "Checking image: $image"
        sudo docker pull "$image"
        locked=$(sudo docker image inspect "$image" | jq -er '.[0].RepoDigests[0]')
        jq -e --null-input \
            --arg image "$image" \
            --arg locked "$locked" \
            '{image: $image, locked: $locked}' >"$file"
    done < <(find ./nixos-config/docker-images -type f -print0 | sort -z)
}

all() {
    : "Fetch and build."
    images
    fetch
    build
}

script_dir=${BASH_SOURCE%/*}
cd "$script_dir/../"
if [[ $# == 0 ]]; then
    help
    echo ""
    die "No command provided"
else
    TIMEFORMAT="Command completed in %3lR"
    time "$@"
fi
