#!/usr/bin/env bash
# Update flake inputs. This is a standalone script because I don't want to put my GitHub token in
# /etc/nix/nix.conf. The token has zero privileges and exists just to avoid API rate limiting, but
# still.

set -Eeuo pipefail
die() {
    echo -e "$0" ERROR: "$@" >&2
    exit 1
}
export -f die
# shellcheck disable=2154
trap 's=$?; die "line $LINENO - $BASH_COMMAND"; exit $s' ERR

get_input_nodes() {
    nix flake metadata --json \
        | jq --compact-output --exit-status '.locks.nodes | to_entries'
}

get_input() {
    # shellcheck disable=SC2016 # $name is a jq arg.
    jq_get --arg name "$1" '.[] | select(.key == $name) | .value'
}
export -f get_input

jq_get() {
    jq --exit-status --raw-output "$@"
}
export -f jq_get

fetch_package() {
    set -Eeuo pipefail # Because fucking parallel doesn't inherit this from parent.
    name=$1
    {
        new_input=$(get_input "$name" <<<"$new_inputs")

        type=$(jq_get ".locked.type" <<<"$new_input")
        if [[ "$type" == "github" || "$type" == "gitlab" ]]; then
            owner=$(jq_get ".locked.owner" <<<"$new_input")
            repo_name=$(jq_get ".locked.repo" <<<"$new_input")

            url="git@$type.com:$owner/$repo_name"
        elif [[ "$type" == "git" ]]; then
            url=$(jq_get ".locked.url" <<<"$new_input")
        else
            die "Unexpected type $type: $new_input"
        fi

        # Checkout location based on flake input name, *not* repo name. Barring unlikely-but-possible
        # edge cases such as multiple flake inputs with the same repo name, either one would work just
        # fine. However, basing it on the flake name matches how I use them in Nix and where they get
        # packaged/installed.
        checkout_dir=$destination_all/$name
        echo ""
        if [[ ! -e "$checkout_dir" ]]; then
            echo "Cloning:  $checkout_dir"
            git clone "$url" "$checkout_dir"
        elif [[ "$do_fetch" == "true" ]]; then
            echo "Fetching: $checkout_dir"
            (cd "$checkout_dir" && git fetch --all)
        fi

        new_revision=$(jq_get ".locked.rev" <<<"$new_input")
        old_revision=$(get_input "$name" <<<"$old_inputs" | jq_get ".locked.rev")
        (
            cd "$checkout_dir"
            # Silence annoying message.
            git config --local advice.detachedHead false
            # git branch --force won't modify the current branch, so start with detached head. Using the
            # revision that we're about to switch to (instead of e.g. the old revision) avoids
            # extraneous filesystem churn.
            git checkout "$new_revision"
            # Make two branches as markers for comparison:
            #   1. The old (pre-update) revision.
            #   2. The new revision, with old set as upstream.
            # Result: working copy will be the new revision. Magit will display the new commits in its
            # "unmerged into upstream" section.
            git branch --force cc/old "$old_revision"
            git branch --force cc/new "$new_revision"
            git branch --set-upstream-to=cc/old cc/new
            git switch cc/new
        )
        if [[ "$new_revision" != "$old_revision" ]]; then
            ln -s "$checkout_dir" "$destination_updates/$name"
        fi
    } >"$log_dir/$name.log" 2>&1
}
export -f fetch_package

main() {
    if [[ -e "$destination_updates" ]]; then
        # Recreate from scratch so that only current updates are included.
        rm -rf "$destination_updates"
    fi
    mkdir -p "$destination_all" "$destination_updates"
    export destination_all destination_updates

    cd ~/code/emacs.nix/
    if [[ "$(git status --porcelain flake.lock)" ]]; then
        git stash push --quiet --message "[auto-created] Emacs updates WIP" -- flake.lock
        old_inputs=$(get_input_nodes)
        git stash pop --quiet
    else
        old_inputs=$(get_input_nodes)
    fi

    new_inputs=$(get_input_nodes)
    export old_inputs new_inputs

    if [[ -e "$log_dir" ]]; then
        rm -r "$log_dir"
    fi
    mkdir "$log_dir"
    echo "Logging jobs to: $log_dir"

    input_names=$(jq_get '.[] | select(.key == "root") | .value.inputs | values[]' <<<"$new_inputs")
    # Hey parallel, your pretentious citation nag is really fucking annoying.
    echo "will cite" | parallel --citation >/dev/null 2>&1
    progress_log="$log_dir/.parallel-progress"
    # Halt immediately on error to make it easier to figure out what went wrong.
    parallel --progress --halt-on-error now,fail=1 fetch_package <<<"$input_names" 2>"$progress_log" &
    parallel_pid=$!
    while ps -p "$parallel_pid" >/dev/null; do
        echo ""
        tail -n 1 "$progress_log"
        echo "" # parallel progress doesn't have a trailing newline
        # Show current job processes because some repos (looking at you, nixpkgs) take so long that
        # I may otherwise think it's hanging.
        ps -f -o cmd= --ppid "$parallel_pid" || true
        sleep 1
    done
    if ! wait "$parallel_pid"; then
        {
            echo ""
            echo "Tail of $progress_log:"
            sed 's/\r/\n/g' "$progress_log" | tail -n 2 | sed 's/^/\t/g'
        } >&2
        die "Parallel execution failed, see: $log_dir"
    fi
    echo "Finished"
}

destination=~/nix-inputs
destination_all=$destination/all
destination_updates=$destination/updates
log_dir=/tmp/fetch-sources
export log_dir

do_fetch=""
if [[ "${1:-}" == "fetch" ]]; then
    do_fetch="true"
elif [[ "${1:-}" != "view" ]]; then
    die "Invalid argument. \$1 must be fetch or view"
fi
export do_fetch
main

read -r -d '' lisp_cmd <<EOF || true
(let* ((update-dirs '(("$destination_updates" . 1)))
       (magit-repository-directories update-dirs))
  (with-current-buffer (magit-list-repositories)
    (setq-local magit-repository-directories update-dirs)
    ;; Make sure correct repos are shown for uh... reasons. Not sure why but the let-binding above
    ;; doesn't seem to work consistently.
    (revert-buffer)))
EOF
emacsclient --eval "$lisp_cmd"
