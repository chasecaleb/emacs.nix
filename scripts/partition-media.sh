#!/usr/bin/env bash
# Wipe/partition HDD for media.
#
# WARNING: DANGEROUS! You probably don't want to run this. This was written as a script for the sake
# of documenting how I did this one-off bit of setup.

set -Eeuo pipefail
die() {
    echo -e "${BASH_SOURCE[0]}:${BASH_LINENO[0]}" ERROR: "$@" >&2
    exit 1
}
# shellcheck disable=2154
trap 's=$?; die "$BASH_COMMAND"; exit $s' ERR

confirm_or_die() {
    PROMPT=$1
    local confirm
    read -re -p "$PROMPT [y/N] " confirm
    if [[ "$confirm" != "y" && "$confirm" != "Y" ]]; then
        die "Cancelled."
    fi
}

get_disk() {
    local disk_list
    disk_list=$(lsblk --nodeps --paths --list --noheadings --sort size --output name,size \
        | grep -Ev "boot|rpmb|loop|sr0" \
        | tac)
    # Intentionally expanding list so that dialog interprets it as menu items.
    # shellcheck disable=2086
    disk=$(dialog --stdout --menu "Select media disk" 0 0 0 $disk_list)
    clear
    echo "=== lsblk output ==="
    lsblk --noheadings --paths
    echo "===================="
}

# Make OS aware of partition/filesystem changes to update links in /dev/disk/by-partlabel and
# /dev/disk/by-label if needed.
refresh_disk() {
    # Thanks to https://serverfault.com/a/943233 and comments on that answer.
    systemctl restart systemd-udevd.service systemd-udev-trigger.service systemd-udev-settle.service
}

format_disk() {
    # Keep in mind that GPT partition labels (/dev/disk/by-partlabel) and
    # filesystem labels (/dev/disk/by-label) are two different things.
    # https://wiki.archlinux.org/index.php/Persistent_block_device_naming
    sgdisk --zap-all "$disk"
    # 8300 is generic "Linux filesystem"
    sgdisk --clear --new=1:0:0 --typecode=1:8300 "--change-name=1:$partition_name" "$disk"
    refresh_disk

    wipefs --all --force "$partition_path"
    mkfs.ext4 -L media "$partition_path"
}

main() {
    if [[ "$UID" != 0 ]]; then
        die "Must run as root"
    fi

    get_disk
    confirm_or_die "WARNING: You will lose all data on $disk. Continue?"
    confirm_or_die "Are you sure?"
    format_disk

    # See "fileSystems" Nix config, which this MUST match.
    mkdir /mnt/media
    mount /mnt/media
}

# Partition name/path needs to match Nix config for fileSystems.
partition_name=media
partition_path=/dev/disk/by-partlabel/$partition_name

main "$@"
