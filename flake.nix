{
  description = "Emacs nix configuration";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    homeManager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixIndexDatabase = {
      url = "github:Mic92/nix-index-database";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixosHardware = {
      url = "github:NixOS/nixos-hardware";
    };
    sopsNix = {
      url = "github:Mic92/sops-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    pandoc-goodies.flake = false;
    pandoc-goodies.url = "github:tajmone/pandoc-goodies";

    # Everything below here: lisp packages (format then sort alphabetically via M-x sort-lines).
    ace-window.flake = false;
    ace-window.url = "github:abo-abo/ace-window";
    alert.flake = false;
    alert.url = "github:jwiegley/alert";
    all-the-icons.flake = false;
    all-the-icons.url = "github:domtronn/all-the-icons.el";
    async.flake = false;
    async.url = "github:jwiegley/emacs-async";
    async-await.flake = false;
    async-await.url = "github:chuntaro/emacs-async-await";
    atomic-chrome.flake = false;
    atomic-chrome.url = "github:alpha22jp/atomic-chrome";
    avy.flake = false;
    avy.url = "github:abo-abo/avy";
    buttercup.flake = false;
    buttercup.url = "github:jorgenschaefer/emacs-buttercup";
    closql.flake = false;
    closql.url = "github:emacscollective/closql";
    company-mode.flake = false;
    company-mode.url = "github:company-mode/company-mode";
    compat.flake = false;
    compat.url = "github:phikal/compat.el";
    consult.flake = false;
    consult.url = "github:minad/consult/1.9";
    consult-yasnippet.flake = false;
    consult-yasnippet.url = "github:mohkale/consult-yasnippet";
    dash.flake = false;
    dash.url = "github:magnars/dash.el";
    deferred.flake = false;
    deferred.url = "github:kiwanami/emacs-deferred";
    diff-hl.flake = false;
    diff-hl.url = "github:dgutov/diff-hl";
    diminish.flake = false;
    diminish.url = "github:myrjola/diminish.el";
    doct.flake = false;
    doct.url = "github:progfolio/doct";
    dumb-jump.flake = false;
    dumb-jump.url = "github:jacktasia/dumb-jump";
    edit-indirect.flake = false;
    edit-indirect.url = "github:Fanael/edit-indirect"; # Used by markdown-mode for source blocks.
    emacs-sqlite-api.flake = false;
    emacs-sqlite-api.url = "github:pekingduck/emacs-sqlite3-api";
    emacsql.flake = false;
    emacsql.url = "github:skeeto/emacsql";
    embark.flake = false;
    embark.url = "github:oantolin/embark";
    epl.flake = false;
    epl.url = "github:cask/epl";
    exwm-edit.flake = false;
    exwm-edit.url = "github:agzam/exwm-edit";
    exwm.flake = false;
    exwm.url = "github:emacs-exwm/exwm";
    f.flake = false;
    f.url = "github:rejeep/f.el";
    forge.flake = false;
    forge.url = "github:magit/forge";
    gcmh.flake = false;
    gcmh.url = "gitlab:koral/gcmh";
    ghub.flake = false;
    ghub.url = "github:magit/ghub";
    git-link.flake = false;
    git-link.url = "github:sshaw/git-link";
    git-modes.flake = false;
    git-modes.url = "github:magit/git-modes";
    git-timemachine.flake = false;
    git-timemachine.url = "git+https://codeberg.org/pidu/git-timemachine.git";
    go-mode.flake = false;
    go-mode.url = "github:dominikh/go-mode.el";
    hcl-mode.flake = false;
    hcl-mode.url = "github:purcell/emacs-hcl-mode";
    hl-prog-extra.flake = false;
    hl-prog-extra.url = "git+https://codeberg.org/ideasman42/emacs-hl-prog-extra.git";
    hl-todo.flake = false;
    hl-todo.url = "github:tarsius/hl-todo";
    ht.flake = false;
    ht.url = "github:Wilfred/ht.el";
    hydra.flake = false;
    hydra.url = "github:abo-abo/hydra";
    iter2.flake = false;
    iter2.url = "github:doublep/iter2";
    kotlin-mode.flake = false;
    kotlin-mode.url = "github:Emacs-Kotlin-Mode-Maintainers/kotlin-mode";
    kubel.flake = false;
    kubel.url = "github:abrochard/kubel";
    lab.flake = false;
    lab.url = "github:isamert/lab.el";
    llama.flake = false;
    llama.url = "github:tarsius/llama";
    magit-delta.flake = false;
    magit-delta.url = "github:dandavison/magit-delta";
    magit.flake = false;
    magit.url = "github:magit/magit";
    marginalia.flake = false;
    marginalia.url = "github:minad/marginalia";
    markdown-mode.flake = false;
    markdown-mode.url = "github:jrblevin/markdown-mode";
    memoize.flake = false;
    memoize.url = "github:skeeto/emacs-memoize";
    mermaid-mode.flake = false;
    mermaid-mode.url = "github:abrochard/mermaid-mode";
    nginx-mode.flake = false;
    nginx-mode.url = "github:ajc/nginx-mode";
    nix-mode.flake = false;
    nix-mode.url = "github:nixos/nix-mode";
    ob-async.flake = false;
    ob-async.url = "github:astahlman/ob-async";
    ob-http.flake = false;
    ob-http.url = "github:zweifisch/ob-http";
    ob-kubectl.flake = false;
    ob-kubectl.url = "github:ifitzpat/ob-kubectl";
    orderless.flake = false;
    orderless.url = "github:oantolin/orderless";
    org-contrib.flake = false;
    org-contrib.url = "git+https://git.sr.ht/~bzg/org-contrib";
    org-noter.flake = false;
    org-noter.url = "github:weirdNox/org-noter";
    org-pdftools.flake = false;
    org-pdftools.url = "github:fuxialexander/org-pdftools";
    org-roam.flake = false;
    org-roam.url = "github:jethrokuan/org-roam";
    org-roam-ui.flake = false;
    org-roam-ui.url = "github:org-roam/org-roam-ui";
    org-super-agenda.flake = false;
    org-super-agenda.url = "github:alphapapa/org-super-agenda";
    org-ql.flake = false;
    org-ql.url = "github:alphapapa/org-ql";
    org.flake = false;
    org.url = "git+https://git.savannah.gnu.org/git/emacs/org-mode?ref=bugfix";
    orgit.flake = false;
    orgit.url = "github:magit/orgit";
    ov.flake = false;
    ov.url = "github:emacsorphanage/ov";
    page-break-lines.flake = false;
    page-break-lines.url = "github:purcell/page-break-lines";
    pdf-tools.flake = false;
    pdf-tools.url = "github:vedang/pdf-tools";
    phscroll.flake = false;
    phscroll.url = "github:misohena/phscroll";
    pkg-info.flake = false;
    pkg-info.url = "github:emacsorphanage/pkg-info";
    popup-el.flake = false;
    popup-el.url = "github:auto-complete/popup-el";
    promise.flake = false;
    promise.url = "github:chuntaro/emacs-promise";
    pulsar.flake = false;
    pulsar.url = "gitlab:protesilaos/pulsar";
    rainbow-delimiters.flake = false;
    rainbow-delimiters.url = "github:Fanael/rainbow-delimiters";
    reformatter.flake = false;
    reformatter.url = "github:purcell/emacs-reformatter";
    request.flake = false;
    request.url = "github:tkf/emacs-request";
    s.flake = false;
    s.url = "github:magnars/s.el";
    shfmt.flake = false;
    shfmt.url = "github:purcell/emacs-shfmt";
    smartparens.flake = false;
    smartparens.url = "github:Fuco1/smartparens";
    spacemacs-theme.flake = false;
    spacemacs-theme.url = "github:nashamri/spacemacs-theme";
    spell-fu.flake = false;
    spell-fu.url = "git+https://codeberg.org/ideasman42/emacs-spell-fu.git";
    spinner.flake = false;
    spinner.url = "github:Malabarba/spinner.el";
    svg-lib.flake = false;
    svg-lib.url = "github:rougier/svg-lib";
    symbol-overlay.flake = false;
    symbol-overlay.url = "github:wolray/symbol-overlay";
    tablist.flake = false;
    tablist.url = "github:politza/tablist";
    terraform-mode.flake = false;
    terraform-mode.url = "github:emacsorphanage/terraform-mode";
    todoist.flake = false;
    todoist.url = "github:abrochard/emacs-todoist";
    transient.flake = false;
    transient.url = "github:magit/transient";
    transpose-frame.flake = false;
    transpose-frame.url = "github:emacsorphanage/transpose-frame";
    treepy.flake = false;
    treepy.url = "github:volrath/treepy.el";
    ts.flake = false;
    ts.url = "github:alphapapa/ts.el";
    typescript.flake = false;
    typescript.url = "github:emacs-typescript/typescript.el";
    use-package.flake = false;
    use-package.url = "github:jwiegley/use-package";
    vertico.flake = false;
    vertico.url = "github:minad/vertico";
    visual-regexp.flake = false;
    visual-regexp.url = "github:benma/visual-regexp.el";
    visual-fill-column.flake = false;
    visual-fill-column.url = "git+https://codeberg.org/joostkremers/visual-fill-column?ref=main";
    vterm.flake = false;
    vterm.url = "github:akermu/emacs-libvterm";
    web-mode.flake = false;
    web-mode.url = "github:fxbois/web-mode";
    web-server.flake = false;
    web-server.url = "github:skeeto/emacs-web-server";
    websocket.flake = false;
    websocket.url = "github:ahyatt/emacs-websocket";
    wgrep.flake = false;
    wgrep.url = "github:mhayashi1120/Emacs-wgrep";
    with-editor.flake = false;
    with-editor.url = "github:magit/with-editor";
    xelb.flake = false;
    xelb.url = "github:emacs-exwm/xelb";
    xterm-color.flake = false;
    xterm-color.url = "github:atomontage/xterm-color";
    yaml-mode.flake = false;
    yaml-mode.url = "github:yoshiki/yaml-mode";
    yaml.flake = false;
    yaml.url = "github:zkry/yaml.el";
    yasnippet.flake = false;
    yasnippet.url = "github:joaotavora/yasnippet";
  };

  outputs = { self, nixpkgs, nixIndexDatabase, homeManager, nixosHardware, sopsNix, pandoc-goodies, ... }@inputs:
    let
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages.${system};
      emacs = pkgs.emacs30;
      libraries = import ./emacs/nix/libraries.nix { inherit pkgs emacs inputs; };
      init = import ./emacs/nix/init.nix { inherit pkgs emacs libraries; };
      emacsPackages = libraries // init;
      # buildEnv instead of symlinkJoin, because buildEnv links directories instead of individual
      # files (when possible) and has collision detection and... other things that are probably
      # irrelevant. Either one would work, but I expect buildEnv to perform better with Emacs
      # `load-path` (in theory, untested - could use strace to verify).
      lisp = pkgs.buildEnv {
        name = "lisp-all-merged";
        paths = builtins.attrValues emacsPackages;
      };
    in
    rec {
      packages.${system} = emacsPackages // {
        inherit lisp;
        default = lisp;
        emacs-wrapped = pkgs.symlinkJoin {
          name = "emacs-wrapped";
          paths =
            let
              # Burn built-in org files in a fire to ensure they aren't accidentally loaded. This is
              # *not* used for compiling lisp libraries/init because it's a path string, not a
              # derivation. It used to work but no longer does and I'm not sure why, but this works
              # so... cool?
              emacsNoOrg = builtins.path {
                path = emacs;
                name = "emacs-no-org";
                filter = (path: type:
                  !(pkgs.lib.hasInfix "share/emacs/${emacs.version}/lisp/org" path));
              };
            in
            [ emacsNoOrg ];
          buildInputs = [ pkgs.makeWrapper ];
          postBuild = ''
            wrapProgram $out/bin/emacs \
              --add-flags "--init-directory=${lisp}"
          '';
        };

        scripts = pkgs.stdenv.mkDerivation {
          name = "scripts";
          src = ./nixos-config/packaged-scripts;
          phases = "installPhase fixupPhase";
          installPhase = ''
            mkdir "$out"
            cp -R --no-target-directory "$src" "$out/bin"
            substituteInPlace $out/bin/cc-markdown \
              --subst-var-by nixPandocTemplate "${pandoc-goodies}/templates/html5/github/GitHub.html5"
          '';
        };
        # https://nixos.wiki/wiki/TexLive - LaTeX packages for general Emacs org-mode export use
        # plus some others, e.g. for my resume.
        tex = (pkgs.texlive.combine {
          inherit (pkgs.texlive) scheme-medium capt-of enumitem hyperref hyphenat titling titlesec wrapfig;
        });
      };

      apps.${system} = rec {
        # For testing convenience: "nix run" or "nix run .# -- <emacs args>".
        default = emacs;
        emacs = {
          type = "app";
          program = "${packages.${system}.emacs-wrapped}/bin/emacs";
        };
      };

      nixosConfigurations =
        let
          nixosConfigFor = { name, extraModules }: {
            ${name} = nixpkgs.lib.nixosSystem
              {
                system = "x86_64-linux";
                modules = extraModules ++ [
                  ./nixos-config/nixos/options.nix
                  ./nixos-config/nixos/nix.nix
                  ./nixos-config/nixos/base.nix
                  ./nixos-config/nixos/media.nix
                  ./nixos-config/nixos/networking.nix
                  ./nixos-config/nixos/docker-networks.nix
                  ./nixos-config/nixos/x11.nix
                  ./nixos-config/nixos/kubernetes.nix
                  homeManager.nixosModules.home-manager
                  sopsNix.nixosModules.sops
                  ({ config, ... }: {
                    networking.hostName = name;
                    home-manager = {
                      # Gotta love the pkgs vs packages inconsistency.
                      useGlobalPkgs = true;
                      useUserPackages = true;
                      users.${config.chasecaleb.username} = import ./nixos-config/home/home.nix;
                      extraSpecialArgs.nixIndexDb = nixIndexDatabase.hmModules.nix-index;
                    };
                  })
                ];
                specialArgs.myPkgs = packages.${system};
                specialArgs.myEmacsPkgs = emacsPackages;
                specialArgs.nixpkgs = nixpkgs;
              };
          };
        in
        (nixosConfigFor {
          name = "test";
          extraModules = [ ./nixos-config/nixos/test-vm.nix ];
        }) // (nixosConfigFor {
          name = "desktop";
          extraModules = [
            ./nixos-config/nixos/vmware.nix
            {
              chasecaleb.media.enable = true;
              # Make sure to configure VMware network adapter in bridge mode, not NAT.
              # IPv6 is still enabled even without DHCP thanks to IPv6 Router Advertisement, see man
              # systemd.network, particularly IPV6AcceptRA which (usually) defaults to yes.
              chasecaleb.networking.config = {
                DHCP = "no";
                address = [ "192.168.0.51/24" ];
                gateway = [ "192.168.0.1" ];
              };
            }
          ];
        }) // (nixosConfigFor {
          name = "thinkpad";
          extraModules = [
            nixosHardware.nixosModules.lenovo-thinkpad-t450s
            ./nixos-config/nixos/thinkpad.nix
          ];
        });

      devShells.${system}.default = pkgs.mkShell {
        packages = [
          pkgs.age
          pkgs.bashInteractive
          pkgs.coreutils-full
          pkgs.dialog
          pkgs.git
          pkgs.gptfdisk # Provides sgdisk for partition-media.sh
          pkgs.jq
          pkgs.openssh
          pkgs.sops
        ];
      };

      checks.${system} = {
        shellcheck = pkgs.runCommand "check-shellcheck" { } ''
          cd ${self}
          # Script files in packaged-scripts don't have an extension, so match all of them.
          find . -type f \( -name '*.sh' -o -path './nixos-config/packaged-scripts/**' \) \
              -exec ${pkgs.shellcheck}/bin/shellcheck {} \+ 2>&1 | tee $out
        '';
        shfmt = pkgs.runCommand "check-shfmt" { } ''
          cd ${self}
          # shfmt config is in .editorconfig
          # Exclude zsh files because shfmt is weird about zsh... it doesn't support zsh-isms, but
          # it *does* still attempt to check zsh files: https://github.com/mvdan/sh/issues/535
          ${pkgs.shfmt}/bin/shfmt --find . \
              | grep -v -E '/(zshrc-extra|zshenv-extra)$' \
              | xargs ${pkgs.shfmt}/bin/shfmt --diff 2>&1 \
              | tee $out
        '';
      };

      # Usage: nix fmt
      formatter.${system} = pkgs.nixpkgs-fmt;
    };
}
