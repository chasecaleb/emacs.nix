{ config, lib, ... }:
let
  cache = "/mnt/media/cache";
  data = "/mnt/media/data";
  downloads = "/mnt/media/downloads";
  usenet = "${downloads}/usenet";
  library = "${downloads}/library";
  # In theory I would make a separate user for each service, but that's massively overkill.
  uid = 954;
  group = "nogroup";
  gid = config.users.groups.${group}.gid;
  readImage = file: (builtins.fromJSON (builtins.readFile file)).locked;
in
{
  options.chasecaleb.media = {
    enable = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = "Enable media server module";
    };
  };

  # TODO: Set up HTTPS certs (chasecaleb.com subdomains)
  # TODO: Maybe set up remote access (e.g. TailScale VPN). Do HTTPS certs first though.

  # Some helpful resources:
  # https://wiki.servarr.com/docker-guide and all of https://wiki.servarr.com/
  # https://trash-guides.info/ has lots of guides/tips for all the things "media" related.
  config = lib.mkIf config.chasecaleb.media.enable {
    fileSystems."/mnt/media" = {
      device = "/dev/disk/by-label/media";
      fsType = "ext4";
      # Not using "defaults" for options because I don't want exec or suid enabled for security
      # since there shouldn't be any executables on here. See man mount(8) for "defaults" list and
      # info on options.
      #
      # NOTE: It might be good to add "nofail" for resiliency, but I would rather find out right
      # away if something happens with the drive instead of filling up the / (root) partition.
      options = [ "async" "auto" "noatime" "rw" ];
    };

    # These aren't really temporary, I just want to manage them declaratively.
    systemd.tmpfiles.rules = [
      # Services
      "d ${cache}/jellyfin - ${builtins.toString uid} ${builtins.toString gid}"
      "d ${data}/jellyfin - ${builtins.toString uid} ${builtins.toString gid}"
      "d ${data}/jellyseer - ${builtins.toString uid} ${builtins.toString gid}"
      "d ${data}/prowlarr - ${builtins.toString uid} ${builtins.toString gid}"
      "d ${data}/sabnzbd - ${builtins.toString uid} ${builtins.toString gid}"
      "d ${data}/sonarr - ${builtins.toString uid} ${builtins.toString gid}"
      "d ${data}/whisparr - ${builtins.toString uid} ${builtins.toString gid}"

      # Media downloads
      "d ${usenet}/complete - ${builtins.toString uid} ${builtins.toString gid}"
      "d ${usenet}/incomplete - ${builtins.toString uid} ${builtins.toString gid}"
      "d ${library}/movies - ${builtins.toString uid} ${builtins.toString gid}"
      "d ${library}/tv - ${builtins.toString uid} ${builtins.toString gid}"
      "d ${library}/other - ${builtins.toString uid} ${builtins.toString gid}"
    ];

    users.users.media = {
      uid = uid;
      group = group;
      isSystemUser = true;
    };

    chasecaleb.docker.networks.media = {
      subnet = "172.111.0.0/24";
      gateway = "172.111.0.1";
      usedBy = [
        "media-jellyfin"
        "media-prowlarr"
        "media-sonarr"
      ];
    };

    virtualisation.oci-containers.backend = "docker";

    # Super lightweight landing page with links to services.
    virtualisation.oci-containers.containers.media-home = {
      image = readImage ../docker-images/nginx.json;
      extraOptions = [
        "--network=media"
        "--hostname=home"
      ];
      ports = [
        "80:80"
      ];
      volumes = [
        "${./media-index.html}:/usr/share/nginx/html/index.html"
      ];
    };

    # https://jellyfin.org/docs/general/installation/container
    # https://jellyfin.org/docs/general/administration/configuration
    virtualisation.oci-containers.containers.media-jellyfin = {
      image = readImage ../docker-images/jellyfin.json;
      # By default, hostname would change every time container is recreated which confuses
      # Jellyfin's web client.
      extraOptions = [
        "--network=media"
        "--hostname=jellyfin"
      ];
      environment = {
        TZ = "US/Central";
      };
      user = "${builtins.toString uid}:${builtins.toString gid}";
      # https://jellyfin.org/docs/general/networking/
      ports = [
        "8096:8096" # HTTP
        # "8920" # HTTPS (unused because this is only on LAN and I don't want to deal with certs).
        "1900:1900/udp" # Service discovery
        "7359:7359/udp" # Service discovery
      ];
      volumes = [
        "${cache}/jellyfin:/cache"
        "${data}/jellyfin:/config"
        "${library}:/downloads/library"
      ];
    };

    # https://github.com/Fallenbagel/jellyseerr
    virtualisation.oci-containers.containers.media-jellyseer = {
      image = readImage ../docker-images/jellyseer.json;
      extraOptions = [
        "--network=media"
        "--hostname=jellyseer"
      ];
      environment = {
        TZ = "US/Central";
      };
      user = "${builtins.toString uid}:${builtins.toString gid}";
      # https://jellyfin.org/docs/general/networking/
      ports = [
        "5055:5055" # HTTP
      ];
      volumes = [
        "${data}/jellyseer:/app/config"
      ];
    };

    # Image: https://docs.linuxserver.io/images/docker-prowlarr
    # Program: https://wiki.servarr.com/prowlarr
    virtualisation.oci-containers.containers.media-prowlarr = {
      image = readImage ../docker-images/prowlarr.json;
      extraOptions = [
        "--network=media"
        "--hostname=prowlarr"
      ];
      # Apparently linuxserver's images use PUID and PGID instead of the "recent" docker run --user
      # (spoiler alert: it's at least 5 years old, but oh well).
      # https://docs.linuxserver.io/general/understanding-puid-and-pgid
      environment = {
        PUID = builtins.toString uid;
        PGID = builtins.toString gid;
        TZ = "US/Central";
      };
      ports = [ "9696:9696" ]; # Web UI (HTTP)
      volumes = [
        "${data}/prowlarr:/config"
      ];
    };

    # Image: https://docs.linuxserver.io/images/docker-sonarr
    # Program: https://wiki.servarr.com/sonarr
    #
    # Make sure to keep the caveats about volume in mind from here:
    # https://wiki.servarr.com/sonarr/installation#avoid-common-pitfalls
    #   1. Hardlinking: download and library directories should be on the same volume mount
    #   2. Paths: make sure Sonarr and Usenet download client have library mounted to the same in
    #      their containers to avoid prevent them getting confused. Otherwise add a "remote path
    #      mapping" to Sonarr's download client config.
    #
    # Media renaming formats: see
    # https://wiki.servarr.com/sonarr/settings#community-naming-suggestions and
    # https://trash-guides.info/Sonarr/Sonarr-recommended-naming-scheme/ but without episode title
    # in filename since it can delay downloads -- Jellyfin can look it up from IMDB without needing
    # it in the filename. Also episode names can be extremely long. Finally, spaces in filenames are
    # gross but I don't care enough to change it.
    #
    # - Standard episodes: {Series TitleYear} - S{season:00}E{episode:00} [{Preferred Words }{Quality Full}]{[MediaInfo VideoDynamicRangeType]}{[Mediainfo AudioCodec}{ Mediainfo AudioChannels]}{[MediaInfo VideoCodec]}{-Release Group}
    # - Daily episodes {Series TitleYear} - {Air-Date} [{Preferred Words }{Quality Full}]{[MediaInfo VideoDynamicRangeType]}{[Mediainfo AudioCodec}{ Mediainfo AudioChannels]}{[MediaInfo VideoCodec]}{-Release Group}
    # - Anime episodes: {Series TitleYear} - S{season:00}E{episode:00} - {absolute:000} [{Preferred Words }{Quality Full}]{[MediaInfo VideoDynamicRangeType]}[{MediaInfo VideoBitDepth}bit]{[MediaInfo VideoCodec]}[{Mediainfo AudioCodec} { Mediainfo AudioChannels}]{MediaInfo AudioLanguages}{-Release Group}
    # - Series folder: {Series TitleYear} {imdb-{ImdbId}}
    # - Season folder: Season {season:00}
    # - Multi-episode style: scene
    virtualisation.oci-containers.containers.media-sonarr = {
      image = readImage ../docker-images/sonarr.json;
      extraOptions = [
        "--network=media"
        "--hostname=sonarr"
      ];
      # Apparently linuxserver's images use PUID and PGID instead of the "recent" docker run --user
      # (spoiler alert: it's at least 5 years old, but oh well).
      # https://docs.linuxserver.io/general/understanding-puid-and-pgid
      environment = {
        PUID = builtins.toString uid;
        PGID = builtins.toString gid;
        TZ = "US/Central";
      };
      ports = [ "8989:8989" ]; # Web UI (HTTP)
      volumes = [
        "${data}/sonarr:/config"
        # /downloads/usenet and /downloads/library on same volume mount for hardlinking.
        "${downloads}:/downloads"
      ];
    };

    virtualisation.oci-containers.containers.media-whisparr = {
      image = readImage ../docker-images/whisparr.json;
      extraOptions = [
        "--network=media"
        "--hostname=whisparr"
      ];
      environment = {
        PUID = builtins.toString uid;
        PGID = builtins.toString gid;
        TZ = "US/Central";
      };
      ports = [ "6969:6969" ]; # Web UI (HTTP)
      volumes = [
        "${data}/whisparr:/config"
        # /downloads/usenet and /downloads/library on same volume mount for hardlinking.
        "${downloads}:/downloads"
      ];
    };

    # https://trash-guides.info/Downloaders/SABnzbd/Basic-Setup/
    # https://sabnzbd.org/wiki/configuration/4.0/general (see sidebar for config sections)
    virtualisation.oci-containers.containers.media-sabnzbd = {
      image = readImage ../docker-images/sabnzbd.json;
      extraOptions = [
        "--network=media"
        "--hostname=sabnzbd"
      ];
      # Apparently linuxserver's images use PUID and PGID instead of the "recent" docker run --user
      # (spoiler alert: it's at least 5 years old, but oh well).
      # https://docs.linuxserver.io/general/understanding-puid-and-pgid
      environment = {
        PUID = builtins.toString uid;
        PGID = builtins.toString gid;
        TZ = "US/Central";
      };
      ports = [ "8181:8080" ]; # Web UI (HTTP). Not 8080 on host to avoid dev tools conflicts.
      volumes = [
        "${data}/sabnzbd:/config"
        "${usenet}:/downloads/usenet"
      ];
    };
  };
}
