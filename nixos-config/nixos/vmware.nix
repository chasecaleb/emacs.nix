{ config, lib, pkgs, ... }:
{
  virtualisation.vmware.guest.enable = true;

  # Bug fix: prevent NixOS from attempting to do "systemctl reload run-vmblock\\x2dfuse.mount"
  # during "nixos-rebuild switch" if the mount unit's definition changes (e.g. when doing
  # nixos-rebuild switch after a lock file update that results in a new version of open-vm-tools).
  # When the systemd mount unit is reloaded, systemd attempts to re-mount the mount with "-o
  # remount", which fuse doesn't support.
  #
  # If the mount unit file changes AND the unit is active, then NixOS will attempt to reload it,
  # fail, and then start it again to recover from failure. Thus this workaround: preemptively
  # stop the unit so that NixOS only needs to start it again without attempting/failing to reload.
  #
  # https://github.com/libfuse/libfuse/issues/717
  system.activationScripts.vmware-mount-reload-workaround = {
    deps = [ "etc" ];
    text = ''
      name=run-vmblock\\x2dfuse.mount
      path=etc/systemd/system/$name
      old_mount=/run/current-system/$path
      new_mount=$systemConfig/$path

      if ! ${pkgs.diffutils}/bin/diff "$old_mount" "$new_mount" >/dev/null 2>&1; then
        ${pkgs.systemd}/bin/systemctl stop "$name"
      fi
    '';
  };

  services.xserver = {
    videoDrivers = [ "vmware" ];
    modules = [ pkgs.xorg.xf86videovmware ];
  };
}
