{ config, lib, pkgs, ... }:
{
  environment.systemPackages =
    let
      kubeVersion = "1.25.10";
      kubectlOld = pkgs.kubectl.overrideAttrs (_: {
        version = kubeVersion;
        src = pkgs.fetchFromGitHub {
          owner = "kubernetes";
          repo = "kubernetes";
          rev = "v${kubeVersion}";
          sha256 = "sha256-kVbTOwRxp9ganWThSpk6NpNolTQNL4XEqS9Q5FGYiF4=";
        };
      });
    in
    [
      kubectlOld
      pkgs.k9s
      pkgs.kubectx
    ];

  virtualisation.docker = {
    enable = true;
  };
}
