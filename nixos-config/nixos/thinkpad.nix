{ pkgs, ... }:
{
  config = {
    sops.secrets.wpa_supplicant = { };
    networking.wireless = {
      enable = true;
      secretsFile = "/run/secrets/wpa_supplicant";
      networks.chasefiber = {
        # "ext:foo" refers to key "foo" in networking.wireless.secretsFile
        pskRaw = "ext:chasefiber_psk";
      };
    };
    # Light: for XF86 brightness keybinds in Emacs.
    environment.systemPackages = [ pkgs.light ];
  };
}
