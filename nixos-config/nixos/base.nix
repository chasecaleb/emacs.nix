{ config, lib, pkgs, myPkgs, myEmacsPkgs, ... }:
{
  boot = {
    initrd.availableKernelModules = [ "nvme" ];
    initrd.luks.devices.system.device = "/dev/disk/by-partlabel/cryptsystem";
    kernelPackages = pkgs.linuxPackages_latest;
    kernel.sysctl = {
      # sysctl keys have to be quoted so that nix doesn't treat them as nested attributes.
      "fs.inotify.max_user_watches" = 524288;
      "net.ipv4.ip_forward" = 1;
      "net.ipv6.conf.default.forwarding" = 1;
      "net.ipv6.conf.all.forwarding" = 1;
      "vm.swappiness" = 1;
      "vm.vfs_cache_pressure" = 50;
      "vm.dirty_background_ratio" = 5;
      "vm.dirty_ratio" = 10;
      "vm.min_free_kbytes" = 135168;
    };
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };
    tmp.useTmpfs = true;
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/system";
      fsType = "ext4";
    };
    "/boot" = {
      device = "/dev/disk/by-label/EFI";
      fsType = "vfat";
    };
  };

  hardware = {
    enableRedistributableFirmware = true;
    cpu.amd.updateMicrocode = true;
    cpu.intel.updateMicrocode = true;
  };

  # I use zram swap instead of a swap partition or file. Downside is that I can't use hibernation
  # (aka suspend-to-disk), but most of my machines have so much RAM that hibernation takes longer
  # than a restart so it isn't useful to me.
  zramSwap = {
    enable = true;
    algorithm = "zstd";
  };

  documentation.man = {
    # Support apropros and man -k, which I use for consult-man.
    # https://nixos.wiki/wiki/Apropos
    generateCaches = true;
    # Exclude my Emacs config, because it doesn't have any man pages and rebuilding cache on every
    # config change is slow.
    man-db.manualPages = pkgs.buildEnv {
      name = "man-paths";
      paths =
        let
          excluded = (builtins.attrValues myEmacsPkgs) ++ (builtins.attrValues myPkgs);
        in
        builtins.filter
          (p: !(builtins.elem p excluded))
          config.environment.systemPackages;
      pathsToLink = [ "/share/man" ];
      extraOutputsToInstall = [ "man" ]
        ++ lib.optionals config.documentation.dev.enable [ "devman" ];
      ignoreCollisions = true;
    };
  };

  environment = {
    # Make zsh completions work for system packages.
    pathsToLink = [ "/share/zsh" ];
    systemPackages = (builtins.attrValues myPkgs) ++ (with pkgs; [
      (aspellWithDicts (dicts: [ dicts.en dicts.en-computers dicts.en-science ]))
      awscli2
      dig
      dive
      element-desktop
      fd
      file
      git
      git-absorb # Used by magit.
      git-sync # Used to auto commit/push/pull some of my git repos (e.g. org-files).
      go
      gopls
      gping
      htop
      i3lock
      inotify-tools
      jdk
      jq
      kotlin-language-server
      less
      libnotify
      libreoffice
      lsof
      mtr
      ncdu
      nil # Nix LSP server: https://github.com/oxalica/nil/
      nix-diff
      nmap
      nixpkgs-fmt # In path for use with nil LSP server (nil.formatting.command).
      nodejs
      nodePackages.bash-language-server
      nodePackages.mermaid-cli # Needed for e.g. Emacs mermaid-mode.
      nodePackages.prettier
      nodePackages.typescript # Needed for lsp-mode (just the language server isn't enough).
      nodePackages.typescript-language-server
      nodePackages.vscode-langservers-extracted
      nodePackages.yaml-language-server
      openssl
      pandoc # For markdown-mode
      parallel
      pass
      pavucontrol
      ripgrep # For consult, embark, etc.
      scc # Lines-of-code counter (plus complexity calcuations and more)
      screenkey # Keyboard shortcut overlay for screen sharing.
      shellcheck
      shfmt
      sqlite # For emacsql
      ssm-session-manager-plugin # Session manager (SSO) for AWS CLI
      terraform
      terraform-ls
      traceroute
      unzip # Used by Emacs to view zip file contents.
      vlc
      virt-manager
      watchexec
      wget
      whois
      yq
    ]);
  };

  fonts = {
    # Defaults listed here (including some for unicode coverage):
    # https://github.com/NixOS/nixpkgs/blob/master/nixos/modules/config/fonts/fonts.nix
    enableDefaultPackages = true;
    # Ghostscript fonts include Acrobat Reader's "Base 14" (Courier, Helvetica, and Times Roman
    # familes) plus... some others. TLDR I think these are important for compatibility in certain
    # programs like PDF readers.
    enableGhostscriptFonts = true;
    packages = with pkgs; [
      hack-font
      material-icons # For polybar
      emacs-all-the-icons-fonts # Material Design, all-the-icons, GitHub octicons, and a few others.
    ];
    fontconfig.defaultFonts.monospace = [ "Hack" ];
  };

  # GitHub keys:
  # https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/githubs-ssh-key-fingerprints
  # GitLab doesn't publish theirs out of band, so run:
  # ssh-keyscan hostname 2>&1 | grep -v "^#" | cut -d ' ' -f2-
  programs.ssh.knownHosts = {
    gitlab-ed25519 = {
      hostNames = [ "gitlab.com" ];
      publicKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAfuCHKVTjquxvt6CM6tdG4SLp1Btn/nOeHHE5UOzRdf";
    };
    gitlab-rsa = {
      hostNames = [ "gitlab.com" ];
      publicKey = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCsj2bNKTBSpIYDEGk9KxsGh3mySTRgMtXL583qmBpzeQ+jqCMRgBqB98u3z++J1sKlXHWfM9dyhSevkMwSbhoR8XIq/U0tCNyokEi/ueaBMCvbcTHhO7FcwzY92WK4Yt0aGROY5qX2UKSeOvuP4D6TPqKF1onrSzH9bx9XUf2lEdWT/ia1NEKjunUqu1xOB/StKDHMoX4/OKyIzuS0q/T1zOATthvasJFoPrAjkohTyaDUz2LN5JoH839hViyEG82yB+MjcFV5MU3N1l1QL3cVUCh93xSaua1N85qivl+siMkPGbO5xR/En4iEY6K2XPASUEMaieWVNTRCtJ4S8H+9";
    };
    gitlab-ecdsa = {
      hostNames = [ "gitlab.com" ];
      publicKey = "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFSMqzJeV9rUzU4kWitGjeR4PWSa29SPqJ1fVkhtj3Hw9xjLVXVYrU9QlYWrOLXBpQ6KWjbjTDTdDkoohFzgbEY=";
    };
    github-ed25519 = {
      hostNames = [ "github.com" ];
      publicKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOMqqnkVzrm0SdG6UOoqKLsabgH5C9okWi0dh2l9GKJl";
    };
    github-rsa = {
      hostNames = [ "github.com" ];
      publicKey = "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAq2A7hRGmdnm9tUDbO9IDSwBK6TbQa+PXYPCPy6rbTrTtw7PHkccKrpp0yVhp5HdEIcKr6pLlVDBfOLX9QUsyCOV0wzfjIJNlGEYsdlLJizHhbn2mUjvSAHQqZETYP81eFzLQNnPHt4EVVUh7VfDESU84KezmD5QlWpXLmvU31/yMf+Se8xhHTvKSCZIFImWwoG6mbUoWf9nzpIoaSjB+weqqUUmpaaasXVal72J+UX2B+2RPW3RcT0eOzQgqlJL3RKrTJvdsjE3JEAvGq3lGHSZXy28G3skua2SmVi/w4yCE6gbODqnTWlg7+wC604ydGXA8VJiS5ap43JXiUFFAaQ==";
    };
    github-ecdsa = {
      hostNames = [ "github.com" ];
      publicKey = "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBEmKSENjQEezOmxkZMy7opKgwFB9nkt5YRrYMjNuG5N87uRgg6CLrbo5wAdT/y6v0mKV0U2w0WZ2YB/++Tpockg=";
    };
  };

  programs.wireshark = {
    enable = true;
    package = pkgs.wireshark; # I want the GUI, not just CLI.
  };
  # zsh.enable is necessary in system config for completions to work, even when also enabled in
  # home-manager.
  programs.zsh.enable = true;

  services.logind.extraConfig = ''
    HandleLidSwitch=ignore
    HandlePowerKey=ignore
    HandleSuspendKey=ignore
    HandleHibernateKey=ignore
    # Kill user processes (including systemd services) on logout.
    KillUserProcesses=yes
  '';

  security.sudo = {
    # Prevent non-wheel users from executing sudo for security.
    execWheelOnly = true;
    extraConfig = ''
      Defaults timestamp_timeout=30
      # passwd_timeout=0 migrated from my Arch Linux config... but I can't remember why I set it.
      # Seems like tramp had something to do with it.
      Defaults passwd_timeout=0
    '';
  };

  # Before changing stateVersion, check for breaking changes/manual intervention:
  # https://nixos.org/manual/nixos/unstable/release-notes.html
  system.stateVersion = "22.11";
  system.activationScripts.diff-changes = ''
    ${pkgs.bash}/bin/bash ${./activation-diff-changes.sh} \
      "$systemConfig" \
      "${pkgs.nvd}/bin:${pkgs.nix}/bin:${pkgs.ncurses}/bin"
  '';

  # Hey systemd-boot, reinventing the wheel with your own method of setting the default boot entry
  # (by pressing "d" when on the list) is dumb. There's already a default specified in
  # /boot/loader/loader.conf.
  # See: https://github.com/systemd/systemd/issues/15068
  #
  # Also: this is a systemd target, not NixOS system.activationScripts, because it only needs to run
  # on boot not on every configuration activation. NixOS manual also says to prefer a systemd
  # service like this when possible.
  # See: https://nixos.org/manual/nixos/unstable/index.html#sec-activation-script
  systemd.services.clear-systemd-boot-default = {
    description = "Clear systemd-boot's default entry";
    wantedBy = [ "multi-user.target" ];
    serviceConfig = {
      Type = "oneshot";
    };
    script = ''${pkgs.systemd}/bin/bootctl set-default ""'';
  };

  # Default stop timeout (e.g. on shutdown) of 90 seconds is ridiculously long, and lots of distros
  # are moving towards shortening it. I agree, since the only time my systems take more than a few
  # seconds is if something is completely FUBAR to the point where it will hit the timeout no matter
  # how long or short.
  systemd.extraConfig = ''DefaultTimeoutStopSec = 30'';
  systemd.user.extraConfig = ''DefaultTimeoutStopSec = 30'';

  time.timeZone = "US/Central";

  users = {
    mutableUsers = false;
    users.root = {
      # nix run nixpkgs#mkpasswd -- -m sha-512
      hashedPassword = "$6$MlxUZ8gB5PQltXbI$15F56Pr0g/u5QLH/FZQmKM/YjDRO.fNoOS4QrCyLYwmWams4OzPWCgdETz2C4Ya6xqa6yNrCrt6vG4mdm4UM71";
    };
    users.${config.chasecaleb.username} = {
      isNormalUser = true;
      home = "/home/${config.chasecaleb.username}";
      extraGroups = [ "libvirtd" "wheel" ];
      # nix run nixpkgs#mkpasswd -- -m sha-512
      hashedPassword = "$6$/2Nf.4hy9xd/lnlZ$PxZY53nuCKIRRIDhPUolY9GN/6WCwvaj9fv/8OiUxt9Hx0VbGtv66IEdEccYknWtTPXiPz7S2OcICbudZgZg1.";
      shell = pkgs.zsh;
      openssh.authorizedKeys.keyFiles = [ ../home/ssh/self.pub ];
    };
  };

  services.openssh = {
    enable = true;
    openFirewall = false; # Only use tailscale (configured elsewhere), not all interfaces.
    settings = {
      PasswordAuthentication = false;
    };
  };

  services.pcscd.enable = true;

  sops = {
    age.keyFile = "/var/lib/private/sops.key";
    defaultSopsFile = ../secrets/main.yaml;
    secrets = {
      "ssh/self" = {
        owner = config.chasecaleb.username;
      };
      "ssh/gitlab-chasecaleb" = {
        owner = config.chasecaleb.username;
      };
      "ssh/github" = {
        owner = config.chasecaleb.username;
      };
    } // (if config.chasecaleb.workFeatures then { } else {
      "ssh/tron" = {
        owner = config.chasecaleb.username;
      };
      "aws-config/personal" = {
        owner = config.chasecaleb.username;
        path = "/home/${config.chasecaleb.username}/.aws/config";
      };
      "nix-conf-secrets/personal" = {
        # Static path so that nix.conf can include it without logic to derive the path.
        path = "/etc/nix-conf-secrets";
        # World-visible so that nix can read it, since it's included in nix.conf. The tokens in
        # here are restricted to read-only repositories so at least the risk is minimized.
        # Besides, the important part is that it's encrypted in the git repo, not necessarily
        # encrypted on the installed system.
        mode = "0444";
      };
    });
  };

  systemd.tmpfiles.rules = [
    # So that I don't have to add "--flake foo" to nixos-rebuild
    "L+ /etc/nixos/flake.nix - - - - /home/caleb/code/emacs.nix/flake.nix"
  ] ++ (if config.chasecaleb.workFeatures then [
    # Feature flag as a file
    "f /etc/chasecaleb-work - - - - -"
  ] else [ ]);
}
