#!/usr/bin/env bash
# NixOS activation script to show diff of changes.

set -Eeuo pipefail
die() {
    echo -e "${BASH_SOURCE[0]}:${BASH_LINENO[0]}" ERROR: "$@" >&2
    exit 1
}
# shellcheck disable=2154
trap 's=$?; die "$BASH_COMMAND"; exit $s' ERR

previous_system=$1
# NixOS activation scripts run with a minimal $PATH, so need to add nvd, nix, etc.
PATH=$PATH:$2

if [[ -e /run/current-system ]]; then
    color_green=$(tput bold setaf 2)
    color_reset=$(tput sgr0)
    echo -e "$color_green***** Version changes diff: *****$color_reset"
    nvd diff /run/current-system "$previous_system"
    echo -e "$color_green***** End of changes diff *****$color_reset"
fi
