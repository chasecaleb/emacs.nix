{ config, ... }:
{
  services = {
    displayManager.autoLogin = {
      enable = true;
      user = config.chasecaleb.username;
    };
    xserver = {
      enable = true;
      displayManager.session = [{
        manage = "window";
        name = "emacs";
        start = ''
          xsetroot -cursor_name left_ptr
          export ENABLE_EXWM=true
          # Use emacs from $PATH so that the current wrapper with --init-directory is used when
          # logging out and back in, since changes to this file only take effect on (X11) restart,
          # not user session logout
          emacs &
          waitPID=$!
        '';
      }];
      serverFlagsSection = ''
        Option "BlankTime"   "0"
        Option "StandbyTime" "0"
        Option "SuspendTime" "0"
        Option "OffTime"     "0"
      '';
      # Use caps lock as control because I don't want Emacs pinky.
      xkb.options = "ctrl:nocaps";
    };
    xbanish = {
      enable = true;
      arguments = "-m nw";
    };
  };
}
