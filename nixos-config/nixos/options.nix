{ lib, ... }:
{
  # Cross-cutting/general options (e.g. feature flags) go here.
  options.chasecaleb = {
    workFeatures = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = "Feature flag for work things";
    };
    # I can't think of a reason why I would ever want or need to change my main account username,
    # but it's nice having it as a value for the sake of tracking where it's used and so on.
    username = lib.mkOption {
      type = lib.types.str;
      default = "caleb";
      description = "Primary account username";
    };
  };
}
