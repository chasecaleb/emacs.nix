# Custom Docker network.
{ config, lib, pkgs, ... }:
{
  options.chasecaleb.docker.networks = lib.mkOption {
    description = ''
      Custom Docker networks.

      Key is used for network name. Make sure to add --network <name> to each container (and
      probably --hostname <container-name> for DNS as well).
    '';
    default = { };
    type = lib.types.attrsOf (lib.types.submodule {
      options = {
        subnet = lib.mkOption {
          type = lib.types.str;
          description = lib.mdDoc ''
            Sets --subnet for "docker network create".

            This must match the gateway.
          '';
        };
        gateway = lib.mkOption {
          type = lib.types.str;
          description = lib.mdDoc ''
            Sets --gateway for "docker network create".

            This must match the subnet.
          '';
        };
        usedBy = lib.mkOption {
          type = lib.types.listOf lib.types.str;
          default = [ ];
          description = lib.mdDoc ''
            Names of containers from virtualisation.oci-containers using this network.

            Values should be the containers names, as in
            virtualisation.oci-containers.container.<name>, not the corresponding systemd service
            names. This is REQUIRED for containers to be able to use this network, because the
            Docker network must be created before the container is created and cannot be deleted
            until after the container is deleted.
          '';
        };
      };
    });
  };

  config = {
    # NixOS has virtualisation.oci-containers.containers, but no equivalent for Docker networks.
    # Fortunately it's easy to write my own service to handle this.
    # https://github.com/nixos/nixpkgs/blob/master/nixos/modules/virtualisation/oci-containers.nix
    systemd.services = lib.concatMapAttrs
      (name: cfg:
        let
          dependencies = [ "docker.service" "docker.socket" ];
          dependentContainers = builtins.map
            (it: "docker-${it}.service")
            cfg.usedBy;
        in
        {
          "docker-network-${name}" = {
            # wantedBy multi-user.target automatically starts this service on system boot.
            wantedBy = [ "multi-user.target" ];
            after = dependencies;
            requires = dependencies;

            # Network has to be created before the container is created and cannot be deleted until
            # containers are deleted, which can be solved with proper systemd service dependency
            # ordering. See man 5 systemd.unit.
            before = dependentContainers;
            requiredBy = dependentContainers;

            # Inline scripts in .nix files is generally gross, but it's short and filled with Nix
            # variable interpolation.
            script = ''
              set -Eeuo pipefail
              # Make sure network doesn't somehow exist already.
              echo "Ensuring network does not exist already"
              ${pkgs.docker}/bin/docker network rm ${name} || true
              echo "Creating network"
              ${pkgs.docker}/bin/docker network create ${name} \
                  --opt "com.docker.network.bridge.name=${name}" \
                  --driver bridge \
                  --subnet "${cfg.subnet}" \
                  --gateway "${cfg.gateway}"
            '';
            preStop = ''
              set -Eeuo pipefail
              ${pkgs.docker}/bin/docker network rm ${name}
            '';
            serviceConfig = {
              Type = "oneshot";
              RemainAfterExit = "true";
            };
          };
        }
      )
      config.chasecaleb.docker.networks;
  };
}
