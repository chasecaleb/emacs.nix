# Config for Nix/NixOS/Nixpkgs itself.
{ lib, pkgs, nixpkgs, ... }:
let
  nixpkgsPath = "/etc/nixpkgs-channel";
in
{
  config = {
    nix = {
      # Settings is for nix.conf. See man nix.conf.
      settings = {
        experimental-features = [ "nix-command" "flakes" "cgroups" ];
        # Default of 25 connections is a bit low when you have gigabit fiber.
        http-connections = 100;
        # I know how to use git without nagging, thank you very much.
        warn-dirty = false;
        # Default shows only last 10 lines of output on failure, which is too short.
        log-lines = 20;
        # Enable cgroups for systemd resource control of nix-daemon.service.
        use-cgroups = true;
      };
      # extraOptions because nix.settings doesn't have an "include" key.
      extraOptions = ''
        # Exclamation prevents error if the include file does not exist (e.g. on first build or if
        # secrets haven't been decrypted yet).
        !include /etc/nix-conf-secrets
      '';

      # Automatically hardlink identical files.
      optimise.automatic = true;
      # Garbage collection on timer, because I don't have infinite storage.
      gc.automatic = true;
      gc.options = "--delete-older-than 60d";

      # Make registry-based commands (e.g. nix run nixpkgs#foo) use my flake's system nixpkgs, instead
      # of looking up the latest nixpkgs revision from GitHub.
      registry.nixpkgs.flake = nixpkgs;

      # Make angle bracket references (e.g. nix repl '<nixpkgs>') use my flake's nixpkgs, instead of
      # whatever the imperatively managed version is. One day flakes will completely kill off
      # channels... one day.
      #
      # NOTE: My bash or zsh profiles don't seem to use NixOS's command-not-found handler, but if
      # that ever changes then keep in mind that removing the root channel may cause breakage. See
      # the discourse link below.
      #
      # https://discourse.nixos.org/t/do-flakes-also-set-the-system-channel/19798/2
      # https://github.com/NobbZ/nixos-config/blob/main/nixos/modules/flake.nix
      nixPath = [ "nixpkgs=${nixpkgsPath}" ];
    };

    # See man systemd.resource-control.
    # cgroup-based resource management relies on nix.conf feature use-cgroups.
    systemd.services.nix-daemon.serviceConfig = {
      CPUWeight = "idle";
      # IOWeight: 1-10,000 and default is 100. Larger numbers are faster, which is the opposite of
      # IOSchedulingPriority.
      IOWeight = "10";
    };

    # Discord is unfree, for example.
    nixpkgs.config.allowUnfree = true;

    systemd.tmpfiles.rules = [
      # Static symlink for nix.nixPath, which controls $NIX_PATH. Using nixpkgs input directly would
      # result in $NIX_PATH containing a /nix/store value, which would be inaccurate after the first
      # nixos-rebuild switch until logging out (and prone to garbage collection induced breakage).
      "L+ ${nixpkgsPath} - - - - ${nixpkgs}"
    ];
  };
}
