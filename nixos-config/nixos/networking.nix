{ config, lib, pkgs, ... }:
{
  options.chasecaleb.networking = {
    config = lib.mkOption {
      type = lib.types.attrsOf lib.types.anything;
      description = ''
        Extra network config, e.g. to disable DHCP and use a static IP.

        See systemd.network.networks.<name> NixOS options.
      '';
      default = { };
    };
  };

  config = {
    # Networking stack:
    # - systemd-networkd
    # - unbound (not systemd-resolved) on 127.0.0.1.
    #
    # https://nixos.wiki/wiki/Systemd-networkd
    networking.useNetworkd = true;
    services.resolved.enable = false;
    # systemd-networkd-wait-online is broken in systemd 253.6, waiting for any interface fixes this.
    # Also this seems reasonable anyways since anyInterface is relevant for e.g. a device with an
    # ethernet and a wifi interface.
    # https://github.com/NixOS/nixpkgs/issues/247608
    # https://github.com/numtide/srvos/blob/85fed23fce8ada8017486090569c0a9054875d5c/nixos/common/networking.nix#L17
    systemd.network.wait-online.anyInterface = true;
    systemd.network.networks."50-ethernet" = lib.recursiveUpdate
      {
        matchConfig.Name = "en*";
        linkConfig.RequiredForOnline = "routable"; # Affects network-online.target.
        networkConfig.IPv6PrivacyExtensions = true;
        DHCP = "yes";
        dns = [ "127.0.0.1" ]; # Not sure this matters since resolved is disabled, but it can't hurt.
        dhcpV4Config.UseDNS = false;
        dhcpV6Config.UseDNS = false;
        ipv6AcceptRAConfig.UseDNS = false;
      }
      config.chasecaleb.networking.config;

    networking.firewall = {
      enable = true; # Explicit default
      trustedInterfaces = [ config.services.tailscale.interfaceName ];
      # Allow incoming tailscale traffic through firewall to allow direct connect instead of relay.
      allowedUDPPorts = [ config.services.tailscale.port ];
    };

    # Note: when querying unqualified name via dig, make sure to use +search: "dig +search desktop".
    networking.resolvconf.extraConfig = ''
      search_domains=border-octatonic.ts.net
    '';

    services.unbound = {
      enable = true;
      resolveLocalQueries = true; # Register unbound in /etc/resolv.conf
      settings = {
        server.prefetch = true;
        forward-zone = [
          {
            name = ".";
            forward-tls-upstream = true;
            forward-addr = [
              "9.9.9.9@853#dns.quad9.net"
              "149.112.112.112@853#dns.quad9.net"
              "1.1.1.1@853#cloudflare-dns.com"
              "1.0.0.1@853#cloudflare-dns.com"
            ];
          }
          {
            # Tailscale "MagicDNS"
            name = "ts.net.";
            forward-addr = [ "100.100.100.100" ];
          }
        ];
      };
    };

    environment.systemPackages = [ pkgs.tailscale ]; # Add tailscale to $PATH for interactive use.
    # Note: Tailscale auth keys enforce a limit on expiration lifetime, so they aren't useful for
    # me. Instead, one-time device setup steps are:
    # 1. sudo tailscale up --accept-dns=false
    #    (DNS is configured in unbound via NixOS instead)
    # 2. Copy the URL from previous step to login
    # 3. Go to https://login.tailscale.com/admin/machines to disable device's key expiry.
    #
    # Note that "auth keys" and "device keys" are not the same.
    # See also: https://tailscale.com/kb/1028/key-expiry/
    services.tailscale.enable = true;
  };
}
