# shellcheck shell=bash # -*- mode: sh; sh-shell: bash -*-
# Systemd unit script to ensure essential git repos are checked out.

set -Eeuo pipefail
die() {
    echo -e "$0" ERROR: "$@" >&2
    exit 1
}
# shellcheck disable=2154
trap 's=$?; die "line $LINENO - $BASH_COMMAND"; exit $s' ERR

code_dir=$HOME/code

# Associative array, repo name -> directory.
declare -A repos
repos=(
    ["org-files"]="$HOME/org-files"
    ["pass"]="$HOME/.password-store"
    ["emacs.nix"]="$code_dir/emacs.nix"
    ["gpg-keys"]="$code_dir/gpg-keys"
)

check_internet() {
    if [[ "${did_check:-}" == "" ]]; then
        did_check=true
        echo "Checking internet connection"
        count=0
        while ! ssh -T git@gitlab.com >/dev/null; do
            if [[ "$count" == 30 ]]; then
                die "No internet connection"
            fi
            echo "Waiting for internet connection..."
            ((count += 1))
            sleep 1
        done
    fi
}

run_gpg_import=
if [[ ! -e ~/code/gpg-keys ]]; then
    run_gpg_import=true
fi

for repo in "${!repos[@]}"; do
    destination=${repos[$repo]}
    echo "Checking $destination"
    if [[ ! -e "$destination" ]]; then
        # Wait until this point to check internet connection since this isn't needed if repos have
        # already been cloned.
        check_internet

        # Make sure parent directory exists first.
        mkdir -p "$(dirname "$destination")"
        repo_url="git@gitlab.com:chasecaleb/$repo.git"
        echo "Cloning repo $repo_url to $destination"
        git clone "$repo_url" "$destination"
    fi
done

if [[ "$run_gpg_import" == "true" ]]; then
    ~/code/gpg-keys/import.sh
    chmod 700 ~/.gnupg
fi
