#!/usr/bin/env bash
# watchexec + rsync utility to e.g. sync files to a test VM.

set -Eeuo pipefail
die() {
    echo -e "${BASH_SOURCE[0]}" ERROR: "$@" >&2
    exit 1
}
# shellcheck disable=2154
trap 's=$?; die "line $LINENO - $BASH_COMMAND"; exit $s' ERR

confirm_or_die() {
    PROMPT=$1
    local confirm
    read -re -p "$PROMPT [y/N] " confirm
    if [[ "$confirm" != "y" && "$confirm" != "Y" ]]; then
        die "Cancelled."
    fi
}

rsync_args=()
export rsync_args

main() {
    if [[ $# != 2 ]]; then
        die "Missing required positional arguments: <local-source> <local-or-remote-dest>"
    fi

    local source dest
    # Trim trailing slashes from input, because rsync is obnoxiously picky so we want to control
    # trailing slash vs lack thereof in this script.
    # shellcheck disable=SC2001
    source=$(echo "$1" | sed 's@/*$@@')
    # shellcheck disable=SC2001
    dest=$(echo "$2" | sed 's@/*$@@')

    rsync_args=(--verbose --archive --delete
        --backup --backup-dir=/tmp/rsync-backup
        "$source/" "$dest")

    echo "***** DRY RUN: *****"
    rsync --dry-run "${rsync_args[@]}"
    echo ""
    confirm_or_die "See dry run above... is this correct?"

    watchexec --watch "$source" --restart -- rsync "${rsync_args[@]}"
}

main "$@"
