{ nixosConfig, nixIndexDb, pkgs, ... }:
{
  imports = [
    nixIndexDb
  ];

  config = {
    programs.home-manager.enable = true;

    home = {
      # For stateVersion changes, see:
      # https://nix-community.github.io/home-manager/release-notes.html
      stateVersion = "22.11";

      file = {
        # https://nixos.wiki/wiki/Discord#Discord_wants_latest_version
        ".config/discord/settings.json".text = ''{ "SKIP_HOST_UPDATE": true }'';

        # I don't want to rewrite my git config into home-manager format. Nothing wrong it, but also not
        # worth dealing with.
        ".config/git/config".source = ./gitconfig;
        ".config/git/gitconfig-work".source = ./gitconfig-work;

        ".config/glab-cli/config.yml".source = ./glab-config.yml;
        ".config/glab-cli/aliases.yml".source = ./glab-aliases.yml;

        # Don't use services.polybar because it doesn't set $PATH right and isn't worth dealing with,
        # since I can start polybar from my Emacs config instead.
        # https://github.com/nix-community/home-manager/issues/1616
        ".config/polybar/config".source = ./polybar.conf;

        # Embedding this inline instead of as a separate file for the sake of parameter expansion.
        ".gradle/gradle.properties".source =
          (pkgs.formats.javaProperties { }).generate "gradle.properties" {
            # Toolchain auto-provisioning on NixOS doesn't work due to incorrect LDD path, so disable it.
            # https://docs.gradle.org/current/userguide/toolchains.html#sub:disable_auto_provision
            "org.gradle.java.installations.auto-download" = "false";
            # - Gradle auto-detects the system default java version, so don't need to list that here.
            # - Toolchains known to Gradle can be verified with: "./gradlew -q javaToolchains".
            # - Java versions with LTS and EOL listed here: https://endoflife.date/java
            "org.gradle.java.installations.paths" = builtins.concatStringsSep "," [
              "${pkgs.jdk8}/lib/openjdk"
              "${pkgs.jdk17}/lib/openjdk"
              "${pkgs.jdk21}/lib/openjdk"
            ];
            "org.gradle.java.home" = "${pkgs.jdk8}/lib/openjdk";
          };

        ".jira.d/config.yml".source = ./jira-config.yml;
      };

      packages =
        let
          glabWrapped = pkgs.symlinkJoin {
            name = "glab";
            paths = [ pkgs.glab ];
            buildInputs = [ pkgs.makeWrapper ];
            postBuild = ''
              wrapProgram $out/bin/glab --run "source ${./glab-init.sh}"
            '';
          };
          # See https://github.com/NixOS/nixpkgs/blob/master/pkgs/applications/misc/polybar/default.nix
          # And list of modules in sidebar here: https://github.com/polybar/polybar/wiki
          # In the future I may want to add nlSupport and iwSupport for networking module.
          polybarOverride = (pkgs.polybar.override { pulseSupport = true; });
        in
        [
          glabWrapped
          polybarOverride
        ] ++ (with pkgs; [
          (discord.override { nss = pkgs.nss_latest; }) # Use correct browser for links.
          dmenu # Used by dunst for its action menu
          fzf
          glabWrapped
          go-jira
          manix
          # peek # gif screen recorder, but gifski dependency is failing to build on 7/31/2023.
          scrot # Screenshots
          yubioath-flutter
        ]);
    };

    programs.direnv.enable = true;

    programs.chromium = {
      enable = true;
    };

    # Reminder: make sure to sign in to Firefox account on first setup.
    programs.firefox = {
      enable = true;
      package = pkgs.firefox-bin;
      profiles.main = {
        # settings corresponds to about:config
        settings = {
          "accessibility.warn_on_browsewithcaret" = false;

          "browser.aboutConfig.showWarning" = false;
          "browser.backspace_action" = 0; # 0 means history back (or forward with shift+backspace)
          "browser.bookmarks.showMobileBookmarks" = true;
          "browser.ctrlTab.recentlyUsedOrder" = false;
          "browser.contentblocking.category" = "strict";
          "browser.newtabpage.enabled" = false;
          "browser.protections_panel.infoMessage.seen" = true;
          "browser.rights.3.shown" = true;
          "browser.startup.homepage" = "about:blank";
          "browser.tabs.warnOnClose" = false;

          "devtools.chrome.enabled" = true;

          "dom.forms.autocomplete.formautofill" = true;
          "findbar.highlightAll" = true;

          # I used to have this set to true because I also had Firefox synced to my Windows desktop and
          # didn't want all of the same extensions enabled. That isn't the case anymore, so I'm explicitly
          # reverting this back to the default of false to make sure it gets applied everywhere.
          "services.sync.addons.ignoreUserEnabledChanges" = false;

          # Use tab-less windows (along with I Hate Tabs extension)
          "browser.tabs.opentabfor.middleclick" = false;
          "toolkit.legacyUserProfileCustomizations.stylesheets" = true;

          # Disable TRR (aka built-in DNS-over-HTTPS) and use system DNS instead.
          # I use systemd-resolved with DoH, so I don't want an app-specific implementation.
          # https://support.mozilla.org/en-US/kb/firefox-dns-over-https#w_manually-enabling-and-disabling-dns-over-https
          "network.trr.mode" = 5;

          # org-roam generates random files in /tmp, so Firefox's "Allow this file to open..." prompt shows
          # up each time the file is recreated even if the "always allow this file" checkbox is enabled.
          "network.protocol-handler.external.org-protocol" = true;
        };
        userChrome = ''
          #titlebar { display: none }
        '';
      };
    };

    programs.fzf = {
      enable = true;
      defaultCommand = "fd --type f --hidden --follow --exclude .git";
      defaultOptions = [ "--height=80%" "--layout=reverse" ];
    };

    programs.gpg = {
      enable = true;
      settings = {
        use-agent = true;
        no-emit-version = true;
        no-comments = true;
        keyid-format = "0xlong";
        with-fingerprint = true;
        list-options = "show-uid-validity";
        verify-options = "show-uid-validity";
        # Signing subkey (gpg -k with flag S)
        default-key = "0xC42CFFADE5C2EC5C";
        # Encryption subkey (gpg -k with flag E)
        default-recipient = "0x53060B01F1632108";

        # DO NOT USE SKS KEYSERVER - BAD THINGS MAY HAPPEN.
        # https://gist.github.com/rjhansen/67ab921ffb4084c865b3618d6955275f
        keyserver = "hkps://keys.openpgp.org/";

        personal-cipher-preferences = "AES256 AES192 AES CAST5";
        personal-digest-preferences = "SHA512 SHA384 SHA256 SHA224";
        cert-digest-algo = "SHA512";
        default-preference-list = "SHA512 SHA384 SHA256 SHA224 AES256 AES192 AES CAST5 ZLIB BZIP2 ZIP Uncompressed";
      };
    };

    services.dunst = {
      enable = true;
      settings = {
        # Make font bigger (default is 8)
        global = {
          font = "Monospace 10";
        };
        # Default low urgency text contrast is too low.
        urgency_low = {
          foreground = "#ffffff";
        };
      };
    };

    services.gpg-agent = {
      enable = true;
      pinentryPackage = pkgs.pinentry-curses;
      extraConfig = ''
        allow-emacs-pinentry
      '';
    };

    programs.ssh = {
      enable = true;
      extraConfig = builtins.readFile ./ssh/config;
    };

    # Wrap comma, nix-index, and command-not-found with nix-index-db file.
    # https://github.com/nix-community/nix-index-database#usage-in-home-manager
    programs.nix-index-database.comma.enable = true;
    programs.nix-index.enable = true;

    programs.zsh = {
      enable = true;
      # I have completion configured in my zshrc-extra file instead.
      completionInit = "";
      # My zshrc needs to at start because of this:
      # https://github.com/zsh-users/zsh-syntax-highlighting/issues/67
      initExtraFirst = builtins.readFile ./zshrc-extra;
      envExtra = builtins.readFile ./zshenv-extra;
      # https://github.com/zsh-users/zsh-autosuggestions
      autosuggestion.enable = true;
      syntaxHighlighting.enable = true;
    };

    xresources.properties = {
      # XIM might cause input lag when typing fast, maybe... I read that somewhere recently but
      # can't figure out where now. However since I don't use X11 Input Method translation (I think
      # this is used for e.g. Japanese), it doesn't hurt to turn off.
      # This couuld also be done by adding --without-xim when building Emacs, but using Xresources
      # config is simpler than overriding Nix's Emacs derivation.
      "Emacs.useXIM" = false;
    };
  };
}
